<?php
    require('config/config.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
    <title><?php echo $title; ?></title>
    <link href="/bpdp/login/favicon.ico" type="image/x-icon" rel="icon">
    <link href="/bpdp/login/favicon.ico" type="image/x-icon" rel="shortcut icon">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="layout.css">
</head>
<body>
    <div class="container"> 
        <div class="row-fluid">
            <div class="span12">
                <div class="navbar navbar-fixed-top navbar-inverse">
                    <div class="navbar-inner">
                        <div class="container">
                            <a class="btn btn-navbar" data-toggle="collapse" data-target=".menu-main">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </a>
                            <a href="#" class="visible-desktop brand"><?php echo constant('TITLE'); ?></a> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="well well-large text-center">
                <h3>Vítejte v aplikaci BPDP</h3>
                <p>Aplikace slouží pro tvorbu zadání bakalářských i diplomových prací a projektů. Dále k prezentování zadání a umožnit rezevaci vybraných tématů studentům.</p>
                <a class="btn btn-large btn-primary" href="<?php echo constant('URL_LOGIN'); ?>">Vstup do aplikace</a>
            </div>
        </div>
    </div>
</body>
</html>