<?php

/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc.
 * (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Controller', 'Controller');
App::uses('CakeEmail', 'Network/Email');
/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

	public $helpers = array(
		'Session',
		'Js' => array('Jquery'),
		'Html' => array('className' => 'BoostCake.BoostCakeHtml'),
		'Form' => array('className' => 'BoostCake.BoostCakeForm'),
		'Paginator' => array('className' => 'BoostCake.BoostCakePaginator'),
		'Csv',
		'App'
	);

	public $uses = array('User');

	public $scaffold;

	public $components = array(
		'Sql',
		'RequestHandler',
		'Paginator',
		'Session' => array('className' => 'ExtendedSession'),
		'EmailNotifier',
		'Auth' => array(
			'authorize' => 'Controller',
			'authenticate' => array('Shibboleth'),
			'authError' => 'Bohužel, k této akci nebo celé sekci nemáte dostatečná oprávnění.',
			'loginRedirect' => array(
				'controller' => 'topics',
				'action' => 'index'
			),
			'logoutRedirect' => array(
				'controller' => 'topics',
				'action' => 'index'
			),
			'flash' => array(
				'element' => 'alert',
				'key' => 'auth',
				'params' => array(
					//'plugin' => 'BoostCake',
					'class' => 'error')
			)
		)
	);
	
		
	public function beforeFilter() {
		if (!$this->Auth->loggedIn()) {
			$this->Auth->authError = false;
		}		
		if (!empty($this->request->data)) {
			$this->request->data = trimArrayRecursively($this->request->data);
		}
		
		$user = $this->Auth->user();

		$title_for_layout = '';
		$this->set(compact('title_for_layout'));

		$paginator = Configure::read('Application.paginator');

		$settings = $paginator['default'];
		if (!empty($paginator[$this->request->controller])) {
			$settings = array_replace_recursive($settings, $paginator[$this->request->controller]);
		}

		$limit = $this->Auth->user('User.page_limit');
		if (!empty($limit) && $this->request->controller == 'topics') {
			$settings['limit'] = $limit;
		}
		$this->Paginator->settings = $settings;
	}

	public function beforeRender() {

		$current_user = $this->Auth->user();

		$this->set(compact('current_user'));
	}

	public function isAuthorized($user = null) {
		return $user['Group']['name'] == 'superuser';
	}

	protected function add_data($model, $data) {
		$this->$model->create();

		if ($this->$model->saveAll($data)) {
			$this->Session->success('Data byla uložena.');
			return true;
		}
		$this->Session->fault('Data se nepodařilo uložit. Zkontrolujte, zda jsou vyplněna všechna pole.');
	}

	public function view($id = null) {
		$model = Inflector::classify($this->request->controller);

		$data = $this->$model->findById($id);
		if (!$data) {
			throw new NotFoundException(__('Vybraná data nebyla nalezena.'));
		}
		$this->set(strtolower($model), $data);
	}

	public function edit($id = null, $options = array()) {
		$model = Inflector::classify($this->request->controller);
		$defaultOptions = array('conditions' => array($model . '.id' => $id));
		$data = $this->$model->find('first', array_merge_recursive($options, $defaultOptions));
		if (!$data) {
			throw new NotFoundException(__('Data nebyla nalezena odpovídající data pro editaci.'));
		}
		if ($this->isPostRequest()) {
			$this->$model->id = $id;
			if ($this->$model->saveAll($this->request->data, $options)) {
				$this->Session->success('Data byla úspěšně aktualizována.');
				return true;
			}
			$this->Session->fault('Data nebyla uložena. Zkontrolujte, zda jsou vyplněna všechna pole.');
		} else {
			$this->request->data = $data;
		}
		return false;
	}

	public function delete($id) {

		$model = Inflector::classify($this->request->controller);

		if ($this->request->is('get')) {
			throw new MethodNotAllowedException();
		}

		if ($this->$model->delete($id)) {
			$this->Session->success('Data byla úspěšně odstraněna.');
			return true;
		}

		$this->Session->fault('Data se nepodařilo odstranit. ');
	}

	public function get_array_value($array, $index, $separator = '.') {
		$parts = explode($separator, $index);

		$data = $array;

		$next = false;
		foreach ($parts as $part) {
			$part = str_replace('-', '_', $part);
			if ($next) {
				foreach ($data as $key => $val) {
					$data[$key] = $val[$part];
				}
				break;
			}

			if ($part == '%d') {
				$next = true;
			} else {
				$data = $data[$part];
			}
		}
		return $data;

	}

	public function isPostRequest() {
		return $this->request->is(array(
			'post',
			'put'
		));
	}

	public function removeDiacritics($str) {
		return strtr($str, "ÁÄČÇĎÉĚËÍŇÓÖŘŠŤÚŮÜÝŽáäčçďéěëíňóöřšťúůüýž", "AACCDEEEINOORSTUUUYZaaccdeeeinoorstuuuyz");
	}

}

function trimArrayRecursively($input) {

	if (!is_array($input))
		return trim($input);

	return array_map('trimArrayRecursively', $input);
}