<?php

class UsersController extends AppController {

    public $uses = array(
        'User',
        'Thesis'
    );

    public function index() {
    	$title_for_layout = __("Uživatelé");
        $this->set(compact('title_for_layout'));    

        $conditions = array();
		if ($this -> request -> is(array('post', 'put')) && !empty($this->request->data['User']['search'])) {
			$conditions = array(
				'User.surname LIKE' => '%'.$this->request->data['User']['search'].'%',
				'User.email LIKE' => '%'.$this->request->data['User']['search'].'%'
			);
		}
		
		$this->Paginator->settings['conditions'] = array(
			'AND' => array(
				'OR' =>
					$conditions			
			)
		);
		$this->Paginator->settings['contain'] = array('Group.name', 'Institute');
    	$users = $this->Paginator->paginate('User');
        $this->set(compact('users'));
    }
    
    public function view($id = null) {
		if(parent::isPostRequest()) {
            setlocale(LC_CTYPE, 'cs_CZ.utf-8');
            $forename_to_edit = ucfirst(iconv("UTF-8", "ASCII//TRANSLIT", trim($this->request->data['User']['forename'])));
            $surname_to_edit = ucfirst(iconv("UTF-8", "ASCII//TRANSLIT", trim($this->request->data['User']['surname'])));
            $name_valid = $this->User->getName($this->Auth->user('User.email'));
            if($forename_to_edit == $name_valid[0]) {
                if($surname_to_edit == $name_valid[1]) {
        			$this->request->data['User']['id'] = $this->Auth->user('User.id');
        			parent::edit($this->Auth->User('User.id'));
        			$this->login();
                } else {
                    $this->User->validationErrors['surname'] = Array(__("Neplatné příjmení - musí korespondovat s Vaší emailovou adresou."));
                }
            } else {
                $this->User->validationErrors['forename'] = Array(__("Neplatné jméno - musí korespondovat s Vaší emailovou adresou."));
            }
		} 
		$this->request->data = $this->User->findById($id);

        $thesesMerged = $this->Thesis->find('all', array(
            'conditions' => array(
                'user_id' => $id,
                'archived' => 0,
            ),
            'contain' => array('Type.abbreviation')
        ));
		$institutes = $this->User->Institute->find('list', array(
			'fields' => array('id', 'abbreviation')
		));
		
		$userQueues = $this->User->getCountUserQueues($this->Auth->user('User.id'));
		$this->set(compact('institutes', 'userQueues', 'thesesMerged'));
    }
    
    public function beforeFilter() {
        parent::beforeFilter();
    }
    
    public function beforeRender() {
        parent::beforeRender();            
	}

    /**
     * alows or deny action of controller for user
     * @param type $user
     * @return boolean
     */
    public function isAuthorized($user = null) {        
       
        if (in_array($this->action, array('login', 'logout'))) {
            return true;
        }
        
        // pass auth if user is editting or viewing own profile
        if (in_array($this->action, array('edit', 'view')) && isset($this->params['pass'][0]) 
                && $user['User']['id'] == $this->params['pass'][0]) {
            return true;
        }
                
        return parent::isAuthorized($user);
    }

    public function login($email = null) {
        if($this->Auth->user('Group.name') == 'superuser' && isset($email)) {
            $this->Session->write('login_email', $email);
        }

		try {
	        if($this->Auth->login()) { 
				return $this->redirect($this->Auth->redirect());
	        }
		} catch(ForbiddenException $e) {
			if($e->getCode() != 2) {
				$this->set('title_for_layout', __('Ověření uživatele selhalo'));
				$this->Session->fault($e->getMessage());
			} else {
				$this->set('title_for_layout', __('Aplikace není přístupná'));
				$this->Session->info($e->getMessage());
			} 
			$this->layout = 'error';
			$this->view = 'empty'; 
		} 
    }
    
    public function logout() {
        $login_email = $this->Session->read('login_email');
        if(isset($login_email)) {
            $this->Session->delete('login_email');
            return $this->redirect(array('action' => 'login'));       
        } else {
            $this->redirect(Configure::read('Application.url'));
        }
    }

    public function usernames() {
        $users = $this->User->find('all');
        foreach($users as $user) {
            $names = $this->User->getName($user['User']['email']);
            $user['User']['forename'] =  $names[0];
            $user['User']['surname'] =  $names[1];
            $this->User->id = $user['User']['id'];
            $this->User->save($user);
        }
        $this->view = 'empty';
    }

}