<?php
class DepartmentsController extends AppController {
        
                
    public function index($id = null) {
    	
		$data = &$this->request->data;
		$abbreviation = &$data['Department']['abbreviation'];
    	$parent_id = &$data['Department']['parent_id'];
		$this->setAdditionalData();
				
    	if(empty($parent_id) || $parent_id == 0) {
            $parent_id = null; 
        }
		if(isset($abbreviation)) {
			$abbreviation = strtoupper($abbreviation);
		}
		
		if(!empty($id)) {
			$this->set('subtitle', 'Editace oboru');
			if(parent::edit($id, $options = Array())) {
				return $this->redirect(array('action' => 'index'));
			}
		} else if ($this->request->is(array('post', 'put'))) {
			if($this->Department->alreadyExists($data)) {
				$this->Session->setFlash(__('Stejný obor v zadané nadřazené kategorri již existuje.'), 'alert', array('class' => 'error'));
				return;
			}
			
			if(parent::add_data('Department', $data)) {
				return $this->redirect(array('action' => 'index'));
			}
		}
    }    
        
    public function delete($id) {
        parent::delete($id);  
		return $this->redirect(array('action' => 'index'));
    }
    
    public function beforeFilter() {
        parent::beforeFilter();
    }
    
    public function beforeRender() {
        $title_for_browser = __("Obory na fakultě");
        $this->set(compact('title_for_browser'));
        parent::beforeRender();                
    }
    
    private function setAdditionalData() {
        $departments = $this->Department->generateTreeList(null, null, null, '');
        $root_departments = $this->Department->find('list', array('conditions' => array('Department.parent_id' => null)));
		$abbreviations_list = $this->Department->find('list', array('fields'=>array('abbreviation')));
        $this->set(compact('abbreviations_list', 'departments', 'root_departments'));
    }
    
    public function isAuthorized($user = null) {
        return parent::isAuthorized($user);
    }
}
 