<?php

class TypesController extends AppController {
    
    public function index($id = null) {
    	
		$data = &$this->request->data;
		$this->setAdditionalData();
		
		if(!empty($id)) {
			$this->set('subtitle', 'Editace typu práce');
			if(parent::edit($id, $options = Array())) {
				return $this->redirect(array('action' => 'index'));
			}
		} else if ($this->request->is(array('post', 'put'))) {			
			if(parent::add_data('Type', $data)) {
				return $this->redirect(array('action' => 'index'));
			}
		}
    }
    
    public function delete($id) {
        parent::delete($id);  
		return $this->redirect(array('action' => 'index'));
    }
    
    public function beforeFilter() {
        parent::beforeFilter();
    }
    
    public function beforeRender() {
        $title_for_browser = __("Typy studentských prací");
        $this->set(compact('title_for_browser'));
        parent::beforeRender();                           
    }
    
    private function setAdditionalData() {
        $types = $this->Type->find('all');   
		$rootDepartments = $this->Type->Department->find('list', array('conditions' => array('parent_id' => null)));		     
        $this->set(compact('types', 'rootDepartments'));
    }
    
    public function isAuthorized($user = null) {   
        return parent::isAuthorized($user);
    }
}