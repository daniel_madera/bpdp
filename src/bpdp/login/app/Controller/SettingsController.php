<?php

class SettingsController extends AppController {

	public $uses = array(
		'Group',
		'Thesis',
		'Topic',
		'Queue'
	);

	public function index() {
		$groups = $this->Group->find('all', array('conditions' => array('Group.name NOT' => array('admin','superuser'))));
		$this->set(compact('groups')); 
		if ($this->request->is('post', 'put')) {
			if (!empty($this->request->data['Settings']['id'])) {
				$this->Group->id = $this->request->data['Settings']['id'];
				$timestamp = strtotime($this->request->data['Settings']['auto_access_time'].' '.$this->request->data['Settings']['auto_access_date']);
				if (!$timestamp) {
					$this->Session->fault(__('Nevalidní datum a čas.'));
					return;
				}
				$date = date('Y-m-d H:i:s', $timestamp);
				if (!$this->Group->saveField('auto_access', $date)) {
					$this->Session->fault(__('Nepodařilo se uložit datum.'));
					return;
				}
				$this->request->data = array();
				$groups = $this->Group->find('all', array('conditions' => array('Group.name NOT' => array('admin', 'superuser'))));
				$this->set(compact('groups'));
			} else if (!empty($this->params['named']['archive_theses'])) {
				$this->Thesis->archiveAll();
			} else if (!empty($this->params['named']['archive_topics'])) {
				$this->Topic->archiveAll();
			} else if (!empty($this->params['named']['remove_queues'])) {
				$this->Queue->removeAll();
			}
			$this->Session->success(__('Operace proběhla úspěšně.'));
		}

	}

	public function beforeFilter() {
		parent::beforeFilter();
	}

	public function beforeRender() {
		parent::beforeRender();
		$title_for_layout = __('Nastavení');
		$this->set(compact('title_for_layout'));
	}

	public function clearlogs() {
		$this->Session->success(__('Operace proběhla úspěšně.'));
		$this->Session->fault(__('Nepodařilo se vyčistit log soubory.'));
	}

	public function isAuthorized($user = null) {
		return parent::isAuthorized($user);
	}

}
