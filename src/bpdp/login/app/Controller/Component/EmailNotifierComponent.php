<?php
App::uses('Component', 'Controller');

class EmailNotifierComponent extends Component {
	
	public $usedModels = array('Topic', 'Thesis');	
	
	public function initialize(Controller $controller) {
		parent::initialize($controller);
		
		foreach($this->usedModels as $model) {
			$this->$model = ClassRegistry::init($model);
		}		
	}
		
	public function sendEmail($template, $layout, $to, $vars = array(), $options = array()) {
		
		if(Configure::read('Application.email_notifications') != 1) {
			return;
		} 
		
		$email = new CakeEmail('default');
		// if set debug_email turn on debug report and set recipient to debug_email
		$debug_email = Configure::read('debug_email');
		$email_to = $to;
		if(!empty($debug_email)) {
			$email_to = $debug_email;
			$debug = 'Original recipient: '.$to;
			if(isset($vars['debug'])) {
				$vars['debug'] .= ', '.$debug;
			} else {
				$vars['debug'] = $debug;
			}
		}
		
		$email->viewVars($vars);
		$email->config($options);
		return $email->template($template, $layout)
		    ->emailFormat('html')
		    ->to($email_to)
		    ->send();
	}
	 
	public function topicRequested($queue_id) {
		$queue = $this->Topic->Queue->find('first', 
			array(
				'contain' => array(
					'Topic.Leader.email',
					'User.email',
					'User.full_name',
					'Type.name'
				),
				'conditions' => array(
					'Queue.id' => $queue_id
				)
			)
		);		
		if(empty($queue)) {
			throw new InternalErrorException('Nebyla nalezena žádost studenta o téma.');
		}
		$vars = array('queue' => $queue);
		$this->sendEmail('topic_requested', 'default', $queue['Topic']['Leader']['email'], $vars, array('replyTo' => $queue['User']['email'], 'subject' => __('Žádost o téma č. %s', $queue['Queue']['topic_id'])));
	}
		
	public function topicRequestMerged($topic_id) {
		// notification to student who is accepted
		$topic = $this->Topic->find('first', 
			array(
				'contain' => array(
					'Leader.email',
					'Leader.full_name',
					'Thesis.User',
					'Thesis.Type.name',
				),
				'conditions' => array(
					'Topic.id' => $topic_id
				)
			)
		);	
		$vars = array('topic' => $topic);
		$this->sendEmail('topic_request_merged', 'default', $topic['Thesis']['User']['email'], 
		$vars, array('replyTo' => $topic['Leader']['email'], 'subject' => __('Přijetí žádosti o téma č. %s', $topic['Topic']['id'])));
	}	
	
	public function topicRequestsRejected($topic_id, $selecteQueues = array()) {
		$default = array(
			'conditions' => array(
				'Topic.id' => $topic_id
			),
			'contain' => array(
				'Leader', 'Queue.Type.name', 'Queue.User.email'
			)
		);
		
		$topic = $this->Topic->find('first', $default);		
		$vars = array('topic' => $topic);
		foreach($topic['Queue'] as $queue) {
			if(empty($selecteQueues) || (!empty($selecteQueues) && in_array($queue['id'], $selecteQueues))) {
				$vars['queue'] = $queue;
				$this->sendEmail('topic_request_rejected', 'default', $queue['User']['email'], 
				$vars, array('replyTo' => $topic['Leader']['email'], 'subject' => __('Zamítnutí žádosti o téma č. %s', $topic['Topic']['id'])));
			}
		}
	}
	
	public function thesisDeleted($id) {
		$thesis = $this->Thesis->find('first', 
			array(
				'contain' => array(
					'Topic.Leader',
					'User.email',
					'Type.name',
					'Topic',
				),
				'conditions' => array(
					'Thesis.id' => $id
				)
			)
		);	
		$vars = array('thesis' => $thesis);
		$this->sendEmail('thesis_deleted', 'default', $thesis['User']['email'], 
		$vars, array('replyTo' => $thesis['Topic']['Leader']['email'], 'subject' => __('Zrušení oficiálního zadání tématu č. %s', $thesis['Topic']['id'])));		
	}

}