<?php

App::uses('BaseAuthenticate', 'Controller/Component/Auth');

class ShibbolethAuthenticate extends BaseAuthenticate {
	public function authenticate(CakeRequest $request, CakeResponse $response) {
		$this->init();
		// manage alternative login through superuser
		$login_email = CakeSession::read('login_email');
		if(isset($login_email) && !empty($login_email)) {
			$user = $this->User->find('first', array(
				'conditions' => array(
					'email' => $login_email
				),
				'contain' => array('Group', 'Institute')
			));
			if(!$user) {
				CakeSession::fault(__('Nepodařilo se najít uživatele s emailem %s.', $login_email));
			} else {
				return $user;
			}
		}

		$user = $this->User->find('first', array(
			'conditions' => array(
				'email' => $this->server['eppn']
			)
		));	
		if(empty($user)) {
			$this->User->create();
			$user['User']['email'] = $this->server['eppn'];
		} else {
			$this->User->id = $user['User']['id'];
		}

		if(empty($user['User']['surname']) || empty($user['User']['forename'])) {
			$name = $this->User->getName($this->server['eppn']);
			$user['User']['forename'] = $name[0];
			$user['User']['surname'] = $name[1];
		}
		try {
			$user['User']['group_id'] = $this->getGroupIdByConfig();
			$this->Group->id = $user['User']['group_id'];
			$groupName = $this->Group->field('name');
			if($groupName == 'employee' || $groupName == 'admin' || $groupName == 'superuser') {
				$user['User']['institute_id'] = $this->readInstitute();
			} else {
				$user['User']['institute_id'] = null;
			}
			if(Configure::read('debug') < 0) {
				CakeLog::write('debug', sprintf('Successful login: "%s" | orgunit-dn: "%s" | affiliation: "%s"',
					$this->server['eppn'],
					$this->server['orgunit-dn'],
					$this->server['affiliation']	
				));
			}
		} catch(ForbiddenException $e) {
			CakeLog::write('debug', sprintf('Unsuccessful login: "%s" | orgunit-dn: "%s" | affiliation: "%s"',
				$this->server['eppn'],
				$this->server['orgunit-dn'],
				$this->server['affiliation']	
			));
			throw new ForbiddenException(__('Nemáte dostatečná oprávnění pro vstup do aplikace.').' '.
				__('Nevalidní parametr z Shibbolethu:').' '.$e->getMessage());			
		}
		
		//debug(date('d. m. Y v H:i:s', time()));
		$accessTime = strtotime($this->Group->field('auto_access'));
		if($accessTime > time()) {
			throw new ForbiddenException(__('Systém ještě není přístupný pro zadávání a výběr témat. Datum zpřístupnění: %s', date('d. m. Y v H:i:s', $accessTime)), 3);							
		}	
		
		$user['User']['last_login'] = date('Y-m-d H:i:s');
		$this->User->save($user);
		$user = $this->User->find('first', array(
			'conditions' => array(
				'email' => $this->server['eppn']
			),
			'contain' => array(
				'Group', 'Institute'
			)
		));
		return $user;
	}

	private function init() {
		$this->User = ClassRegistry::init('User');
		$this->Group = ClassRegistry::init('Group');
		$this->AltUser = ClassRegistry::init('AltUser');
		$this->Institute = ClassRegistry::init('Institute');
		$permissions = Configure::read('Application.restrictions');
		
		// reads $_SERVER for parameters optionally debug_"parameter" if set
		foreach ($permissions as $group_name => $indexes) {
			foreach ($indexes as $index => $values) {
				$this->server[$index] = env($index);
				$debug_val = Configure::read('debug_'.$index);
				if(!empty($debug_val)) {
					$this->server[$index] = $debug_val;
				}
			}
		}		
	}
	
	private function checkIfContainsAll($contained, $constains) {
		if(empty($contained) || empty($constains)) {
			return false;
		}
		$contained = array_map('trim', $contained);
		$constains = array_map('trim', $constains);
		foreach ($contained as $key => $value) {
			if(!in_array($value, $constains)) {
				return false;
			}
		}
		return true;
	}

	private function getGroupIdByConfig() {
		$alt_access = $this->getAltAccess();
		if($alt_access) {
			return $alt_access['group_id'];
		}

		$groups = $this->Group->find('list', array('order' => 'Group.id'));
		foreach ($groups as $groupIndex => $groupName) {
			$restrictions = Configure::read('Application.restrictions.'.$groupName);
			$found = true;
			foreach($restrictions as $key => $value) {
				if(!$this->passed($key, $value)) {
					$found = false;
					$failedOn = $key;
					// debug($key);
					// debug($this->server[$key]);
					// debug($value['combinations']);
					break;
				}
			}
			if($found)
				return $groupIndex;
		}		
		throw new ForbiddenException('affiliation, orgunit-dn');
	}
	
	// compare each value of array with server parameter if one fits returns true otherwise returns false
	private function passed($key, $array) {
		foreach($array['combinations'] as $combination) {
			if(array_key_exists('delimiter', $array)) {
				$combination = explode($array['delimiter'], $combination);
				$serverCombination = explode($array['delimiter'], $this->server[$key]);
				if($this->checkIfContainsAll($combination, $serverCombination)) {
					return true;
				}
			} else {
				if($combination == $this->server[$key]) {
					return true;
				}
			}
		}		
		return false;
	}

	private function readInstitute() {
		$alt_access = $this->getAltAccess();
		if($alt_access) {
			return $alt_access['institute_id'];
		}

		$instituteAbbreviation = $this->readProperty('cn', $this->server['orgunit-dn']);
		if(!isset($instituteAbbreviation)) {
			$instituteAbbreviation = Configure::read('Application.default_institute');
			if(empty($instituteAbbreviation)) {
				throw new ForbiddenException(__('Není nastavený výchozí ústav v konfiguračním souboru.'));
			}			
		}
		
		$intitute = $this->Institute->find('first', array(
			'conditions' => array(
				'Institute.abbreviation' => $instituteAbbreviation
			)
		));
		
		if(!isset($intitute['Institute']['id'])) {
			throw new ForbiddenException(__('O ústavu %s neexistuje údaj v databázi.', $instituteAbbreviation));
		}
		return  $intitute['Institute']['id'];
	}

	private function readProperty($name, $text) {
		preg_match('/[\s,]?' . $name . '=([A-Z]{1,5})[\z\s,]/', $text, $matches);
		if (count($matches) == 2) {
			return $matches[1];
		} 
		return null;
	}

	private function getAltAccess() {
		
		$alt_user = $this->AltUser->find('first', array(
			'conditions' => array(
				'AltUser.email' => $this->server['eppn']
			)
		));

		// debug($alt_user);
		// die();

		if(isset($alt_user['AltUser']['id'])) {
			return array(
				'institute_id' => $alt_user['AltUser']['institute_id'],
				'group_id' => $alt_user['AltUser']['group_id']
			);
		}

		return false;
	}
}