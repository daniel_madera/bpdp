<?php
App::import('Component', 'Session');

class ExtendedSessionComponent extends SessionComponent {
	
	public function fault($message) {
		parent::setFlash(__($message), 'alert', array('class' => 'error'));
	}
	
	public function warning($message) {
		parent::setFlash(__($message), 'alert', array('class' => 'warning'));
	}
	
	public function success($message) {
		parent::setFlash(__($message), 'alert', array('class' => 'success'));
	}
	
	public function info($message) {
		parent::setFlash(__($message), 'alert', array('class' => 'info'));
	}
	
} 
