<?php

App::uses('Component', 'Controller');
class SqlComponent extends Component {
    	
	/**
	 * returns list of columns in model
	 * model - name of model
	 * filter - array with defined types only
	 * options
	 * 	detailed - returns columns with properties to each (default: false)
	 *  modelname - returns columns like 'Model.column' (default: true) 
	 */
	 
    public function getColumns($model, $filter = null, $options = array()) {
    	
		$default_options = array(
			'detailed' => false, 
			'modelname' => true
		);
		
		$options = array_replace($default_options, (array) $options);
		
    	$schema = ClassRegistry::init($model)->schema();
		
		foreach ($schema as $column => $properties) {	
			$passed = true;
			if(is_array($filter)) {
				foreach($filter as $key => $value) {
					if($properties[$key] != $value) {
						$passed = false;
					}					
				}
			}	
 			
			if($passed) {
				$column_operand = $column;
				
				if($options['modelname']) {
					$column_operand = $model.'.'.$column;
				}
				
				if($options['detailed']) {
					$columns[$column_operand] = $properties;
				} else {
					$columns[] = $column_operand;
				}
			}
		} 
		
		//debug($columns);
		return $columns;		
    }
	
	
	/**
	 *  returns array of conditions for each column with same value
	 * 	options - array
	 * 		logic - AND or OR (default: OR)
	 * 		comparsion - LIKE or null to compare direct value (default: LIKE)
	 *  	wildcard - pattern to sprintf with value example: '%%%s%%' or '%s'
	 */
	 
	 public function getColumnsConditions($columns, $values, $options = array()) {
	 	
		$default_options = array(
			'logic' => 'OR', 
			'comparsion' => 'LIKE', 
			'wildcard' => '%%%s%%'
		);
		
		$options = array_replace($default_options, (array) $options);
		$values = (array) $values;
		
		foreach ($values as &$value) {
			$value = sprintf($options['wildcard'], $value);				
		}
		
		$conditions = array();
		foreach ($columns as $column) {
			
			$column = 'LOWER('.$column.')';			
			if(!empty($options['comparsion'])) {
				$column .= ' '.$options['comparsion']; 
			} 
			
			foreach($values as $value) {
				$conditions[$column] = $value;
			}				
		}		
		
		return array($options['logic'] => $conditions); 		
	 }
	 
	 function isInteger($input){
		  return preg_match('@^[-]?[0-9]+$@',$input) === 1;
		}
	 
	
}