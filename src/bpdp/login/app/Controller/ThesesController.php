<?php

class ThesesController extends AppController {
	public $components = array(
		'RequestHandler',
		'Mpdf'
	);

	public $uses = array(
		'Queue',
		'Thesis',
		'Institute'
	);

	public function add() {
		if (!empty($this->params['named']['Queue.id'])) {
			$queue_id = $this->params['named']['Queue.id'];
			$queue = $this->Queue->find('first', array(
				'conditions' => array(
					'Queue.id' => $queue_id
				),
				'contain' => array(
					'Topic'
				)
			));
			if(empty($queue)) {
				$this->Session->fault('Žádost studenta o téma nebyla nalezena.');
				return $this->redirect($this->referer());
			}
			if ($this->Thesis->alreadyExists(array('Thesis.topic_id' => $queue['Queue']['topic_id']))) {
				$this->Session->fault('Toto téma je již vybrané.');
				return $this->redirect($this->referer());
			}
									
			$thesis['Thesis']['name'] = $queue['Topic']['name'];
			$thesis['Thesis']['topic_id'] = $queue['Queue']['topic_id'];
			$thesis['Thesis']['type_id'] = $queue['Queue']['type_id'];
			$thesis['Thesis']['user_id'] = $queue['Queue']['user_id'];
			
			$this->Thesis->create();
			if ($this->Thesis->save($thesis, array('validate' => false))) {
				$this->EmailNotifier->topicRequestMerged($queue['Queue']['topic_id']);
				$this->Queue->deleteAllByUserIdTypeId($queue['Queue']['user_id'], $queue['Queue']['type_id']);
				$this->EmailNotifier->topicRequestsRejected($queue['Queue']['topic_id']);
				$this->Queue->deleteAllByTopicId($queue['Queue']['topic_id']);
				$this->Session->success('Student byl přiřazen. Můžete pokračovat vyplněním oficiálního zadání.');
				return $this->redirect(array('controller' => 'topics', 'action' => 'index'));
			} else {
				throw new InternalErrorException('Systému se nepodařilo přiřadit studenta k tématu vytvořením oficiálního zadání.');
			}
		}

		$this->Session->fault('Zadaný dotaz není validní.');
		return $this->redirect($this->referer());

	}

	public function edit($id = null, $options = Array()) {
		$this->set('title_for_layout', __('Úprava oficiálního zadání'));
		$isValid = true;
		if (parent::isPostRequest()) {
			$this->remove_empty($this->request->data['Requirement'], 'text');
			$this->remove_empty($this->request->data['Literature'], 'text');
			$this->Thesis->Requirement->deleteByThesisId($id);
			$this->Thesis->Literature->deleteByThesisId($id);
			
			if(isset($this->request->data['Thesis']['final']) && $this->request->data['Thesis']['final'] == 1) {
				$this->Thesis->set($this->request->data);
				$isValid = $this->Thesis->validates();
			} 
		}
		if ($isValid && parent::edit($id, array('validate' => false, 'contain' => array('Topic.id', 'Topic.consultant', 'Requirement', 'Literature')))) {
			return $this->redirect(array('action' => 'index'));
		} else if(parent::isPostRequest()) {
			$this->Session->fault('Data nebyla uložena. Zkontrolujte, zda jsou vyplněna všechna pole.');
		}
						
		$topic = $this->Thesis->Topic->find('first', array(
			'conditions' => array(
				'Thesis.id' => $id
			),
			'contain' => array('Thesis.User', 'Thesis', 'Leader.full_name', 'Leader.id', 'Department.id', 'Institute')
		));
		
		$types = $this->Thesis->Type->find('list');
		$institutes = $this->Institute->find('list');
		$departments = $this->Thesis->Topic->Department->getList();
		$departments_abbreviation = $this->Thesis->Topic->Department->findSubDepartments('list','abbreviation');
		$this->set(compact('types', 'departments', 'departments_abbreviation', 'topic', 'institutes'));
	}

	public function delete($id = null) {
		$topic_id = $this->Thesis->field('topic_id');
		$this->EmailNotifier->thesisDeleted($id);
		if(parent::delete($id)) {
			$this->Thesis->Topic->setStatus($topic_id, 'active');
			$this->Session->success('Oficiální zadání bylo úspěšně odstraněno. Téma zůstalo zachováno.');
		}
		
		return $this->redirect(array('action' => 'index'));
	}

	public function index($id = null) {
		$this->set('title_for_layout', __('Přehled oficiálních zadání'));

		if (!empty($id)) {
			$conditions = array('Thesis.id' => $id);
		} else {
			$conditions = array('Topic.leader_id' => $this->Auth->user('User.id'));
		}
		
		$this->Paginator->settings['conditions'] = $conditions;
		$this->Paginator->settings['order'] = array('Thesis.created DESC');
		$this->Paginator->settings['contain'] = array('Topic.Leader.full_name', 'User.full_name', 'Type.abbreviation', 'Topic.id');
		$theses = $this->Paginator->paginate('Thesis');

		$this->set(compact('theses'));
	}
	
	public function admin_index() {
		$conditions = array();
		if(!empty($this->params->named['institute'])) {
			$conditions['Topic.institute_id'] = $this->params->named['institute'];
			$this->set('title_for_layout', __('Oficiální zadání ústavu %s', $this->Auth->user('Institute.abbreviation')));
		} else {
			$this->set('title_for_layout', __('Oficiální zadání všech ústavů'));
		}
		$this->Paginator->settings['conditions'] = $conditions;
		$this->Paginator->settings['order'] = array('Thesis.created DESC');
		$this->Paginator->settings['contain'] = array('Topic.Leader.full_name', 'User.full_name', 'Type.abbreviation', 'Topic.id');
		$theses = $this->Paginator->paginate('Thesis');
		$this->set(compact('theses'));
		$this->view = 'index';
	}

	public function download($id = null) {
		$this->Thesis->id = $id;
		if (!$this->Thesis->exists()) {
			throw new NotFoundException(__('Data nebyla nalezena.'));
		}

		ini_set('memory_limit', '512M');
		$thesis = $this->Thesis->find('first', array(
			'conditions' => array(
				'Thesis.id' =>  $id
			),
			'contain' => array('User.full_name', 'User.surname', 'Topic.Leader.full_name', 'Topic.Institute.name', 'Type', 'Requirement', 'Literature', 'Topic.consultant') 
		));
		$this->set(compact('thesis'));

		$this->Mpdf->init();
		$filename = __('zadani-%s.pdf', str_replace(" ", "-", strtolower(iconv('UTF-8', 'ASCII//TRANSLIT', $thesis['User']['surname']))));
		$this->Mpdf->setFilename($filename);
		$this->set('title_for_layout', $filename);
		$this->Mpdf->setOutput('I');
	}

	public function export() {
		$this->set('title_for_layout', __('Export zadání'));
		$institutes = $this->Institute->find('list');
		$this->set(compact('institutes'));
		if (parent::isPostRequest()) {

			$conditions = array();
			$conditions['Topic.institute_id'] = $this->request->data['Thesis']['institute_id'];
			$conditions['Thesis.final'] = $this->request->data['Thesis']['final'];
			
			if (!empty($this->request->data['Thesis']['created_from']))
				$conditions['Thesis.created >='] = $this->request->data['Thesis']['created_from'];
			if (!empty($this->request->data['Thesis']['created_to']))
				$conditions['Thesis.created <='] = $this->request->data['Thesis']['created_to'];

			
			$indexes = array();
			foreach ($this->request->data as $index => $value) {
				if (strpos($index, 'column-id-') !== false) {
					$indexes[] = str_replace('column-id-', '', $index);
				}
			}
			if (empty($indexes)) {
				$this->Thesis->invalidate('columns',  __('Vyberte alespoň jeden sloupec.'));
				return;
			}
			
			$containIndexes = array();
			foreach($indexes as $index) {
				if(strpos($index, 'Thesis_') === false) {
					$tmp = str_replace('_', '.', $index);
					$tmp = str_replace('-', '_', $tmp);
					$tmp = str_replace('%d.', '', $tmp);
					
					$parts = split('\.', $tmp);
					if(count($parts) > 2) {
						$containIndexes[] = $parts[0].'.'.$parts[1];
					} else {
						$containIndexes[] = $tmp;
					}
				}
			}
			$containIndexes[] = 'Topic';
			
			$theses = $this->Thesis->find('all', array(
				'conditions' => $conditions,
				'contain' => array_unique($containIndexes),
				'group' => array('Thesis.id'),
				'order' => $this->request->data['Thesis']['sort'].' '.$this->request->data['Thesis']['direction'],
			));
			// debug($containIndexes);
			// debug($theses);
			// debug($indexes);
			$export_data = $this->getReadAbleExportData($theses, $indexes);
			
			if (empty($export_data) || empty($theses)) {
				$this->Session->setFlash(__('Nebyl nalezen žádný záznam.'), 'alert', array('class' => 'error'));
				return;
			}

			$this->set(compact('export_data', 'theses'));
			$settings_data = explode('_', $this->request->data['Thesis']['file_type']);
			$file_type = $settings_data[0];		
			$encoding = 'UTF-8';
			if(isset($settings_data[1])) {
				$encoding = $settings_data[1];
			}	
			$this->set(compact('encoding'));

			if ($file_type == 'xml') {
				$this->set(array(
					'thesis' => $export_data,
					'_serialize' => array('thesis')
				));
				$this->RequestHandler->respondAs($file_type);
				$this->RequestHandler->renderAs($this, $file_type);
			} else {
				$this->layout = null;
				$this->render($file_type . '/export');
				$this->autoLayout = false;
			}

			$this->response->download('export.' . $file_type);
		}
	}

	private function getReadAbleExportData($theses, $indexes) {
		$export_data = array();
		foreach ($theses as $key => $thesis) {
			foreach ($indexes as $index) {
				$parts = explode('_', strtolower($index));

				if (in_array('%d', $parts)) {
					unset($parts[array_search('%d', $parts)]);
					$temp = $parts;
					$parts = array();
					foreach ($temp as $part) {
						$parts[] = $part;
					}
				}

				$count = count($parts);
				if ($count >= 2) {
					$readable_index = $parts[$count - 2] . '_' . $parts[$count - 1];
				} else {
					$readable_index = $index;
				}
				
				$export_data[$key][$readable_index] = $this->get_array_value($thesis, $index, '_');
			}
		}
		return $export_data;
	}

	public function unmakefinal($id = null) {
		if(!$id) {
			$this->Session->fault('Dotaz není validní.');
		} else {
			$this->Thesis->id = $id;
			$this->Thesis->saveField('final', '0');
			$this->Session->success('Úspěšně se podařilo změnit stav zadání');
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function overview() {
		$this->set('title_for_layout', __('Přehled přiřazených zadání'));
		$this->Paginator->settings['conditions'] = array('Thesis.user_id' => $this->Auth->user('User.id'));
		$this->Paginator->settings['order'] = array('Thesis.created DESC');
		$this->Paginator->settings['contain'] = array('Topic.Leader.full_name', 'User.full_name', 'Type.abbreviation', 'Topic.id');
		$theses = $this->Paginator->paginate('Thesis');
		$this->set(compact('theses'));
		$this->view = 'index';
	}

	public function beforeFilter() {
		parent::beforeFilter();
	}

	public function beforeRender() {
		parent::beforeRender();
	}

	public function isAuthorized($user = null) {
		if ($user['Group']['name'] == 'student') {
			if (in_array($this->action, array('overview'))) {
				return true;
			}

			if (in_array($this->action, array('download'))) {
				if (!empty($this->params['pass'][0])) {
					$thesis_id = $this->params['pass'][0];
				} else if (!empty($this->params->named['Thesis.id'])) {
					$thesis_id = $this->params->named['Thesis.id'];
				} else {
					return false;
				}

				$thesis = $this->Thesis->findById($thesis_id);
				$user_id = $user['User']['id'];

				if (!empty($thesis) && $thesis['Thesis']['user_id'] == $user_id) {
					return true;
				}

				return false;
			}
		}
		// employee can change only own thesis except from admin
		if ($user['Group']['name'] == 'employee') {
			if (in_array($this->action, array(
				'edit',
				'delete',
				'download'
			))) {

				if (!empty($this->params['pass'][0])) {
					$thesis_id = $this->params['pass'][0];
				} else if (!empty($this->params->named['Thesis.id'])) {
					$thesis_id = $this->params->named['Thesis.id'];
				} else {
					return false;
				}

				$thesis = $this->Thesis->find('first', array(
						'conditions' => array(
							'Thesis.id' => $thesis_id
						),
						'contain' => array(
							'Topic.leader_id'
						)
					)
				);
				
				$user_id = $user['User']['id'];

				if (empty($thesis)) {
					return false;
				}

				if ($thesis['Thesis']['final'] == 1 && $this->action == 'edit') {
					return false;
				}

				if ($thesis['Topic']['leader_id'] == $user_id) {
					return true;
				}
			} else if (in_array($this->action, array('add'))) {
				$queue = $this->Queue->find('first', array(
						'conditions' => array(
							'Queue.id' => $this->params->named['Queue.id']
						),
						'contain' => array(
							'Topic.leader_id'
						)
					)
				);

				if (!empty($queue) && $queue['Topic']['leader_id'] == $user['User']['id']) {
					return true;
				}

				return true;
			} else if (in_array($this->action, array('index'))) {
				return true;
			}

			return false;
		}

		if ($user['Group']['name'] == 'admin') {
			if(in_array($this->action, array('admin_index'))) {
				if(!isset($this->params->named['institute']) || $this->params->named['institute'] != $user['User']['institute_id']) {
					return false;
				}
			}

            return true;
        }

		return parent::isAuthorized($user);
	}

	private function remove_empty(&$data, $column) {

		if (empty($data)) {
			return;
		}

		foreach ($data as $key => $value) {
			if (empty($value[$column])) {
				unset($data[$key]);
			}
		}

	}
}
