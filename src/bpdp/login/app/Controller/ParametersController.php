<?php
class ParametersController extends AppController {
        
                
    public function index($id = null) {
    	
		$data = &$this->request->data;
		$this->setAdditionalData();
		
		if(!empty($id)) {
			$this->set('subtitle', 'Editace parametru');
			if(parent::edit($id, $options = Array())) {
				return $this->redirect(array('action' => 'index'));
			}
		} else if ($this->request->is(array('post', 'put'))) {
			
			if(parent::add_data('Parameter', $data)) {
				return $this->redirect(array('action' => 'index'));
			}
		}
    }    
        
    public function delete($id) {
        parent::delete($id);  
		return $this->redirect(array('action' => 'index'));
    }
    
    public function beforeFilter() {
        parent::beforeFilter();
    }
    
    public function beforeRender() {
        $title_for_browser = __("Vlastnosti vyhledáváných témat");
        $this->set(compact('title_for_browser'));
        parent::beforeRender();                
    }
    
    private function setAdditionalData() {
        $parameters = $this->Parameter->find('all');
        $this->set(compact('parameters'));
    }
    
    public function isAuthorized($user = null) {
        return parent::isAuthorized($user);
    }
}
 