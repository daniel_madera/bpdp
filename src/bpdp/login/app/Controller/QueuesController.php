<?php 

class QueuesController extends AppController {
    
    public function delete($id) {
        if($id != null && parent::isPostRequest()) {
        	$queue = $this->Queue->findById($id);
        	$this->EmailNotifier->topicRequestsRejected($queue['Queue']['topic_id'], array($id));
            $this->Queue->delete($id, false);
			$this->Session->success(__('Student byl úspěšně odstraněn.'));
        } else {
            $this->Session->fault(__('Neplatný dotaz na odstranění studenta z fronty.'));
        }
        
        return $this->redirect($this->referer());
    }
    
    public function isAuthorized($user = null) {    
     
        if ($user['Group']['name'] == 'employee') {
            
            if($this->action == 'delete') {
            	if(!isset($this->request->params['pass'][0]))
					return false;
				
                $id = $this->request->params['pass'][0];
                $this->recursive = 2;
                $queue = $this->Queue->find('first', array(
                	'contain' => array(
						'Topic.Leader.id'
					),
					'conditions' => array(
						'Queue.id' => $id
					)
				));
                
                if(!empty($queue) && $queue['Topic']['Leader']['id'] == $user['User']['id']) {
                    return true;
                } else {
                    return false;
                }                
                
            }         
            
        }

        if ($user['Group']['name'] == 'admin') {
            return true;
        }
        
        return parent::isAuthorized($user);
    }
}
