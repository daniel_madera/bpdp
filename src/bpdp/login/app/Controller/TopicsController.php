<?php

class TopicsController extends AppController {

	var $uses = array(
		'Topic',
		'User',
		'Institute',
	);
	
	var $contaionNeededForDetail = array(
		'Institute',
		'Leader',
		'Department.id',
		'Type.id',
		'Type.name',
		'Type.abbreviation',
		'Type.description',
		'Queue.User.full_name',
		'Queue',
		'Queue.Type.abbreviation',
		'Thesis.id',
		'Parameter.id',
		'Thesis.user_id',
		'Thesis.type_id'
	);

	private function resetPaginator() {
		$this->request->params['named']['page'] = 1;
	}

	public function index() {
		
		$this->set('subtitle_for_layout', __('Témata'));
		$paginator = &$this->Paginator->settings;
		$this->setAdditionalData();
		$types_all = $this->Topic->Type->find('all');
		$leaders = $this->Topic->Leader->findEmployees('list', array(
			'order' => array(
				'Leader.surname' => 'asc'
			)
		));

		// init filters
		$paginator['joins'] = array();
		if (empty($paginator['conditions'])) {
			$paginator['conditions'] = array();
		}
		// pass GET vars to POST because filter manages only POST
		if (!empty($this->request->named['leader'])) {
			$this->request->data['Topic']['leader_id'] = $this->Auth->user('User.id');
		}
		if (empty($this->request->data['Topic']['institute_id'])) {
			unset($this->request->data['Topic']['institute_id']);
		}
		// reset to page 1 after change filter
		if (isset($this->request->data['Topic']['reset'])) {
			unset($this->request->data['Topic']['reset']);
			$this->resetPaginator();
		}
		// No need to pass selected parameters when all are selected 
		if (!empty($this->request->data['Parameter']['Parameter']) && count($this->request->data['Parameter']['Parameter']) == count($this->Topic->Parameter->find('list'))) {
			unset($this->request->data['Parameter']['Parameter']);
		}
		
		if (!empty($this->request->data['Topic']['leader_id']) && count($this->request->data['Topic']['leader_id']) == count($leaders)) {
			unset($this->request->data['Topic']['leader_id']);
		}
		// default conditions
		$statusData = &$this->request->data['Topic']['status'];
		if(!empty($this->request->params['named']['archived'])) {
			$paginator['conditions']['Topic.status'] = 'archived';
		} else {
			// default states
			$allowedStatuses = array('active', 'pending', 'merged');
			if (!empty($this->request->named['leader'])) {
				$allowedStatuses[] = 'hidden';
			}
			if($this->Auth->User('Group.name') == 'student') {
				$allowedStatuses = array('active', 'pending', 'merged');
			}
			if(isset($statusData)) {
				if(!is_array($statusData)) {
					$statusData = array($statusData);
				}
				$statusData = array_intersect($statusData, $allowedStatuses);		
			} else {
				$statusData = $allowedStatuses;
			}
			if(is_array($statusData) && count($statusData) == 1) {
				$statusData = $statusData[0];
			}
			$paginator['conditions']['Topic.status'] = $statusData;	
		
		}
		if(isset($this->request->params['named']['student'])) {
			$paginator['conditions']['OR']['Thesis.user_id'] = $this->Auth->user('User.id');
			$paginator['conditions']['OR']['TopicUser.user_id'] = $this->Auth->user('User.id');
		}

		
		// apply filter
		$paginator['joins'] = array_merge_recursive($paginator['joins'], $this->getJoinsBasedOnFilterData($this->request->data));
		$paginator['conditions'] = array_merge_recursive($paginator['conditions'], $this->getConditionsBasedOnFilterData($this->request->data));
		// manage keywords searching
		if (!empty($this->request->named['keywords'])) {
			$this->request->data['Topic']['keywords'] = $keywords = $this->request->named['keywords'];
		} else {
			$keywords = &$this->request->data['Topic']['keywords'];
		}
		if (!empty($keywords) && !$this->Sql->isInteger($keywords)) {
			$keywords = strtolower($keywords);
			$string_columns = $this->Sql->getColumns('Topic', array('type' => 'string'));
			$subconditions = $this->Sql->getColumnsConditions($string_columns, $keywords);
			$subconditions['OR']['LOWER(Leader.surname) LIKE'] = '%'.$keywords.'%';		
			$paginator['conditions'] = array_replace_recursive($paginator['conditions'], $subconditions);
		} else if (!empty($keywords) && $this->Sql->isInteger($keywords)) {
			$paginator['conditions'] = array('Topic.id' => $keywords);
		}

		if(isset($keywords) && !$this->request->params['isAjax']) {
		    $this->resetPaginator();
		}		

		$paginator['group'] = 'Topic.id';
		$paginator['contain'] = $this->contaionNeededForDetail;
		$topics = $this->Paginator->paginate('Topic');
		$this->set(compact('topics', 'leaders', 'types_all'));
	}

	public function add($id = null) {
		if (parent::isPostRequest()) {
			$topic = $this->request->data;
			$topic['Topic']['leader_id'] = $this->Auth->user('User.id');
			if ($this->add_data('Topic', $topic)) {
				return $this->redirect(array(
					'action' => 'index'
				));
			}
		} else {
			if ($id != null) {
				$this->request->data = $this->Topic->find('first', array(
					'contain' => $this->contaionNeededForDetail,
					'conditions' => array(
						'Topic.id' => $id
					)
				));
				unset($this->request->data['Topic']['id']);
			}
		}
		$this->setAdditionalData();
		$this->set('title_for_layout', __('Vypsat nové téma'));
	}

	public function edit($id = null, $options = Array()) {
		$this->set('title_for_layout', __('Editovat téma'));
		$this->setAdditionalData();
		$options = array('contain' => $this->contaionNeededForDetail);
		if (parent::edit($id, $options)) {
			$this->redirect(array('action' => 'index'));
		}
	}

	public function hide($id = null) {
		$this->Topic->setStatus($id, 'hidden');
		$this->Session->success('Téma bylo úspěšně skryto.');	
		$this->redirect($this->referer());
	}

	public function show($id = null) {
		$this->Topic->setStatus($id, 'active');
		$this->Session->success('Téma bylo úspěšně obnoveno.');	
		$this->redirect($this->referer());
	}

	public function delete($id) {
		$topic = $this->Topic->findById($id);
		
		if(!empty($topic)) {
			if(in_array($topic['Topic']['status'], array('merged'))) {
				$this->Session->fault('Nelze odstranit téma, ke kterému je přiřazeno zadání.');
				return $this->redirec($this->referer());
			} 
		}
		
		$this->EmailNotifier->topicRequestsRejected($id);
		$this->Topic->Queue->deleteAllByTopicId($id);

		parent::delete($id);
		$this->redirect(array('action' => 'index'));
	}

	public function request($id = null) {
		$title_for_layout = __('Požádání o téma');
		$topic = $this->Topic->find('first', array(
			'conditions' => array(
				'Topic.id' => $id
			),
			'contain' => $this->contaionNeededForDetail
		));
		$thesesMerged = $this->Topic->Thesis->find('all', array(
            'conditions' => array(
                'user_id' => $this->Auth->user('User.id'),
                'archived' => 0,
            ),
            'contain' => array('Type.abbreviation')
        ));
		$this->set(compact('title_for_layout', 'topic', 'thesesMerged'));
		$this->setAdditionalData();

		if (empty($topic)) {
			throw new NotFoundException(__('Data o tématu nebyla nalezena.'));
		}

		if (parent::isPostRequest()) {
			$this->Topic->Queue->set($this->request->data);
			if (!$this->Topic->Queue->validates()) {
				return;
			}
			if ($this->Topic->Queue->queueExists($this->Auth->user('User.id'), $topic['Topic']['id'], $this->request->data['Queue']['type_id'])) {
				$this->Session->fault('O toto téma jste již zažádal. Vyčkejte na vyjádření od vyučujícího.');
				return $this->redirect(array('action' => 'index'));
			}
			if (!$this->Topic->Queue->canStudentRequestTopic($this->Auth->user('User.id'), $this->request->data['Queue']['type_id'])) {
				$this->Session->fault('Máte vyčerpaný limit zažádání. Vyčkejte na vyjádření od vyučujících.');
				return $this->redirect(array('action' => 'index'));
			}
			if ($this->Topic->Thesis->existsByUserType($this->Auth->user('User.id'), $this->request->data['Queue']['type_id'])) {
				$this->Session->fault('Práci tohoto typu máte již přiřazenou. Pokud nesouhlasíte s tímto tématem, můžete zkusit požádat o zrušení vedoucího práce.');
				return $this->redirect(array('action' => 'index'));
			}
			$this->request->data['Queue']['user_id'] = $this->Auth->user('User.id');
			$this->request->data['Queue']['topic_id'] = $topic['Topic']['id'];

			$this->Topic->Queue->create();
			if ($this->Topic->Queue->save($this->request->data)) {
				$this->Session->success('Byl jste zařazen mezi studenty, kteří požádali o stejné téma. Vedoucí práce Vás přiřadí k tématu nebo odmítne. O rozhodnutí budete informován emailem.');
				$this->EmailNotifier->topicRequested($this->Topic->Queue->id);
				return $this->redirect(array('action' => 'index'));
			}

		}
	}

	public function merge() {
		if (parent::isPostRequest()) {
			$this->redirect(array_merge(array(
				'controller' => 'theses',
				'action' => 'add'
			), $this->params['named']));
		}
	}

	private function setAdditionalData() {
		$types = $this->Topic->Type->find('list');
		$parameters = $this->Topic->Parameter->find('list');
		$departments = $this->Topic->Department->findSubDepartments();
		$departments_abbreviation = $this->Topic->Department->findSubDepartments('list','abbreviation');

		$institutes = $this->Institute->find('list', array('fields' => array(
				'id',
				'name'
			)));
		$userQueues = $this->Topic->Leader->getCountUserQueues($this->Auth->user('User.id'));
		$typesAll = $this->Topic->Type->find('all', array('fields' => array(
				'Type.id',
				'Type.name',
				'Type.department_id'
			)));
		
		$this->set(compact('types', 'departments', 'departments_abbreviation', 'parameters', 'institutes', 'userQueues', 'typesAll'));
	}

	public function beforeFilter() {
		if($this->action == 'index' || empty($this->action)) {
			if(empty($this->request->params['named'])) {
				return $this->redirect($this->getInitRedirect());	
			}
		}
		parent::beforeFilter();
	}

	public function beforeRender() {
		$this->helpers[] = 'Department';
		$this->helpers[] = 'Status';
		$this->helpers[] = 'Topic';
		parent::beforeRender();
	}

	public function isAuthorized($user = null) {
		if($user['Group']['name'] == 'student') {
			if(isset($this->request->params['named']['archived'])) {
				return false;
			}
		} 
		if (in_array($this->action, array(
			'view',
			'index',
			'merge'
		))) {
			return true;
		}

		// employee can change only own Topic except from admin
		if ($user['Group']['name'] == 'employee') {

			if (in_array($this->action, array(
				'edit',
				'delete',
				'hide',
				'show'
			))) {
				if (!empty($this->params['pass'][0])) {
					$topic_id = $this->params['pass'][0];
				} else if (!empty($this->params->named['Topic.id'])) {
					$topic_id = $this->params->named['Topic.id'];
				}

				$Topic = $this->Topic->findById($topic_id);
				$user_id = $user['User']['id'];

				if (!empty($Topic) && $Topic['Topic']['leader_id'] == $user_id) {
					return true;
				}

				return false;

			}

			if (in_array($this->action, array('add'))) {
				return true;
			}
			
		} else if ($user['Group']['name'] == 'student') {
			if (in_array($this->action, array('request'))) {
				return true;
			}
		}

		if ($user['Group']['name'] == 'admin') {
            return true;
        }

		return parent::isAuthorized($user);
	}

	private function getJoinsBasedOnFilterData($data) {
		$paginator = array('joins' => array());
		$models = array(
			'Type',
			'Department',
			'Parameter'
		);
		foreach ($models as $model) {
			if (!empty($data[$model][$model])) {
				$paginator['joins'][] = $this->Topic->getJoinManyToManyOnSpecificId('INNER', $model, $data[$model][$model]);
			}
		}
		if (isset($this->request->named['student'])) {
			$paginator['joins'][] = $this->Topic->getJoinManyToManyOnSpecificId('LEFT', 'User', $this->Auth->user('User.id'), array('table' => 'queues'));
		}
		return $paginator['joins'];
	}

	private function getConditionsBasedOnFilterData($data) {
		$paginator = array('conditions' => array());
		$columns = array(
			'leader_id',
			'institute_id'
		);
		foreach ($columns as $column) {
			if (isset($data['Topic'][$column])) {
				if(is_array($data['Topic'][$column]) && count($data['Topic'][$column]) == 1) {
					$data['Topic'][$column] = $data['Topic'][$column][0];
				}
				$paginator['conditions']['Topic.' . $column] = $data['Topic'][$column];
			}
		}
		return $paginator['conditions'];
	}
	
	public function getInitRedirect() {
		$param['controller'] = 'topics';
		$param['action'] = 'index';
		$user = $this->Auth->user();
		if(empty($user)) {
			return $this->redirect(array('controller' => 'users', 'action' => 'login'));
		}
		
		if ($user['Group']['name'] == 'student') {
			$param['all'] = 1;
		} else {		
			$param['leader'] = $user['User']['id'];
		} 
		return $param;
	}

}
