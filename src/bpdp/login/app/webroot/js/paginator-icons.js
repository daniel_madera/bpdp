function paginate_icons_sort() {
	var icon = "fa fa-sort-";
	var btns = $('.pagination-icons').find('.btn, .btn');
	var btn = $('.pagination-icons').find('.btn.asc, .btn.desc');
	if(btn.hasClass('numeric')) {
		icon += 'numeric-';
	} else if(btn.hasClass('alpha')) {
		icon += 'alpha-';
	}
	if(btn.hasClass('desc')) {
		icon += 'desc';
	} else if(btn.hasClass('asc')) {
		icon += 'asc';
	}
	btn.attr('data-toggle', 'button');
	btn.find("i").attr("class", icon);
}

$( document ).ready(paginate_icons_sort); 