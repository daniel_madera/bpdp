$( document ).ready(function(event) {
	$('.dropdown-toggle').click(function(e) {
	  e.preventDefault();
	  setTimeout($.proxy(function() {
	    if ('ontouchstart' in document.documentElement) {
	      $(this).siblings('.dropdown-backdrop').off().remove();
	    }
	  }, this), 0);
	});
	$('.js-confirm-op').removeAttr('onclick');
	$('.js-confirm-op').click(function(e){
		e.preventDefault();
	    var form_id = $(this).attr('data-submit');
	    if(form_id) {
	    	var form = $(form_id);
	    } else {
	    	var form = $(this).prev();
	    }	    
	    var text = $(this).attr('data-text');
	    var title = $(this).attr('data-title');
	    var body = '<h4>' + title + '</h4>' + text;
	    bootbox.confirm(body, function(result) {
	    	if(result)
	    		form.submit();
	    }); 
	    return false;
	});
	
	$('#types-select-div').find('input[type="checkbox"]').click(
		function(e) {
			var data = $(this).attr('data-department');
			var inputSel = 'input[data-department="' + data + '"]';
			if(!$(this).is(':checked')) {
				if(!$('#types-select-div').find(inputSel + ':checked').length){
					$('#departments-select-div').find(inputSel).parent().fadeOut();
					$('#departments-select-div').find(inputSel).removeAttr('checked');
				}
			} else {
				$('#departments-select-div').fadeIn();
				$('#departments-select-div').find(inputSel).parent().fadeIn();
			}
		}
	);
	$('#departments-select-div').hide();
	$('#departments-select-div').find('input[type="checkbox"]').parent().hide();
	$('#types-select-div').find('input[type="checkbox"]:checked').each(
		function(e) {
			var data = $(this).attr('data-department');
			$('#departments-select-div').find('input[data-department="' + data + '"]').parent().show();
			$('#departments-select-div').show();
		}
	);

	$('#teachers-select-all').click(function(e) {
		$('#topics-filter-form-leader').find('input').prop('checked', true);
		var first = $('#topics-filter-form-leader input[type="checkbox"]:first');
		first.removeAttr('checked');
		first.trigger('click');
	});

	$('#teachers-unselect-all').click(function(e) {
		$('#topics-filter-form-leader').find('input').removeAttr('checked');
		var first = $('#topics-filter-form-leader input[type="checkbox"]:first');
		first.prop('checked', true);
		first.trigger('click');
	});
	
});