	
function create_input(name, value) {
	var div = document.createElement('div');
    
    var button = document.createElement('button');
    button.className = "close input-close"; 
    button.innerHTML = '&times;';
    button.setAttribute('type', 'button');
    button.setAttribute('onClick', 'this.parentNode.remove(); return false;');
        
    var input = document.createElement('input');
    
    //name = 'data[' + name.replace('.', '][') + ']';
    
    input.setAttribute('name', 'column-id-' + name);
    input.setAttribute('value', value);
    input.setAttribute('type', 'text');
    input.setAttribute('readonly', 'readonly');
    
    div.appendChild(button);
    div.appendChild(input);
    
    return div;
}
