function requirements_add_button_clicked() {
	var index = parseInt($("#requirements-counter").attr("value"));
	var id = "requirements-item-input-" + index;
	$("#requirements-container").append(
	'<div class="requirement-item" id="requirement-item-' + index + '"><button type="button" class="close" data-dismiss="alert">&times;</button><label for="'+id+'">'+(index+1)+'. </label><textarea row="2" class="" name="data[Requirement][' + index + '][text]" id="' + id + '" maxlength="200" type="text"></textarea></div>');
	$("#requirements-counter").attr('value', index + 1);
}

function ucfirst(string) {
    return string.charAt(0).toUpperCase() + string.substr(1);
}

function add_input_button_clicked(identificator, input_rows_count) {
	
	var container_id = identificator + '-container';
	var counter_id = identificator + '-counter';
	
	var index = parseInt($("#" + counter_id).attr("value"));
	var input_id = identificator + "-item-input-" + index;
	var div_id = identificator + "-item-" + index;
	var div_class = identificator + "-item";
	var counter = id + 1;
	var input_name = 'data[' + ucfirst(identificator) + '][' + index + '][text]';
	
	var div = document.createElement('div');
	div.id = div_id;
	div.className = div_class;
	
	var button = document.createElement('button');
	button.className = "close";	
	button.innerHTML = '&times;';
	button.setAttribute('type', 'button');
	
	var label = document.createElement('label');
	label.innerHTML = counter + '. ';
	label.setAttribute('for', input_id);
	
	var textarea = document.createElement('textarea');
	textarea.id = input_id;
	textarea.setAttribute('name', input_name);
	textarea.setAttribute('maxlength', 800);
	textarea.setAttribute('type', 'text');
	textarea.setAttribute('row', input_rows_count);
	
	div.appendChild(button);
	div.appendChild(label);
	div.appendChild(textarea);
		
	$("#" + container_id).append(div);
	$("#" + counter_id).attr('value', index + 1);
}


function literature_add_button_clicked() {
	var index = parseInt($("#literature-counter").attr("value"));
	var id = "literature-item-input-" + index;
	$("#literature-container").append(
	'<div class="literature-item alert-dismissable" id="literature-item-' + index + '"><button type="button" class="close" data-dismiss="alert">&times;</button><label for="'+id+'">'+(index+1)+'. </label><textarea row="2" class="" name="data[Literature][' + index + '][text]" id="' + id + '" maxlength="200" type="text"></textarea></div>');
	$("#literature-counter").attr('value', index + 1);
}

//function requirements_delete_clicked() {
//	var index = parseInt(substring($(this).parent().attr("id")), 18);
//	var count = parseInt($("#requirements-counter").attr("value"));
//	var real_index, next_value;
//	for(var i = index; i < count - 1; i++) {
//		real_index = i + 1;
//		$("#requirements-item-" + real_index).find("label").val(real_index + ".");
//		$("#requirements-item-" + real_index).attr("id", "requirements-item-" + i);
//		$("#requirements-item-input-" + real_index).attr("id", "requirements-item-input-" + i);
//		
//	}
//}
//
