function ucfirst(string) {
    return string.charAt(0).toUpperCase() + string.substr(1);
}

function get_indentifiers(identifier, index) {
	var identifiers = new Array();
	identifiers["container_id"] = identifier + '-container';
	identifiers["counter_id"] = identifier + '-counter';
	
	identifiers["input_id"] = identifier + "-item-input-" + index;
	identifiers["div_id"] = identifier + "-item-" + index;
	identifiers["div_class"] = identifier + "-item";
	identifiers["input_name"] = 'data[' + ucfirst(identifier) + '][' + index + '][text]';
	
	return identifiers;
}
 
function add_input_button_clicked(identifier, input_rows_count, limit, text) {
		
	var index = parseInt($("#" + identifier + '-counter').attr("value"));
	var count = index + 1;
	
	//if(count >= limit) {
	//	$("#" + identifier + '-add-button').hide();
	//}
	
	identifiers = get_indentifiers(identifier, index);
	
	var div = document.createElement('div');
	div.id = identifiers["div_id"];
	div.className = identifiers["div_class"];
	
	var button = document.createElement('button');
	button.className = "close input-close";	
	button.innerHTML = '&times;';
	button.setAttribute('type', 'button');
	button.setAttribute('onClick', 'remove_input(this, ' + limit + '); return false;');
	
	
	var label = document.createElement('label');
	label.innerHTML = count + '.';
	label.setAttribute('for', identifiers["input_id"]);
	
	var textarea = document.createElement('textarea');
	textarea.id = identifiers["input_id"];
	textarea.setAttribute('name', identifiers["input_name"]);
	textarea.setAttribute('maxlength', 800);
	textarea.setAttribute('type', 'text');
	textarea.setAttribute('rows', identifiers["input_rows_count"]);
	textarea.innerHTML = text;
	
	div.appendChild(button);
	div.appendChild(label);
	div.appendChild(textarea);
		
	$("#" + identifiers["container_id"]).append(div);
	$("#" + identifiers["counter_id"]).attr('value', count);
}


function remove_input(button, limit) {
	var split_values = button.parentNode.id.split("-");
	var identifier = split_values[0];
	var index = split_values[2];
	var counter_id = identifier + "-counter";
	var count = parseInt($("#" + counter_id).attr("value"));
	
	//if(count <= limit) {
	//	$("#" + identifier + '-add-button').show();
	//}
	
	var next_index, id_part = identifier + "-item", div, cur_ids, next_ids;
	var id = get_indentifiers(identifier, index);
	
	$("#" + id["div_id"]).remove();
	
	for(var i = index - 1; i < count - 1; i++) {
		cur_ids = get_indentifiers(identifier, i);
		next_ids = get_indentifiers(identifier, i + 1);
		next_index = i + 1;
		
		$("#" + next_ids["input_id"]).attr('name', cur_ids["input_name"]);
		var label = $("#" + next_ids["div_id"]).find("label");
		label.text(next_index + ".");
		label.attr('for', cur_ids["input_id"]);
		$("#" + next_ids["input_id"]).attr('id', cur_ids["input_id"]);
		$("#" + next_ids["div_id"]).attr('id', cur_ids["div_id"]);
	}
	$("#" + counter_id).attr('value', count - 1);
}

function remove_empty_inputs (identifier, limit) {
	$("#" + identifier + "-container").find('input:text','textarea').each( function(){
		  var result = $(this).val();
		  if(result == "") {
			  var button = $(this).parent().find('button');
			  remove_input(button, limit);
		  }
		  
	});
	
}

