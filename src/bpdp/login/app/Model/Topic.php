<?php

class Topic extends AppModel {
    
	public $actsAs = array('Containable');
	//public $recursive = 1;
		
    public $belongsTo = array(
        'Leader' => array(
            'className' => 'User',
            'foreignKey' => 'leader_id',
            'fields' => array(
            	'id', 'full_name', 'email'								
			),
			'counterCache' => true,
			'counterScope' => array(
              'Topic.archived' => 1
            )
		),
		'Institute'
    );
    
    public $hasOne = 'Thesis';
	
	public $hasMany = array(
        'Queue',
    );
	
	public $hasAndBelongsToMany = array(
		'Department' => array(
			'fields' => array('Department.id', 'Department.name', 'Department.abbreviation'),
		), 
		'Type' => array(
			'fields' => array('Type.id', 'Type.name', 'Type.abbreviation', 'Type.limit'),
		),
		'Parameter',
	);
	
	public $validate = array(
        'name' => array(
        	'maxLength' => array(
	            'rule' => array('maxLength', 200),
	            'message' => 'Jméno smí být maximálně na %d znaků.',
	            'required' => true
	        ),
	        'notEmpty' => array(
				'rule'    => 'notEmpty',
	            'message' => 'Jméno nesmí být prázdné.',
	            'required' => true
			)
        ),
        'abstract' => array(
        	'maxLength' => array(
	            'rule' => array('maxLength', 1500),
	            'message' => 'Abstrakt smí být maximálně na %d znaků.',
	            'required' => true
	        ),
	        'notEmpty' => array(
				'rule'    => 'notEmpty',
	            'message' => 'Abstrakt nesmí být prázdný.',
	            'required' => true
			)
		),
		'requirements' => array(
			'rule' => array('maxLength', 500),
			'message' => 'Požadavky smí být maximálně na %d znaků.',
			'allowEmpty' => true
		),
		'literature' => array(
			'rule' => array('maxLength', 500),
			'message' => 'Literatura smí být maximálně na %d znaků.',
			'allowEmpty' => true
		),
		'consultant' => array(
			'rule' => array('maxLength', 255),
			'message' => 'Konzultant smí být maximálně na %d znaků.',
			'allowEmpty' => true			
		)
	);
	
	function beforeValidate($options = Array()) {
		if (empty($this->data['Department']['Department'])) {
			$this->invalidate('departments-count');
			$this->Department->invalidate('Department', 'Vyberte alespoň jeden obor.');
		}
		
		if (empty($this->data['Type']['Type'])) {
			$this->invalidate('types-count');
			$this->Type->invalidate('Type', 'Vyberte alespoň jeden typ práce.');			
		}
		
	}

    public $virtualFields = array(
        'status' => '
        	CASE
        		WHEN Topic.archived THEN "archived"
        		WHEN Topic.state = 0 THEN "hidden"
        		WHEN (SELECT queues.id FROM queues WHERE queues.topic_id = Topic.id LIMIT 1) THEN "pending"
        		WHEN (SELECT theses.id FROM theses WHERE theses.topic_id = Topic.id LIMIT 1) THEN "merged"
			ELSE 
				"active"
			END
			',
    );
	
	public $stateStatuses = array(
		0 => 'hidden',
	    1 => 'active',
	);
		
	public function setStatus($id, $status) { 
		if(!$id) {
			throw new InternalErrorException('Není nastaveno žádné ID tématu, pro označení stavu.');
		} 
		$this->validateStatus($status);
		$this->id = $id;
		$this->saveField('state', $this->getState($status));
	}
	
	public function validateStatus($status) {
		if(!in_array($status, $this->stateStatuses)) {
			throw new InternalErrorException('Neplatný stav tématu.');
		}
		return true;
	}
        
	public function getStatus($state) { 
		return $this->stateStatuses[$state];
	}
	
	public function getState($status) {
		$this->validateStatus($status);
		return array_search($status, $this->stateStatuses);		
	}
		
	public function isOwnedBy($topic_id, $leader_id) {
    	return $this->field('id', array('id' => $topic_id, 'leader_id' => $leader_id)) !== false;
    }

    public function archiveAll() {        
        $this->query('UPDATE topics SET topics.archived=1 WHERE topics.id NOT IN (SELECT theses.topic_id FROM theses WHERE 1) AND topics.id NOT IN (SELECT DISTICT queues.topic_id FROM queues WHERE 1);');
    }
}