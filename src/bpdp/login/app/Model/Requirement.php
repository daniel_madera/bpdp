<?php

class Requirement extends AppModel {
    
    public function deleteByThesisId($thesis_id) {
        
        $conditions = array(
            'Requirement.thesis_id' => $thesis_id
        );
        
        $this->deleteAll($conditions, false);        
    }
}
    