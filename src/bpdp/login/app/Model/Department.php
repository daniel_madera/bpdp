<?php

class Department extends AppModel {
	public $actsAs = array('Tree');

	public function findSubDepartments($type = 'list', $column = 'name') {

		$function = sprintf('get%s', ucfirst($type));

		return $this -> $function($column);
	}

	public $validate = array(
		'name' => array(
			'required' => array(
				'rule' => 'notEmpty',
				'message' => 'Název musí být vyplněn.'
			),
			'size' => array(
				'rule' => array(
					'between',
					5,
					50
				),
				'message' => 'Název by měl být v rozsahu %d až %d znaků.'
			)
		),
		'abbreviation' => array(
			'requir' => array(
				'rule' => 'notEmpty',
				'message' => 'Zkratka musí být vyplněna.'
			),
			'size' => array(
				'rule' => array(
					'between',
					2,
					8
				),
				'message' => 'Zkratka by měla být v rozsahu %d až %d znaků.'
			)
		)
	);

	public function getList($column = 'name') {

		$parents = $this -> find('list', array('conditions' => array('parent_id' => null))); 
		$concat_departments = array();

		foreach ($parents as $parent_id => $parent_name) {
			$departments = $this -> children($parent_id);
			foreach ($departments as $department) {
				$concat_departments[$department['Department']['id']] = array(
					'name' => sprintf('%s -> %s', $parent_name, $department['Department'][$column]),
					'value' => $department['Department']['id'],
					'data-department' => $parent_id
				);
			}
		}

		return $concat_departments;
	}
	
	public function alreadyExists($department) {
		$record = $this->find('first', array(
			'conditions' => array(
				'AND' => array(
					'Department.parent_id' => $department['Department']['parent_id'],
					'OR' => array(
						'Department.name' => $department['Department']['name'],
						'Department.abbreviation' => $department['Department']['abbreviation']
				)
			)
		)));
		
		return !empty($record);				
	}
	
	
}
