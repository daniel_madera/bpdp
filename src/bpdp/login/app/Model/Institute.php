<?php

class Institute extends AppModel {
	public $validate = array(
		'name' => array(
			'required' => array(
				'rule' => 'notEmpty',
				'message' => 'Název musí být vyplněn.'
			),
			'size' => array(
				'rule' => array(
					'between',
					5,
					100
				),
				'message' => 'Název by měl být v rozsahu %d až %d znaků.'
			)
		),
		'abbreviation' => array(
			'requir' => array(
				'rule' => 'notEmpty',
				'message' => 'Zkratka musí být vyplněna.'
			),
			'size' => array(
				'rule' => array(
					'between',
					2,
					5
				),
				'message' => 'Zkratka by měla být v rozsahu %d až %d znaků.'
			)
		)	
	);
}