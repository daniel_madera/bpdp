<?php

class Parameter extends AppModel {
	public $validate = array(
		'name' => array(
			'required' => array(
				'rule' => 'notEmpty',
				'message' => 'Název musí být vyplněn.'
			),
			'size' => array(
				'rule' => array(
					'between',
					1,
					30
				),
				'message' => 'Název by měl být v rozsahu %d až %d znaků.'
			)
		), 
		'description' => array(
			'size' => array(
				'rule' => array(
					'maxLength',
					200
				),
				'message' => 'Popis může mít maximálně %d znaků.',
				'allowEmpty' => true
			)
		)		
	);
}
 