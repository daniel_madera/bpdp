<?php

class Type extends AppModel {
	public $belongsTo = array(
		'Department'
	);
	
	public $validate = array(
		'name' => array(
        	'maxLength' => array(
	            'rule' => array('maxLength', 200),
	            'message' => 'Jméno smí být maximálně na %d znaků.',
	            'required' => true
	        ),
	        'notEmpty' => array(
				'rule'    => 'notEmpty',
	            'message' => 'Jméno nesmí být prázdné.',
	            'required' => true
			)
        ),
        'description' => array(
        	'maxLength' => array(
	            'rule' => array('maxLength', 500),
	            'message' => 'Popis smí být maximálně na %d znaků.',
	            'required' => true
	        ),
	        'notEmpty' => array(
				'rule'    => 'notEmpty',
	            'message' => 'Popis nesmí být prázdný.',
	            'required' => true
			)
		),
		'abbreviation' => array(
			'requir' => array(
				'rule' => 'notEmpty',
				'message' => 'Zkratka musí být vyplněna.'
			),
			'size' => array(
				'rule' => array(
					'between',
					2,
					5
				),
				'message' => 'Zkratka by měla být v rozsahu %d až %d znaků.'
			)
		), 
		'range' => array(
        	'maxLength' => array(
	            'rule' => array('maxLength', 30),
	            'message' => 'Rozsah smí být maximálně na %d znaků.',
	            'required' => true
	        ),
	        'notEmpty' => array(
				'rule'    => 'notEmpty',
	            'message' => 'Rozsah nesmí být prázdný.',
	            'required' => true
			)
		),
		'department_id' => array(
			'notEmpty' => array(
				'rule'    => 'notEmpty',
	            'message' => 'Vyberte skupinu oborů.',
	            'required' => true
			)
		)
	);
}
 