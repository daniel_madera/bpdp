<?php
App::uses('Type', 'Model');
class User extends AppModel {

    public function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->virtualFields['name'] = sprintf(
            'if(%s.forename is not null AND TRIM(%s.forename) <> "" AND %s.surname is not null AND TRIM(%s.surname) <> "", CONCAT(%s.surname, " ", %s.forename), "")',
            $this->alias, $this->alias, $this->alias, $this->alias, $this->alias, $this->alias
        );
        $this->virtualFields['full_name'] = sprintf(
            'if(%s.title_behind_name is not null AND TRIM(%s.title_behind_name) <> "", 
                CONCAT(if(%s.title_before_name is not null AND TRIM(%s.title_before_name) <> "", 
                    CONCAT(%s.title_before_name, " ", CONCAT(%s.forename, " ", %s.surname)), CONCAT(%s.forename, " ", %s.surname)), ", ", %s.title_behind_name), 
                        if(%s.title_before_name is not null AND TRIM(%s.title_before_name) <> "", 
                            CONCAT(%s.title_before_name, " ", CONCAT(%s.forename, " ", %s.surname)), CONCAT(%s.forename, " ", %s.surname)))', 
                $this->alias, $this->alias, $this->alias, $this->alias, $this->alias, $this->alias,
                $this->alias, $this->alias, $this->alias, $this->alias, $this->alias, $this->alias,
                $this->alias, $this->alias, $this->alias,
                $this->alias, $this->alias, $this->alias, $this->alias, $this->alias, $this->alias,
                $this->alias, $this->alias
        );
    }

	public $actsAs = array('Containable'); 
	 
	public $validate = array(
        // 'forename' => array(
        //     'rule'    => array('between', 2, 100),
        //     'message' => 'Jméno musí být nejméně 2 a maximálně 100 znaků.',
        //     'allowEmpty' => false
        // ),
        // 'surname' => array(
        //     'rule'    => array('between', 5, 100),
        //     'message' => 'Příjmení musí být nejméně 5 a maximálně 100 znaků.',
        //     'allowEmpty' => false
        // ),
        'title_before_name' => array(
            'rule' => array('maxLength', 25),
            'message' => 'Tituly mouhou mít délku maximálně %d znaků.',
            'allowEmpty' => true    
        ),
        'title_behind_name' => array(
            'rule' => array('maxLength', 15),
            'message' => 'Tituly mouhou mít délku maximálně %d znaků.',
            'allowEmpty' => true    
        )     
	);
	
    public $belongsTo = array(
        'Group', 'Institute'
    );	
             
    public $hasMany = array(
        'Queue', 'Topic' => array(
            'foreignKey' => 'leader_id',
        )
    );
    
    public function getName($email) {
        $result = null;
        preg_match("/[a-z.]+/", strtolower($email), $result);
        $username = split('[.-]', $result[0]);
        
        if(count($username) < 1) {
            return $email;
        }   

        foreach($username as &$name) {
            $name = ucfirst($name);
        }

        return $username;
    }
    
    public function hasName($user) {
        
        if(!isset($user)) {
            return false;
        }
                
        return !empty($user['User']['name']);
    }  
	
	public function getCountUserQueues($user_id) {
		if(!$user_id) {
            throw new NotFoundException('Nepředali jste uzivatelske id');
        }
		$typeModel = new Type();
		$types = $typeModel->find('list', array(
			'fields' => array(
				'id', 'abbreviation'
			)
		));
		
		$userQueues = array();
		foreach($types as $type_id => $type_name) {
			$count = $this->Queue->find('count', array(
				'conditions' => array(
					'Queue.type_id' => $type_id,
					'Queue.user_id' => $user_id 
				)
			));
			if($count > 0) {
				$userQueues[$type_name] = $count;
			}
		}   
		return $userQueues;
	}
	
	public function findEmployees($type, $options = array()) {
		$defaultOptions = array(
			'conditions' => array(
				'Group.name' => array(
					'employee',
					'admin',
                    'superuser'
			)),
			'contain' => array(
				'Group.name'
			),
            'fields' => array(
                'id', 'name'
            )
		);
		
		return $this->find($type, array_merge_recursive($defaultOptions, $options));
	}
}  