<?php

class Queue extends AppModel { 
	
	public $actsAs = array('Containable'); 
	
	public $belongsTo = array(
		'Topic',
		'User',
		'Type'
	);

	public $validate = array('type_id' => array(
			'rule' => 'notEmpty',
			'message' => 'Vyberte prosím typ práce.'
		));

	public function findQueue($user_id, $topic_id, $options = array()) {
		$options['conditions'] = array(
			'Queue.user_id' => $user_id,
			'Queue.topic_id' => $topic_id,
		);
		$queue = $this->find('first', $options);
		return $queue;
	}

	/**
	 * @param user_id
	 * returns array where key is Type.id an value is array of topics
	 *
	 */
	public function get($user_id) {

		$types = $this->Type->find('list', array('fields' => array('id')));

		$result = array();

		foreach ($types as $type) {
			$queues = $this->find('list', array('conditions' => array(
					'user_id' => $user_id,
					'type_id' => $type['id']
				)));

			$result[$type['id']] = array();
			foreach ($queues as $queue) {
				$result[$type['id']][] = $this->Topic->findById($queue['topic_id']);
			}
		}

		return $result;
	}

	/**
	 * @param user_id, type_id
	 *
	 * returns array where key is Type.id and value is count of queues
	 *
	 */
	public function getCounts($user_id) {

		$queues = $this->get($user_id);

		foreach ($queues as $type_id => $topics) {
			$queues[$type_id] = count($topics);
		}

		return $queues;
	}
	
	public function canStudentRequestTopic($user_id, $type_id) {
		$count = $this->find('count', array(
			'conditions' => array(
				'user_id' => $user_id,
				'type_id' => $type_id
		)));
		
		return $count < Configure::read('Application.limit.requests.student');
	}

	public function isLimitExceeded($user_id, $type_id) {
		
		$counts = $this->getCounts($user_id);
				
		if(empty($counts)) {
			throw new InternalErrorException('Špatná vstupní data.');
		}

		return $counts[$type_id] > Configure::read('Application.limit.requests.student');
	}

	public function queueExists($user_id, $topic_id, $type_id) {

		$options = array('conditions' => array(
				'user_id' => $user_id,
				'topic_id' => $topic_id,
				'type_id' => $type_id
			));

		return $this->find('count', $options) > 0;
	}

	public function removeAll() {
		$this->query('TRUNCATE TABLE `queues`');
	}

	public function deleteAllByUserId($student_id) {
		$conditions = array('AND' => array(
			'Queue.user_id' => $student_id,
		));
		$this->deleteAll($conditions);
	}
	
	public function deleteAllByTopicId($topic_id) {
		$conditions = array('AND' => array(
			'Queue.topic_id' => $topic_id,
		));
		$this->deleteAll($conditions);
	}
	public function deleteAllByUserIdTypeId($student_id, $type_id) {
		$conditions = array('AND' => array(
			'Queue.user_id' => $student_id,
			'Queue.type_id' => $type_id,
		));
		$this->deleteAll($conditions);		
	}
}
