<?php

class Literature extends AppModel {
    public function deleteByThesisId($thesis_id) {
        
        $conditions = array(
            'Literature.thesis_id' => $thesis_id
        );
        
        $this->deleteAll($conditions, false);        
    }
}
    