<?php

class Thesis extends AppModel {
	
	public $actsAs = array('Containable');
	
	public $hasMany = array(
		'Requirement',
		'Literature'
	);
	public $belongsTo = array(
		'User',
		'Type',
		'Topic'
	);
	
	public $virtualFields = array(
		'status_text' => '
            CASE 
            	WHEN Thesis.archived = 1 THEN "Archivované"
            	WHEN Thesis.final = 0 THEN "Rozpracované"
            	WHEN Thesis.final = 1 THEN "Dokončené" 
            END'
	); 
	
	public function existsByUserType($user_id, $type_id) {
		$options = array(
			'conditions' => array(
				'Thesis.user_id' => $user_id, 
				'Thesis.type_id' => $type_id,
				'Thesis.archived' => 0
			),
			'fields' => array('Thesis.id')
		);
		$data = $this->find('first', $options);
		return !empty($data);
	}

	public $validate = array( 
		'name' => array(
        	'maxLength' => array(
	            'rule' => array('maxLength', 200),
	            'message' => 'Název smí být maximálně na %d znaků.',
	            'required' => true,
	            'allowEmpty' => false
	        ),
	        'notEmpty' => array(
				'rule'    => 'notEmpty',
	            'message' => 'Název nesmí být prázdný.',
	            'required' => true,
	            'allowEmpty' => false
			)
        ),
		'name_en' => array(
        	'maxLength' => array(
	            'rule' => array('maxLength', 200),
	            'message' => 'Anglický název smí být maximálně na %d znaků.',
	            'allowEmpty' => true
	        ),
	        'notEmpty' => array(
				'rule'    => 'notEmpty',
	            'message' => 'Anglický název nesmí být prázdný.',
	            'allowEmpty' => true
			)
        ),
        'requirement-counter' => array(
        	'minCount' => array(
				'rule'       => array('minCount', 3, 'Requirement'),
	            'message'    => 'Vyplňte alespoň %d zásady pro vypracování.',
	            'allowEmpty' => true
			),
			'manCount' => array(
				'rule'       => array('maxCount', 20, 'Requirement'),
	            'message'    => 'Počet zásad pro vypracování nesmí být větší než %d.',
	            'allowEmpty' => true
			)
		),
		'literature-counter' => array(
        	'minCount' => array(
				'rule'       => array('minCount', 1, 'Literature'),
	            'message'    => 'Vyplňte alespoň %d doporučenou literaturu.',
	            'allowEmpty' => true
			),
			'manCount' => array(
				'rule'       => array('maxCount', 20, 'Literature'),
	            'message'    => 'Počet doporučených literatur nesmí být větší než %d.',
	            'allowEmpty' => true
			)
		),
		'created_to' => array(
			'rule'       => 'date',
            'message'    => 'Zadejte validní datum.',
            'allowEmpty' => true
		),
		'created_from' => array(
			'rule'       => 'date',
            'message'    => 'Zadejte validní datum.',
            'allowEmpty' => true
		)
	);
	
	function minCount($check, $limit, $index) {
		return !empty($this->data[$index]) && count($this->data[$index]) >= $limit;
	}
	
	function maxCount($check, $limit, $index) {
		return !empty($this->data[$index]) && count($this->data[$index]) <= $limit;
	}

	public function archiveAll() {
		$this->query('DELETE FROM theses WHERE final="0"');
		$this->query('UPDATE theses JOIN topics ON topics.id = theses.topic_id SET theses.archived=1,topics.archived=1 WHERE 1');
	}
}
