<?php
/**
 * Application model for CakePHP.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Model', 'Model');
/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {
	
	public $recursive = -1;
		
	public function alreadyExists($conditions) {
		$data = $this->find('first', array('conditions' => $conditions));
		
		return $data;
	}
	
	public function getJoinManyToMany($type, $model, $options = array()) {
		$models = array($model, $this->name);
		sort($models);
		$modelName = $models[0].$models[1];
		$tables = array_map('Inflector::pluralize', array_map('strtolower', $models));
		$tableName = $tables[0].'_'.$tables[1];
		$defaultOptions = array(
			'table' => $tableName,
			'alias' => $modelName,
			'type' => $type,
			'conditions' => array(
				$modelName.'.'.strtolower($this->name).'_id = .'.$this->name.'.id'			
			)
		);
		
		return array_replace_recursive($defaultOptions, $options);
	}
	
	public function getJoinManyToManyOnSpecificId($type, $model, $id, $options = array()) {
		$join = $this->getJoinManyToMany($type, $model, $options);
		$join['conditions'][] = $join['alias'].'.'.strtolower($model).'_id = '.$id;
		return $join;		
	}	
}
