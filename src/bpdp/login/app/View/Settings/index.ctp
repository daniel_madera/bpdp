<div class="row-fluid">
	<div class="span5 well">
	<h3><?php echo __('Přístupy'); ?></h3>
	<?php 
		foreach ($groups as $group) {
				echo '<script>
				$(function() {
					$("#datepicker-'.$group['Group']['id'].'").datepicker({
						dateFormat : "dd.mm.yy"
					}).attr("readonly", "readonly");
					$("#timepicker-'.$group['Group']['id'].'").timepicker({
					}).attr("readonly", "readonly");
				});
				</script>';
				
			echo '<h4>'.h($group['Group']['name']).'</h4>';
			echo __('Nyní je datum přístupu nastaven od: ').' <strong>'.$this->Time->format($group['Group']['auto_access'], '%H:%M %d.%m.%Y').'</strong><br><br>';
			echo $this->element('forms/create', array(
		        'model' => 'Settings',
		        'options' => array(
					'class' => 'form-inline',
					'inputDefaults' => array(						
						'label' => false, 
						'div' => false
					),
				)
		    ));
			echo $this->Form->input('id', array('type' => 'hidden', 'value' => $group['Group']['id']));
				echo $this->Form->input('auto_access_date', array(
					'class' => 'input-small',
					'id' => 'datepicker-'.$group['Group']['id'],
					'type' => 'text',
					'label' => __('Nastavte nový čas přístupu: <br>'),
					'div' => false
				)).' ';
				echo $this->Form->input('auto_access_time', array(
					'class' => 'input-small',
					'id' => 'timepicker-'.$group['Group']['id'],
					'type' => 'text',
					'label' => false,
					'div' => false
				)).'<br><br>';
			echo $this->element('forms/submit', array(
					'title' => __('Nastavit'),
					'options' => array(
						'class' => 'btn btn-primary btn-block'
					)
				));
		}
	?>		
	
	</div>
	<div class="span4 well">
	<h4><?php echo __('Archivace'); ?></h4>
		<?php
		echo $this->element('button', array(
			'link' => array(
				'action' => 'index',
				'archive_theses' => '1'
			),
			'options' => array(
				'title' => __('Archivovat všechny zadání a témata'),
				'post' => true,
				'class' => 'btn btn-primary input-block-level',
				'confirm' => __('Opravdu chcete archivovat všechna oficiální zadání a přiřazená témata k nim?') . __('Upozornění: Zadání, která nebyla odeslána k dalšímu zpracování budou odstraněna.')
			)
		));

		echo '<br>' . $this->element('button', array(
			'link' => array(
				'action' => 'index',
				'archive_topics' => '1'
			),
			'options' => array(
				'title' => __('Archivovat nezadaná témata'),
				'post' => true,
				'class' => 'btn btn-primary input-block-level',
				'confirm' => __('Opravdu chcete archivovat nezadaná témata? Pokud chcete archivovat všechna témata, musíte nejdříve odstranit všechny fronty.')
			)
		));
		?>
	</div>
	<div class="span3 well">
		<h4><?php echo __('Fronty studentů o témata'); ?></h4>
		<?php
		echo $this->element('button', array(
			'link' => array(
				'action' => 'index',
				'remove_queues' => '1'
			),
			'options' => array(
				'title' => __('Odstranit všechny fronty'),
				'post' => true,
				'class' => 'btn btn-primary input-block-level',
				'confirm' => __('Odstraněním budou odebrány všechny žádosti studentů o témata?')
			)
		));
		?>
	</div>
</div>
<div class="row-fluid">
	<div class="span5">
		<?php echo $this->element('cancel-button'); ?>
	</div>
</div>
