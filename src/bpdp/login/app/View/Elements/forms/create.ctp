<?php

$default_options = array(
	'inputDefaults' => array (
		'div' => 'form-group well',
		'class' => 'form-control input-block-level'
	),
	'novalidate' => true
);

if(!empty($options)) {
	$options = array_replace_recursive($default_options, (array) $options);
} else {
	$options = $default_options;
}


echo $this->Form->create($model, $options);
