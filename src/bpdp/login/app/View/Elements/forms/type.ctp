<h3>
 <?php
	if(isset($subtitle)) {
		echo __($subtitle);
	} else {
		echo __('Nový typ práce');
	}
?>
</h3>
<?php		
echo $this->element('forms/create', array(
    'model' => 'Type',
));

if(!empty($this->request->data['Type']['id'])) {
	echo $this->Form->input('id', array('type' => 'hidden'));
}

echo $this->Form->input(
    'name', array(
    'label' => __('Název'),
    'placeholder' => __('Název')
));

echo $this->Form->input(
    'abbreviation', array(
    'label' => __('Zkratka'),
    'placeholder' => __('Zkratka')
    )); 
echo $this->Form->input(
    'description', array(
    'type' => 'textarea',
    'label' => __('Popis'),
    'rows' => 3
));
echo $this->Form->input(
    'department_id', array(
    'label' => __('Patří do'),
    'multiple' => false,
    'options' => $rootDepartments
));
echo $this->Form->input(
    'range', array(
    'label' => __('Rozsah práce'),
    'placeholder' => __('Rozsah práce')
));