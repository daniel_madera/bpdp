<?php
echo '<div class="row-fluid">';
echo '<div class="span6">';
echo $this->Form->input(
    'name', array(
        'label' => __('Název'),
        'placeholder' => __('Název')
    ));

echo $this->Form->input(
    'abstract', array(
        'type' => 'textarea',
        'label' => __('Abstrakt'),
    )); 
echo $this->Form->input(
    'requirements', array(
        'type' => 'textarea',
        'label' => __('Požadavky na studenta'),
        'rows' => 3
    ));
	 
	
echo $this->Form->input(
    'literature', array(
        'type' => 'textarea',
        'label' => __('Doporučená literatura'),
        'rows' => 2
    ));
	
echo $this->Form->input(
    'consultant', array(
        'type' => 'textarea',
        'label' => __('Konzultant'),
        'rows' => 1
    ));    
echo '</div>';

echo '<div class="span6">';
 	// if(!isset($this->request->data['Type']['Type'])) {
 		// $this->request->data['Type']['Type'] = array_keys($types);
	// }
	// if(!isset($this->request->data['Department']['Department'])) {
		// $this->request->data['Department']['Department'] = array_keys($departments);
	// }
	// if(!isset($this->request->data['Parameter']['Parameter'])) {
		// $this->request->data['Parameter']['Parameter'] = array_keys($parameters);
	// }
	
	if(empty($this->request->data['Topic']['institute_id'])) {
		$this->request->data['Topic']['institute_id'] = $current_user['User']['institute_id'];
	}
	
	echo $this->Form->input('institute_id', array(
        'label' => __('Vyberte ústav pod jakým bude práce vedena'),
    ));
	
	foreach($typesAll as $type) {
		$t = $type['Type'];
		$typeList[$t['id']] = array('name' => h($t['name']), 'value' => $t['id'], 'data-department' => $t['department_id']);		
	}
	
    echo $this->Form->input('Type', array(
        'label' => __('Vyberte pro jaké typy prací je téma vhodné (minimálně jeden)'),
        'multiple' => 'checkbox',
        'options' => $typeList,
        'div' => array(
        	'id' => 'types-select-div',
        	'class' => 'form-group well'
        ) 
    ));
    
    echo $this->Form->input('Department', array(
        'label' => __('Vyberte obory, pro které je téma vhodné (minimálně jeden)'),
        'multiple' => 'checkbox',
        'div' => array(
        	'id' => 'departments-select-div',
        	'class' => 'form-group well'
        ) 
    ));
    if(!empty($parameters)) {    		
		echo $this->Form->input('Parameter', array(
	        'label' => __('Zaškrtněte doplňující vlastnosti'),
	        'multiple' => 'checkbox', 
	    ));
	}
		
	echo '</div>';
echo '</div>';