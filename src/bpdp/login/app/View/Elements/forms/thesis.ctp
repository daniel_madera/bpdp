
<?php
echo '<div class="row-fluid">';
echo '<div class="span7">';    
echo $this->Form->input(
    'name', array(
        'label' => __('Název'),
        'placeholder' => __('Název')
    ));
        
echo $this->Form->input(
    'name_en', array(
        'label' => __('Anglický název'),
        'placeholder' => __('Anglický název')
    )); 

echo $this->element('forms/add-input', array(
    'identifier' => 'requirement',
    'title' => __('Zásady pro vypracování').' ('.__('minimálně tři').')',
    'rows_count' => 3    
));

echo $this->element('forms/add-input', array(
    'identifier' => 'literature',
    'title' => __('Doporučená literatura').' ('.__('minimálně jednu').')',
    'rows_count' => 3    
));

echo $this->Form->input(
    'Topic.consultant', array( 
        'type' => 'textarea',
        'label' => __('Konzultant'),
        'rows' => 3 
    ));  
      
echo $this->Form->input(
    'Topic.id', array(
        'type' => 'hidden',
    ));
echo $this->Form->input(
    'user_id', array(
        'type' => 'hidden',
    ));
echo '</div>';
echo '<div class="span5">';    
echo '<div class="well">';    
    echo $this->Form->input('User.name', array(
        'label' => __('Zvolený student'),
        'readonly' => true,
        'div' => false,
        'disabled' => 'disabled',
        'default' => h($topic['Thesis']['User']['name'])
    ));    
    
    echo $this->Form->input('type_id', array(
        'label' => __('Zvolený typ práce'),
        'multiple' => false,
        'disabled' => true,
        'div' => false, 
        'options' => $types,
        'selected' => $topic['Thesis']['type_id']
    ));

    echo $this->Form->input('Topic.institute_id', array(
        'label' => __('Zvolený ústav pod kterým je práce vedena'),
        'multiple' => false,
        'disabled' => true,
        'div' => false, 
        'options' => $institutes,
        'selected' => $topic['Topic']['institute_id']
    ));
    
    echo $this->Form->button(__('Editovat'), array('id' => 'thesis-form-type-edit-button', 'class' => 'btn btn-danger')); 
    
echo '</div>';   
echo '<div class="well">';
    echo '<h4>Téma</h4>';    
    echo $this->element('views/topic/detail', array('topic' => $topic));   
echo '</div>';
echo '</div>';
echo '</div>';

$this->Js->get('#thesis-form-type-edit-button'); 
$this->Js->event('click', 'if (confirm("'.__('Opravdu chcete editovat typ práce nebo ústav?').'")) {$("#ThesisTypeId").removeAttr("disabled"); $("#TopicInstituteId").removeAttr("disabled"); $(this).hide();}');
?>
