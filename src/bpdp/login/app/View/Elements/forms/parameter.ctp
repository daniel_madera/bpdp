<h3>
 <?php
	if(isset($subtitle)) {
		echo __($subtitle);
	} else {
		echo __('Nová vlastnost');
	}
?>
</h3>
<?php			
    
    echo $this->element('forms/create', array(
        'model' => 'Parameter'
    ));
	
	if(!empty($this->request->data['Parameter']['id'])) {
		echo $this->Form->input('id', array('type' => 'hidden'));
	}
	
    echo $this->Form->input(
        'name', array(
            'label' => __('Název'),
            'placeholder' => __('Název')
        ));
		
	echo $this->Form->input(
	    'description', array(
	    'type' => 'textarea',
	    'label' => __('Popis'),
	    'rows' => 3
	));

 ?>