<?php
echo $this->Html->script('add-input');

if(empty($identifier)) {
    throw new NotFoundException('Nastavte proměnnou "identifier"');
}

if(empty($rows_count) || $rows_count < 2) {
    $rows_count = 3;
}
$limit = 0;
$this->Js->get('#'.$identifier.'-add-button');
$this->Js->event('click', 'add_input_button_clicked("'.$identifier.'", '.$rows_count.','.$limit.', "");');
if (empty($this->request->data[Inflector::camelize($identifier)])) {
    $this->Js->get('document');
    $this->Js->event('ready', '
        for(var i = 0; i < '.Configure::read('Application.init.'.$identifier).'; i++) {
            add_input_button_clicked("'.$identifier.'", '.$rows_count.', '.$limit.', "");
        }
    ');
} else {
    $data = $this->request->data[Inflector::camelize($identifier)];
    
    $this->Js->get('document');
    foreach($data as $value) {
        //debug($value);
        $this->Js->event('ready', '            
            add_input_button_clicked("'.$identifier.'", '.$rows_count.', '.$limit.', "'.str_replace('"', '\"', preg_replace('/\s\s+/', '\n', $value['text'])).'");
        ');
    }
    
}

echo '<div class="well form-group">';

if(!empty($title)) {
    echo ' <label>'.__($title).'</label>';   
}

if ($this->Form->isFieldError($identifier.'-counter')) {
    echo $this->Form->error($identifier.'-counter');
}


echo '<div id="'.$identifier.'-container"></div>';
    
echo $this->Html->tag('button', __('Přidat'), array(
    'class' => 'btn btn-success',
    'id' => $identifier.'-add-button',
    'type' => 'button'
));
echo $this->Form->input($identifier.'-counter', array(
    'type' => 'hidden',
    'id' => $identifier.'-counter',
    'value' => 0
));


echo '</div>';
echo $this->Js->writeBuffer();