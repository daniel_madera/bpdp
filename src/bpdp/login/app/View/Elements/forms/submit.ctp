<?php

if(!isset($title)) {
	$title = __('Uložit data');
}
if(!isset($options['end'])) {
	$options['end'] = true;
}

$default_options = array(
	'div' => 'form-group',
	'class' => 'btn btn-primary btn-large btn-block',
	'span12' => true	
);

if(empty($link)) {
    $link = "#";
}

if(!empty($options)) {
	$options = array_replace_recursive($default_options, $options);
} else {
	$options = $default_options;
}

$span12 = $options['span12'];

if($span12) {
	echo '<div class="row-fluid">';
}

if(isset($cancel)) {
	echo '<div class="span6">';
	echo $this->element('button', array(
				'link' => '#',
				'options' => array(
					'title' => __('Zrušit'),
					'class' => 'btn btn-default btn-large btn-block',
					'onClick' => 'history.go(-1);return false'
				)
			));
	echo '</div>';
}
if($span12) {
	echo '<div class="span'.(isset($cancel) ? '6' : '12').'">';
}

if(isset($js) && $js === true) {
	echo $this->Js->submit(__($title), $options);
} else {
	echo $this->Form->button(__($title), $options); 
}

if($options['end'] === true) {
	echo $this->Form->end();
} 
if($span12) {
	echo '</div>';
	echo '</div>';
}
?>

