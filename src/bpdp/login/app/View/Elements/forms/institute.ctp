<h3>
 <?php
	if(isset($subtitle)) {
		echo __($subtitle);
	} else {
		echo __('Nový ústav');
	}
?>
</h3>
<?php			
    
    echo $this->element('forms/create', array(
        'model' => 'Institute'
    ));
	
	if(!empty($this->request->data['Institute']['id'])) {
		echo $this->Form->input('id', array('type' => 'hidden'));
	}
	
    echo $this->Form->input(
        'name', array(
            'label' => __('Název'),
            'placeholder' => __('Název')
        ));
    echo $this->Form->input(
        'abbreviation', array(
            'label' => __('Zkratka'),
            'placeholder' => __('Zkratka')
        ));
    
 ?>