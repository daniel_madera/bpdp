<?php
echo $this->element('forms/create', array('model' => 'User', 'options' => array(
	'inputDefaults' => array (
		'div' => 'form-group',
		'class' => 'form-control input-block-level'
	)
)));

echo $this->Form->input('title_before_name', array(
	'label' => __('Tituly před jménem'),
));

echo $this->Form->input('forename', array(
	'label' => __('Jméno'),
));

echo $this->Form->input('surname', array(
	'label' => __('Příjmení'),
));

echo $this->Form->input('title_behind_name', array(
	'label' => __('Tituly za jménem'),
));

$options = array(
	0 => __('Vše rozbaleno'),
	1 => __('Vše zabaleno')
);

echo $this->Form->input(
	'collapsed', array(
	    'type' => 'select',
	    'label' => __('Výchozí zobrazení témat'),
	    'options' => $options
));

$options = array(5 => 5, 10 => 10, 15 => 15, 20 => 20, 25 => 25, 30 => 30);

echo $this->Form->input(
	'page_limit', array(
	    'type' => 'select',
	    'label' => __('Počet témat na stránku'),
	    'options' => $options
));

echo $this->element('forms/submit', array('cancel' => true));
	
?>
