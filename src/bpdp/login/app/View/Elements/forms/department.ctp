<h3>
 <?php
	if(isset($subtitle)) {
		echo __($subtitle);
	} else {
		echo __('Nový obor');
	}
?>
</h3>
<?php			
    
    echo $this->element('forms/create', array(
        'model' => 'Department'
    ));
	
	if(!empty($this->request->data['Department']['id'])) {
		echo $this->Form->input('id', array('type' => 'hidden'));
	}
	
    echo $this->Form->input(
        'name', array(
            'label' => __('Název'),
            'placeholder' => __('Název')
        ));
    echo $this->Form->input(
        'abbreviation', array(
            'label' => __('Zkratka'),
            'placeholder' => __('Zkratka')
        ));
    
    $root_departments[0] = 'Obory';
    echo $this->Form->input(
        'parent_id', array(
            'type' => 'select',
            'label' => __('Vyberte nadřazenou kategorii'),
            'options' => $root_departments,
        )
    );                        
 ?>