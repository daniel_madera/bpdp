<?php
if (!isset($class)) {
	$class = 'info';
}
if (!isset($close)) {
	$close = true;
}
$titles = array(
    'info' => 'Informace!',
    'success' => 'Úspěch!',
    'warning' => 'Upozornění!',
    'error' => 'Chyba!'
);
?>

<div class="alert alert-block alert-dismissable alert-<?php echo $class;?>">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <h4><?php echo __(empty($title) ? __($titles[$class]) : __(h($title))); ?></h4> 
    <?php echo __(h($message)); ?>    
</div>
