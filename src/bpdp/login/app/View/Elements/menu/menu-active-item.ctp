<?php
    if(empty($options)) {
        $options = array();
    }
             
    $request = $this->params['named'];
    
    $request['controller'] = $this->params['controller'];
    
    $request['action'] = $this->params['action'];
    
    $removeKeys = array('sort', 'page', 'direction');

    foreach($removeKeys as $key) {
        if(isset($request[$key]))
            unset($request[$key]);
    }
    
    
                    
    $is_active = false;
    
    if(count($request) == count($link)) {
    
        foreach($request as $key => $value) {
            $is_active = true;
            
            if(!isset($link[$key]) || $link[$key] != $value) {
                $is_active = false;
                break;               
            }
        }
    }
    
?>

<li <?php echo ($is_active ? 'class="active"' : '');?>>
    <?php echo $this->Html->link(__($title), $link, $options); ?>
</li>
