<div class="navbar navbar-fixed-top navbar-inverse">
	<div class="navbar-inner">
		<div class="container">
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".menu-main">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			<?php 
				echo $this->Html->link(__("%s - %s",Configure::read("Application.name"),Configure::read("Application.slogan")), 
					array(
						'controller'=>'topics',
						'action' => 'index'	 					
					), array(
						'class' => 'visible-desktop brand'
					)
				); 
			?>
			<a class="visible-phone visible-tablet brand" href="#"> 
				<?php echo __("%s",Configure::read("Application.name")); ?>
			</a>
			<?php if(!empty($current_user)): ?>
			<p class="navbar-text pull-right">
				<?php
				echo $this->Html->link(h($current_user['User']['full_name']),
					array('controller'=>'users','action'=>'view',$current_user['User']['id']),array('escape'=>false));
				echo '&nbsp;&nbsp;'.$this->Html->tag('i','',array('class'=>'icon-user icon-white'));
				?>
			</p>
			<div class="nav-collapse collapse menu-main">
				<ul class="nav">
					<?php					
					echo $this->element('menu/main-'.$current_user['Group']['name']);
					//echo $this->element('menu/menu-active-item',array('title'=>__('O aplikaci'),'link'=> array('controller'=>'pages','action'=>'display','about')));
					if(Configure::read('debug') > 0): 
						echo $this->element('menu/menu-active-item',array('title'=>__('Login'),'link'=> array('controller'=>'users','action'=>'login')));
					endif;
					echo $this->element('menu/menu-active-item',array('title'=>__('Odhlásit se'),'link'=> array('controller'=>'users','action'=>'logout')));
					?>
				</ul>
			</div>
			<?php endif;?>
		</div>
	</div>
</div>