<?php 
	foreach($menus as $header => $menu) {
		$link = $menu[0];
		if(empty($link)) {
			$link = '#';
		}
		$items = $menu[1];
	
		echo '<li class="dropdown">';
		
		
		
		echo $this->Html->link($header.'<b class="caret"></b>',$link,array('escape'=>false,'class'=>'dropdown-toggle','data-toggle'=>'dropdown'));
		
		echo '<ul class="dropdown-menu">';
		foreach($items as $header => $options) {
			
			$class = 'Html';
			$function = 'link';
						
			if(isset($options['link'])) {
				$link = $options['link'];
				if (isset($options['post']) and $options['post']) {
					$class = 'Form';
					$function = 'postLink';
				}	
			} else {
				$link = $options;
			}
			
			echo '<li>';
			echo $this->$class->$function($header, $link);
			echo '</li>';	
		}
		echo '</ul></li>';	
	}
?>