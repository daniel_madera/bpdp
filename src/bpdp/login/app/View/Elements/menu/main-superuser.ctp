<?php

$menus = array(
    __('Témata pro studenty') => array(
        '#',
        array(
            __('Vytvořit téma') => array(
                'controller' => 'topics',
                'action' => 'add'
            ),
            __('Všechna') => array(
                'controller' => 'topics',
                'action' => 'index', 
                'all' => 1
            ),
            __('Pouze moje') => array(
                'controller' => 'topics',
                'action' => 'index',
                'leader' => $current_user['User']['id']
            ),
            __('Archiv') => array(
                'controller' => 'topics',
                'action' => 'index',
                'archived' => 1
            )
        )
    ),
    __('Oficiální zadání') => array(
        '#',
        array(
            __('Přehled mých zadání') => array(
                'controller' => 'theses',
                'action' => 'index'
            ),
            __('Všechna zadání') => array(
                'controller' => 'theses',
                'action' => 'admin_index'
            ),
            __('Export zadání') => array(
                'controller' => 'theses',
                'action' => 'export'
            )
        )
    ),
    __('Nastavení') => array(
        '#',
        array(
            __('Uživatelé') => array(
                'controller' => 'users',
                'action' => 'index'
            ),
            __('Typy prací') => array(
                'controller' => 'types',
                'action' => 'index'
            ),
            __('Obory') => array(
                'controller' => 'departments',
                'action' => 'index'
            ),
            __('Ústavy') => array(
                'controller' => 'institutes',
                'action' => 'index'
            ),
            __('Vlastnosti témat') => array(
                'controller' => 'parameters',
                'action' => 'index'
            ),
            __('Správa aplikace') => array(
                'controller' => 'settings',
                'action' => 'index'
            ),
            __('Můj profil') => array(
                'controller' => 'users',
                'action' => 'view',
                $current_user['User']['id']
            )
        )
    )
);

echo $this->element('menu/menu-dropdown', array('menus' => $menus));
