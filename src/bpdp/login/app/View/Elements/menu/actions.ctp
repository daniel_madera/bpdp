<?php

if (!isset($action)) {
	$actions = array('view');
}

else if(!is_array($action)) {
	$actions = array($action);
} else if (is_array($action)) {
	$actions = $action;
}

if(!isset($options)) {
	$options = array();
}

$default_options = array(
	'icon' => true,
	'tooltip' => true,
	'title' => false
);

$titles = array(
    'view' => __('Zobrazit'),
    'edit' => __('Editovat'),
    'delete' => __('Smazat'),
    'add' => __('Přidat'),
    'download' => __('Stáhnout'),
    'hide' => __('Skrýt'),
    'show' => __('Zobrazit')
);

foreach ($actions as $action) {
    if(!empty($titles[$action])) {
        $default_options['tooltip'] = $titles[$action];
    }
    
    $link = array();
    
	if(isset($controller)) {
	    $link['controller'] = $controller;
    }
    
	$link['action'] = $action;	
	
	if(isset($id)) {
	   $link[] = $id;
    }
    
    if ($action == 'delete') {
        $options['confirm'] = __('Opravdu chcete smazat data?');
    }

	echo $this -> element('button', array(
		'link' => $link,
		'options' => array_merge($default_options, $options)
	));
}
