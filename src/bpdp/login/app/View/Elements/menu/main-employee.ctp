<?php

$menus = array(
	__('Témata pro studenty') => array(
		'#',
		array(
			__('Vytvořit téma') => array(
				'controller' => 'topics',
				'action' => 'add'
			),
			__('Všechna') => array(
				'controller' => 'topics',
				'action' => 'index',
				'all' => 1
			),
			__('Pouze moje') => array(
				'controller' => 'topics', 
				'action' => 'index',
				'leader' => $current_user['User']['id']
			),
			__('Archiv') => array(
				'controller' => 'topics',
				'action' => 'index',
				'archived' => 1
			)
		)
	),
	__('Oficiální zadání') => array(
		'#',
		array(__('Přehled') => array(
				'controller' => 'theses',
				'action' => 'index'
			),
		)
	),
	__('Nastavení') => array(
		'#',
		array(
			__('Můj profil') => array(
				'controller' => 'users',
				'action' => 'view',
				$current_user['User']['id']
			), 
		)
	)
);

echo $this->element('menu/menu-dropdown', array('menus' => $menus));
