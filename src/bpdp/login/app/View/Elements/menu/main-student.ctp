<?php

$menus = array(
	__('Témata') => array(
		'#',
		array(
			__('Všechny') => array(
				'controller' => 'topics',
				'action' => 'index',
				'all' => 1
			),
			__('Pouze moje') => array(
				'controller' => 'topics',
				'action' => 'index',
				'student' => $current_user['User']['id'] 
			),
		)
	),
	__('Oficiální zadání') => array(
		'#',
		array(__('Přehled mých zadání') => array(
				'controller' => 'theses',
				'action' => 'overview'
			), )
	),
	__('Nastavení') => array(
		'#',
		array(__('Můj profil') => array( 
				'controller' => 'users',
				'action' => 'view',
				$current_user['User']['id']
			), )
	)
);

echo $this->element('menu/menu-dropdown', array('menus' => $menus));
