<?php 
	$superuser = Configure::read('Application.admin_contact');
	if(count($superuser) > 0) {
		echo '<p>'.__('Prosím kontaktujte adminstrátora o této chybě - %s.', $this->Html->link($superuser, 'mailto:'.$superuser)).'</p>';
	} 
?>