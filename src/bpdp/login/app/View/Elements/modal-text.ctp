<?php

if($maxLength < strlen($text)) {
    $id = md5(uniqid(rand(), true));
    echo $this->Html->link(substr($text, 0, $maxLength - 3).'...', '#'.$id, array(
        'data-toggle' => 'modal',
    ));
    
    echo '
        <div id="'.$id.'" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="'.$id.'label" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 id="'.$id.'label">'.h($title).'</h4>
            </div>
            <div class="modal-body">
                 <p>'.$text.'</p>
                </div>
                <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
            </div>
        </div> 
    ';
    
} else {
    echo __(h($text));
}


