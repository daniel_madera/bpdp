<?php
if(count($userQueues) > 0):
?>
<div class="well queue-limit">
<span><?php echo __('Požádaná témata'); ?></span>
<table>
    <?php foreach($userQueues as $type => $count): ?>
    <tr>
        <th><?php echo h($type).':'; ?></th>
        <td><?php echo '<strong>'.$count . 'x</strong> ' . __('(limit požádání: %d)', Configure::read('Application.limit.requests.student')); ?> </td>
    </tr>
    <?php endforeach; ?>
</table> 
</div>
<?php
endif;
unset($tmp_types);
?>