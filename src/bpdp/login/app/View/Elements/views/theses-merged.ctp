<?php
if(count($thesesMerged) > 0):
?>
<div class="well queue-limit">
<span><?php echo __('Přiřazená zadání'); ?></span>
<table style="width: 100%;">
    <?php foreach($thesesMerged as $thesis): ?>
    <tr>
        <td><?php echo $thesis['Type']['abbreviation']; ?> </td>
        <td><?php echo h($thesis['Thesis']['name']); ?></td>
    </tr>
    <?php 
        endforeach; 
        unset($thesis);
    ?>
</table> 
</div>
<?php
endif;
?>