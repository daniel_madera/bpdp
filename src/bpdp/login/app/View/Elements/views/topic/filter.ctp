	<?php	
	echo '<div class="well well-small">';
		echo $this->element('forms/create', array(
				'model' => 'Topic', 
	 			'options' => array(
					'class' => 'form-inline no-bottom-margin',
					'inputDefaults' => array(						
						'label' => false, 
						'div' => false
					), 
					'id' => 'topics-search-form',
					// 'default' => false,
				) 
			) 
		);
		echo $this->Form->input(
	        'keywords', array(
	            'placeholder' => __('Zadejte ID nebo klíčová slova'),
	            'class' => ' input-block-level',
	        )); 
			
        echo $this->element('forms/submit', array(
                'title' => "<i class='fa fa-search'></i>",
                'options' => array(
                    'div' => false,  
                    'class' => 'btn input-block-level',
                    'escape' => false,
                    // 'default' => false,
                )
            )  
        );
        echo '</div>';
        echo $this->Form->end();
	?> 
	<div class="well well-small">
	<div class="margin-legend-collapse" data-toggle="collapse" data-target="#topics-filter-form-collapse" id="topics-filter-main-label">
		<legend>
			<?php echo __('Filtrování'); ?>
			<i class="fa fa-arrows-v pull-right"></i>
		</legend>
	</div>
	<?php	 	  
		echo $this->element('forms/create', array(
				'model' => 'Topic', 
				'options' => array(
					'class' => 'form-horizontal',
					'inputDefaults' => array(
						'div' => false,
						'label' => false
					), 
					'id' => 'topics-filter-form',
					'default' => false
				)
			) 
		);	
		echo $this->element('button', array(
			'link' => $this->request->params['named'],
			'options' => array(
				'title' => __('Zrušit všechny filtry'),
				'icon' => 'fa fa-filter',
				'class' => 'btn btn-default input-block-level btn-remove-filters'
			)
		));
		echo $this->element('views/topic/filter-status');
        
			echo $this->Form->input(
		        'Type', array(
		        	'multiple' => false,
		        	'div' => 'control-group',
		        	'class' => 'input-block-level',
		        	'label' => __('Vyberte typ práce'),
		        	'empty' => __('Všechny...')
		    ));
			
                    
            echo $this->Form->input(
                'Department', array(
                    'multiple' => false,
                    'div' => 'control-group',
                    'class' => 'input-block-level',
                    'label' => __('Vyberte obor'),
                    'empty' => __('Všechny...')
            ));
                    
            
			echo $this->Form->input(
		        'institute_id', array(
		        	'multiple' => false,
		        	'div' => 'control-group',
		        	'class' => 'input-block-level',
		        	'label' => __('Vyberte ústav'),
		        	'empty' => __('Všechny...')
		    ));
			
		//echo $this->Form->submit('Send');
		// debug(isset($this->request->params['named']['all']));
		// debug(!empty($this->request->data['Parameter']['Parameter']));
		// debug(!empty($this->request->data['Topic']['leader_id']));
		// debug($this->request->data['Topic']['leader_id']);
		if(isset($this->request->params['named']['all'])):
			
            echo '<a class="pointer" data-toggle="collapse" data-target="#topics-filter-form-collapse">'.__('Další možnosti filtrování').'</a>';
			
		$collapsed = '';
		if(isset($this->request->data['Parameter']['Parameter']) || 
			isset($this->request->data['Topic']['leader_id'])) {
				$collapsed = 'in';
			}
		
		echo '
		<div id="topics-filter-form-collapse" class="collapse '.$collapsed.'" style="margin-top: 5px;">';
					
			// select all when first access to filter					
			if(!isset($this->request->data['Parameter']['Parameter'])) {
				$this->request->data['Parameter']['Parameter'] = array_keys($parameters);
			}
			if(!isset($this->request->data['Topic']['leader_id'])) {
				$this->request->data['Topic']['leader_id'] = array_keys($leaders);
			}			
			$options = array();
			foreach($leaders as $value => $label) {
				$options[] = array(
					'name' => '<span>'.h($label).'</span>',
					'value' => $value,
				);
			}
			
			echo '<label>'.__('Vyberte vedoucí prací').'</label>';
            echo '<div class="pull-right"><a id="teachers-select-all" class="pointer">'.__('Označit vše').'</a> | <a id="teachers-unselect-all" class="pointer">'.__('Odznačit vše').'</a></div>';
			echo $this->Form->input(
		        'leader_id', array(
		        	'multiple' => 'checkbox',
		        	'options' => $options,
		        	'div' => array(
		        		'class' => 'control-group scrollable checkbox-align ',
                        'style' => 'width: 90%;',
		        		'id' => 'topics-filter-form-leader',
		        	),
		        	'escape' => false
		    ));
			
			$options = array();
			foreach($parameters as $value => $label) {
				$options[] = array(
					'name' => '<span>'.$label.'</span>',
					'value' => $value,
				);
			}
			if(!empty($parameters)) {
				echo '<label>'.__('Vyberte doplňující vlastnosti').'</label>'; 
				echo $this->Form->input(
			        'Parameter', array(
			        	'multiple' => 'checkbox',
			        	'label' => false,
			        	'options' => $options,
			        	'escape' => false,
			        	'div' => array(
			        		'class' => 'control-group scrollable checkbox-align',
			        		'id' => 'topics-filter-form-properties'
				)));
			}
		echo '
			<div id="add-paginator-reset"></div>
		</div>
		</div>
		<input type="hidden" id="topics-filter-form-collapse-input" name="collapse" value=""></input>
		'; // div collapse
		endif;	
		echo $this->Form->end();
		$search = $this->Js->get('#topics-search-form')->serializeForm(array('isForm' => true, 'inline' => true));
		$filter =  $this->Js->get('#topics-filter-form')->serializeForm(array('isForm' => true, 'inline' => true));
        
        $request_params = array(
            'update' => '#sub-content',
            'data' => $filter, 
            'dataExpression' => true,
            'method' => 'POST',         
            'async' => true,
            'before' => $this->Js->get('#overlay-wrapper')->effect('fadeIn'),
            'complete' => $this->Js->get('#overlay-wrapper')->effect('fadeOut').' window.scrollTo(0,120); paginate_icons_sort();'
        );
            
        $request1_params = $request_params;
        $request1_params['data'] = $search;	
        	
		echo $this->Js->buffer('	
			var filter = function filterTopicsData() {
				$("#add-paginator-reset").append("<input type=\'hidden\' name=\'data[Topic][reset]\' value=\'1\'/>")
				'.$this->Js->request($this->passedArgs, $request_params).'
			}
			var search = function filterTopicsData() {
                '.$this->Js->request($this->passedArgs, $request1_params).'
            }'.'		
        		$("#topics-filter-form").find("input").on("change", filter);
        		//$("#topics-search-form").find(".btn").on("click", search);
        		$("#topics-filter-form").find("select").on("change", filter);
        		
                // managed collapsing
        		$("#topics-filter-main-label").on("click", function(e) {
        			if($("#topics-filter-form-collapse").hasClass("in")) {
        				$("#topics-filter-form-collapse-input").attr("value", "");
        			} else {
        				$("#topics-filter-form-collapse-input").attr("value", "in");
        			}
        		});
			'
		);
	?>