<?php
if(empty($this->request->params['named']['archived'])):
$altCssClasses = array();
if(isset($this->request->params['named']['all'])) {
	$statuses = array(
		'active' => '', 
		'pending' => '', 
		'merged' => ''
	);
	$altCssClasses = array(
		'pending' => 'active-pending', 
	);
} else {
	$statuses = array(
		'active' => '', 
		'pending' => '', 
		'merged' => '',
	);
	if($current_user['Group']['name'] != 'student') {
		$statuses['hidden'] = '';
	}
}

$functionName = 'getStatusTextEmployee';
if(isset($this->request->params['named']['all'])) {
	$functionName = 'getStatusTextStudent';
}
foreach($statuses as $key => &$status) {
	$status = $this->Status->$functionName($key); 
	if(in_array($key, array_keys($altCssClasses))) {
		$key = $altCssClasses[$key];
	}
	$status .= '<span class="status-legend status-legend-'.$key.'">&nbsp;</span>';
}

if(!isset($this->request->data['Topic']['status'])) {
	$this->request->data['Topic']['status'] = array_keys($statuses);
}

echo $this->Form->input(
    'status', array(
    	'multiple' => 'checkbox',
    	'div' => 'control-group',
    	'class' => 'input-block-level',
    	'label' => __('Vyberte stav zadání'),
    	'options' => $statuses,
    	'escape' => false
));
endif;