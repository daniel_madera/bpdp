<div>
	<dl class="dl-horizontal" id="topics-detail">
	<?php 
		if(empty($topic)) {
			throw new NotFoundException('Nastavte proměnnou "topic"');
		}
		
		$isCurrentUserTopic = in_array($current_user['Group']['name'], array('superuser', 'admin', 'employee'))
                            && $topic['Leader']['id']==$current_user['User']['id'];
							
		if(empty($options)) {
			$options = array();	
		}
		if(empty($options['show-name'])) {
			$options['show-name'] = false;	
		}
        
		$functionName = 'getStatusTextEmployee';
		if(isset($this->request->params['named']['all'])) {
			$functionName = 'getStatusTextStudent';
		}
		
		$status = $this->Status->$functionName($topic['Topic']['status']);
		if($current_user['Group']['name'] == 'student') { 
			if($topic['Topic']['status'] == 'pending') {
				$append = '';
				$queue = $this->Topic->findQueueByUserId($topic['Queue'], $current_user['User']['id']);
				if(!empty($queue)) {
					$append = ' (<strong>'.('včetně Vás').'</strong>)';
				}
	            $status .= ' - '.__('o toto téma studenti požádáli celkem %dx', count($topic['Queue'])).$append;             
	        } else if($topic['Topic']['status'] == 'merged') {
	        	if($topic['Thesis']['user_id'] == $current_user['User']['id']) {
	        		$status .= ' - '.__('%s jako %s', '<strong>'.__('k Vám').'</strong>', $types[$topic['Thesis']['type_id']]);
					$hide[] = 'Typ práce';  
	        	}        	
	        }
		}
		
        $types_string = "";
        if(!empty($topic['Type'])) {
            foreach($topic['Type'] as $type) {
                $abr = h($type['abbreviation']);
                $types_string .= '<a class="pointer" title="'.h($type['name']).' - '.h($type['description']).'">'.$abr.'</a>&nbsp;&nbsp;&nbsp;';
            }
        }
						 
		$data = array(
		    'Název' => '<strong>'.h($topic['Topic']['name']).'</strong>',
		    'ID' => $topic['Topic']['id'],
		    'Stav' => $status,
		    'Typ práce' => $types_string,
			'Vedoucí' => h($topic['Leader']['full_name']),
			'Ústav' => h($topic['Institute']['abbreviation']),
			'Vytvořeno' => date('H:i:s d.m.y', strtotime($topic['Topic']['created'])),
			'Doporučené obory' => h($this->Department->getOneRowList($topic['Department'], $departments_abbreviation)),
			'Abstrakt' => nl2br(h($topic['Topic']['abstract'])),
			'Literatura' => nl2br(h($topic['Topic']['literature'])),
			'Konzultant' => nl2br(h($topic['Topic']['consultant']))
		);
		
		if(!empty($hide)) {
			foreach($hide as $hiddenAttribure) {
				unset($data[$hiddenAttribure]);	
			}
			
		}
		
		$modal_keys = array('Anotace');
		foreach($data as $title => $value) {
			
			if(!empty($value)) { 
				echo '<dt>'.__($title).'</dt>';
				echo '<dd>';
				if(!empty($options['modal-text']) && in_array($title, array_keys($options['modal-text']))) {
					echo $this->element('modal-text', array('title' => $title, 'text' => $value, 'maxLength' => $options['modal-text'][$title]['maxLength']));
				} else {
					echo __($value);
				}
				echo '&nbsp;</dd>';
					
			}
        }
	?>
	</dl>
	<?php
	   if($current_user['User']['id'] == $topic['Leader']['id'] && !empty($topic['Queue'])): 
	?>

	
        <table class="table">
            <tr>
                <th>Student</th>
                <th>Čas vytvoření</th>
                <th>Práce</th>
                <th>Komentář</th>
                <th></th>
            </tr>
            
            <?php
                foreach($topic['Queue'] as $queue) {
                    echo '<tr>';
                    echo '<td>'.h($queue['User']['full_name']).'</td>';
                    echo '<td>'.date('H:i:s d.m.y', strtotime($queue['created'])).'</td>';
                    echo '<td>'.h($queue['Type']['abbreviation']).'</td>';
                    echo '<td>'.$this->element('modal-text', array('text' => h($queue['comment']), 'title' => __('Komentář'), 'maxLength' => 45)).'</td>';
                    echo '<td class="align-text-right">'.
                            $this->Form->postLink(__('Přiřadit studenta'), array(
                                'controller' => 'topics',
                                'action' => 'merge',
                                'Queue.id' => $queue['id']
                            ),
                            array(
                                'class' => 'btn btn-primary js-confirm-op',
                                'data-title' => __('Potvrzení přiřazení'),
                                'data-text' => __('Opravdu chcete přiřadit studenta k tématu? Ostatní studenti budou z fronty odstraněni.'),
                            )).
                            $this->element('menu/actions', array(
                                    'controller' => 'queues',
                                    'action' => 'delete',
                                    'id' => $queue['id'],
	                                'options' => array(
	                                    'icon' => true, 
	                                    'title' => false,
	                                    'class' => 'btn btn-default js-confirm-op',
	                                    'data-title' => __('Odstranění studenta'),
	                                	'data-text' => __('Opravdu chcete zamítnout požádání studenta o téma?'),
	                                ) 
                            ));
	
		              echo '</td>';
                    echo '</tr>';
                }
            ?>                  
        </table>                
    <?php
       endif;
    ?>        
</div>