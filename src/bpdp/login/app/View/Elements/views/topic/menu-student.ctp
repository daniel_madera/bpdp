<?php
echo '
    <ul class="nav nav-tabs">
        '.$this->element('menu/menu-active-item', array(
          'title'=>__('Všechny'),
          'link'=> array('controller'=>'topics','action'=>'index', 'all' => 1))
         ).'
         '.$this->element('menu/menu-active-item', array(
          'title'=>__('Pouze moje'),
          'link'=> array('controller'=>'topics','action'=>'index', 'student' => $current_user['User']['id']))
         ).'
    </ul>
'; 