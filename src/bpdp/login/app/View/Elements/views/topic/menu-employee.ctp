<?php
$link = array(
	'controller' => 'topics',
	'action' => 'index'
);
echo '
	<ul class="nav nav-tabs">
		' . $this->element('menu/menu-active-item', array(
	'title' => __('Všechna'),
	'link' => array_merge($link, array(
		 'all' => 1
	)) 
)) . $this->element('menu/menu-active-item', array(
	'title' => __('Pouze moje'),
	'link' => array_merge($link, array(
		'leader' => $user_id
	))
)) . $this->element('menu/menu-active-item', array(
	'title' => __('Archiv'),
	'link' => array_merge($link, array(
		'archived' => 1
	))
)) . ' 
	</ul>';
 