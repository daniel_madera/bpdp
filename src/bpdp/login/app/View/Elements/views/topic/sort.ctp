<div>
<?php echo $this->Html->script('paginator-icons'); ?>   
<?php
	$options = array(
		'class' => 'btn',
		'escape' => false,
	);
?> 
<div class="pagination-icons btn-block">
    <?php echo __('Seřadit dle').': '; ?>
    <?php $options['class'] = "btn btn-small numeric"; echo $this->Paginator->sort('created', __('Datum vytvoření')."&nbsp;<i class=\'\'></i>", $options); ?>
    <?php //$options['class'] = "btn btn-small alpha"; echo $this->Paginator->sort('status', __('Stav')."&nbsp;<i class=\'\'></i>", $options); ?>
 	<?php //$options['class'] = "btn btn-small numeric"; echo $this->Paginator->sort('id', __('ID')."&nbsp;<i class=\'\'></i>", $options); ?>
 	<?php $options['class'] = "btn btn-small alpha"; echo $this->Paginator->sort('name', __('Název')."&nbsp;<i class=\'\'></i>", $options); ?>
 	<?php $options['class'] = "btn btn-small alpha"; echo $this->Paginator->sort('Leader.surname', __('Vedoucí')."&nbsp;<i class=\'\'></i>", $options); ?>
</div>
    <button id="topics-expand-button" class="btn btn-small dropdown-toggle pull-right" data-toggle="dropdown" href="#" title="<?php echo __('Rozbalit vše'); ?>">
	<i class="fa <?php echo $current_user['User']['collapsed'] == 0 ? 'fa-chevron-up' : 'fa-chevron-down'; ?> "></i>
    </button>
</div>	
