       <?php
        $group = 'student';
        if(in_array($current_user['Group']['name'], array('superuser', 'admin', 'employee'))) {
            $group = 'employee';
        }
        echo $this->element('views/topic/menu-'.$group, array('user_id' => $current_user['User']['id']));
            

        if(empty($topics)):
            echo $this->element('views/records-not-found');
        else:
        ?>
        
        <div class="accordion" id="topics-group">
            <div class="accordion-group">
                <div class="accordion-heading">
                    <span class="accordion-toggle border-bottom border-large">
                        <?php echo $this->element('views/topic/sort'); ?>
                        <span class="records-found-counter"><?php echo __("Počet nalezených záznamů: %d", $this->Paginator->counter(array('format' => '{:count}'))); ?></span>
                    </span>
                </div>
                <?php     
                    foreach ($topics as $topic): 
                        $collapse_id = 'collapse-data-'.$topic['Topic']['id'];   
                        
                        $status_class = 'bg-'.$topic['Topic']['status'];
                        if(isset($this->request->params['named']['all'])) {
                            if($topic['Topic']['status'] == 'pending') {
                                $status_class = 'bg-active-pending';
                            }
                        }                    
                        
                ?>
                        <div id="heading-<?php echo $collapse_id; ?>" class="accordion-heading topics-heading <?php echo $status_class; ?>" data-toggle="collapse" href="#<?php echo $collapse_id; ?>">
                            <a class="accordion-toggle">
                                <span class="topics-title"><?php echo h($topic['Topic']['name']); ?></span>
                                <span class="pull-right">
                                    <?php echo $this->Html->tag('i','',array('class'=>'fa '.($current_user['User']['collapsed'] == 0 ? 'fa-chevron-up' : 'fa-chevron-down'))); ?>
                                </span>
                            </a>
                        </div> 
                        <div id="<?php echo $collapse_id; ?>" class="accordion-body collapse border-bottom <?php echo $current_user['User']['collapsed'] == 0 ? 'in' : ''; ?>">
                            <div class="accordion-inner">
                                <div class="pull-right button-margin-right-top">
                                    <?php echo $this->element('views/topic/action-menu', array('topic'=>$topic)); ?>
                                </div>  
                                <?php echo $this->element('views/topic/detail', array('hide' => array('Název'), 'topic'=>$topic)); ?>
                            </div>
                       </div>
                <?php endforeach; ?>
                <?php unset($topic); ?>
            </div>
        </div>
        <div class="pagination pagination-centered">
            <?php echo $this->Paginator->pagination(); ?>
        </div>  
        <?php endif; 
        echo $this->Js->writeBuffer();
        ?>