<?php

	if($current_user['Group']['name'] == 'student'):
		echo $this->element('views/queue-limits', array('userQueues' => $userQueues));
	else:
		echo $this->element('button', array(
			'link' => array('controller' => 'topics', 'action' => 'add'),
			'options' => array(
				'title' => __('Vytvořit téma'),
				'class' => 'btn btn-primary btn-large input-block-level margin-bottom-20'
			)
		)); 
	endif;		