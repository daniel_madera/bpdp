<?php
$is_user_topic = in_array($current_user['Group']['name'], array(
	'superuser',
	'admin',
	'employee'
)) && $topic['Leader']['id'] == $current_user['User']['id'];

if ($is_user_topic) {
	echo $this->element('menu/actions', array( // copy
		'controller' => 'topics',
		'action' => array('add'),
		'id' => $topic['Topic']['id'],
		'options' => array(
			'tooltip' => __('Vytvořit kopii tématu'),
			'icon' => 'fa fa-copy'
		)
	));
	if (in_array($topic['Topic']['status'], array('merged'))) {
		echo $this->element('menu/actions', array(
			'controller' => 'theses',
			'action' => array('index'),
			'options' => array(
				'tooltip' => __('Přejít k oficiálním zadání'),
				'icon' => false,
				'title' => __('Přejít k oficiálním zadání'),
			)
		));

	}
	if (in_array($topic['Topic']['status'], array(
		'active',
		'pending',
		'hidden'
	))) {
		echo $this->element('menu/actions', array(
			'controller' => 'topics',
			'action' => array(
				'edit',
			),
			'id' => $topic['Topic']['id']
		));
		
		$textAdd = '';
		if($topic['Topic']['status'] == 'pending') {
			$textAdd =  __('Všichni studenti, kteří o téma požádali budou z fronty odstraněni.');
		}
		
		echo $this->element('menu/actions', array(
			'controller' => 'topics',
			'action' => array(
				'delete'
			),
			'id' => $topic['Topic']['id'],
			'options' => array(
				'class' => 'btn btn-default js-confirm-op',
                'data-title' => __('Potvrzení odstranění'),
                'data-text' => __('Opravdu chcete odstranit téma?').' '.$textAdd,
			)
		));
	}
		if ($topic['Topic']['status'] == 'hidden') {
			echo $this->element('menu/actions', array(
				'controller' => 'topics',
				'action' => array('show'),
				'id' => $topic['Topic']['id'],
				'options' => array(
					'tooltip' => __('Zobrazit téma studentům')
				)
			));
		} elseif($topic['Topic']['status'] == 'active') {
			echo $this->element('menu/actions', array(
				'controller' => 'topics',
				'action' => array('hide'),
				'id' => $topic['Topic']['id'],
				'options' => array(
					'tooltip' => __('Sktýt téma studentům')
				)
			));
		}
} else if ($current_user['Group']['name'] == 'student' && !in_array($topic['Topic']['status'], array('hidden', 'archived', 'merged'))) {
	$is_already_requested = false;
	foreach ($topic['Queue'] as $queue) {
		if ($queue['user_id'] == $current_user['User']['id']) {
			$is_already_requested = true;
		}
	}
	if (!$is_already_requested) {
		echo $this->Html->link(__('Požádat'), array(
			'action' => 'request',
			$topic['Topic']['id']
		), array('class' => 'btn btn-primary', ));
	}
}
