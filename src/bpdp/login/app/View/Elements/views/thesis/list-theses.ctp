<table class="table">
    <thead>
    <tr>
        <th>#</th> 
        <th><?php echo __('Název');?></th>
        <th><?php echo __('Typ');?></th>
        <th><?php echo __('Vytvořeno');?></th>
        <?php echo ($this->params['action'] == 'admin_index') ? '<th>'.__('Vedoucí').'</th>' : ''; ?>
        <th><?php echo $current_user['Group']['name'] != 'student' ? __('Student') : __('Vedoucí'); ?></th>
        <th><?php echo __('Stav');?></th>
         <?php if(!isset($nomenu)) { ?>
        	<th><?php echo __('Akce');?></th>
        <?php } ?>
    </tr>
    </thead>
    <tbody class="table-striped">

    <?php foreach ($theses as $thesis): ?>
    <tr>
        <td><?php echo $thesis['Thesis']['id']; ?></td>
        <td>
            <?php 
				echo h($thesis['Thesis']['name']);
            ?>
        </td>
         <td>
            <?php echo h($thesis['Type']['abbreviation']); ?>
        </td>
        <td><?php echo $thesis['Thesis']['created']; ?></td>
        <?php echo ($this->params['action'] == 'admin_index') ? '<td>'.h($thesis['Topic']['Leader']['full_name']).'</td>' : ''; ?>
        <td><?php echo h($current_user['Group']['name'] != 'student' ? $thesis['User']['full_name'] : $thesis['Topic']['Leader']['full_name']) ; ?></td>
        <td><?php echo __($thesis['Thesis']['status_text']); ?></td>
        <td>
            <?php  
            
            // echo $this->element('menu/actions', array(
                // 'action' => array('view'),
                // 'id' => $thesis['Thesis']['id']             
            // ));
            if(!isset($nomenu)) {
	            if($current_user['Group']['name'] != 'student' && $thesis['Thesis']['final'] == 0) {
	                echo $this->element('menu/actions', array(
	                    'action' => array('edit', 'delete'),
	                    'id' => $thesis['Thesis']['id']             
	                ));
	            }

                if($current_user['Group']['name'] == 'superuser' && $thesis['Thesis']['final'] == 1) {
                    echo $this->element('button', array(
                        'link' => array(
                            'action' => 'unmakefinal',
                            $thesis['Thesis']['id']
                        ),
                        'options' => array( 
                            'post' => true,
                            'title' => false,
                            'icon' => 'fa fa-mail-reply-all',
                            'tooltip' => __('Obnovit do rozpracovaných')
                        )
                    ));
                }
	            
	            echo $this->element('menu/actions', array(
	                'action' => array('download'),  
	                'id' => $thesis['Thesis']['id'].'.pdf',
	            ));
			}  
            ?>
        </td>
    </tr>
    <?php endforeach; ?>
    <?php unset($theses); ?>
    
    </tbody>
</table>