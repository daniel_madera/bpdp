<div class="<?php echo isset($class) ? $class : "alert alert-info"; ?>">
	<button type="button" class="close" data-dismiss="alert">&times;</button>
	<h4><?php echo isset($title) ? __($title) : __("Informace"); ?></h4>
	<?php echo isset($before) ? __($before) : ""; ?>
	<?php echo __("Žádná data nebyla nalezena."); ?>
	<?php echo isset($after) ? __($after) : ""; ?>
</div> 