<?php

/**
 * optional button
 * @ $options = array()
 * title = false if not to show title, value to show
 * tooltip = false (default), true - to pass title value, value to set own
 * post = false (default), true - to use post link
 * icon = false (default), value - to set own value (example 'icon-trash')
 */
 
$default_options = array(
	'class' => 'btn btn-default btn-xs',
	'type' => 'button',
	'escape' => false,
	'title' => true,
	'tooltip' => false,
	'post' => false,
	'icon' => false,
	'confirm' => false,
);

if(!empty($options)) {
    $options = array_merge($default_options, $options);
} else {
    $options = $default_options;
}

if ($options['title'] === true && isset($link['action'])) {
	$options['title'] = __(Inflector::humanize($link['action']));
} else if ($options['title']) {
	$options['title'] = __($options['title']);
} else {
	$options['title'] = "";
}

if(empty($options['class'])) {
	$options['class'] = '';
}

if (isset($options['icon'])) {
	if ($options['icon'] === true) {
		$icons = array(
		    'add' => 'fa fa-plus',
			'view' => 'fa fa-eye',
			'edit' => 'fa fa-pencil',
			'delete' => 'fa fa-trash-o',
			'default' => 'fa fa-location-arrow',
			'download' => 'fa fa-download',
			'hide' => 'fa fa-eye-slash',
			'show' => 'fa fa-eye',
		);
		if(isset($link['action']) && in_array($link['action'], array_keys($icons))) {
			$icon_class = $icons[$link['action']];
		} else {
			$icon_class = $icons['default'];
		}
	} else {
		$icon_class = $options['icon'];
	}
	
	$options['icon'] = $this->Html->tag('i', '', array('class' => $icon_class));
} else {
	$options['icon'] = "";
}

$separator = "";
if(!empty($options['title']) and !empty($options['icon'])) {
	$separator = "&nbsp;";
}
	
$title = $options['icon'].$separator.$options['title'] ;

if (!$options['title'] && isset($link['action'])) {
	$options['title'] = Inflector::humanize($link['action']);
}

if ($options['tooltip'] === true) {
	$options['data-toggle'] = 'tooltip';

} else if ($options['tooltip']) {
	$options['title'] = $options['tooltip'];
	$options['data-toggle'] = 'tooltip';
}

$class = 'Html';
$function = 'link';
if (isset($link['action']) && $link['action'] == 'delete' || $options['post']) {
	$class = 'Form';
	$function = 'postLink';
}


if(isset($options['override_action']) && $link['action'] != 'delete') {
	$link['action'] = $options['override_action'];
}

//unset those which doesnt belong to link parameters
$confirm = $options['confirm'];
unset($options['post']);
unset($options['icon']);
unset($options['confirm']);
unset($options['override_action']);

echo $this -> $class -> $function($title, $link, $options);
