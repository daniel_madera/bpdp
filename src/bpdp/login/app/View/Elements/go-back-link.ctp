<?php
	echo $this->element('button', 
		array(
			'link' => '/',
			'options' => array(
				'title' => __('Zpět na uvodní stranu'),
				'icon' => 'fa fa-reply',
				'class' => 'btn btn-default btn-small'
			)
		)
	);
?>