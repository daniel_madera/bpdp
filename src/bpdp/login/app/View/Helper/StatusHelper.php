<?php

class StatusHelper extends AppHelper {
	
	public $statusTextsEmployee = array(
		'hidden' => 'Skryté',
	    'active' => 'Volné',
	    'pending' => 'Čekající na potvrzení',
	    'merged' => 'Přiřazené',
	    'archived' => 'Archivované',
	);
	
	public $statusTextsStudent = array(
		'hidden' => 'Skryté',
	    'active' => 'Volné',
	    'pending' => 'Volné (již&nbsp;zažádané)',
	    'merged' => 'Přiřazené',
	    'archived' => 'Archivované',
	);
	
	public function getStatusTextStudent($status) {
		$this->isValidStatus($status);
		return __($this->statusTextsStudent[$status]);
	}
	
	public function getStatusTextEmployee($status) {
		$this->isValidStatus($status);
		return __($this->statusTextsEmployee[$status]);
	}
	
	private function isValidStatus($status) {
		if(array_key_exists($status, $this->statusTextsStudent) && array_key_exists($status, $this->statusTextsEmployee)) {
			return true;
		}
		throw new NotFoundException(__('Status %s nebyl nalezen.', $status));
	}
}
