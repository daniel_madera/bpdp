<?php

class TopicHelper extends AppHelper {
	
	public function findQueueByUserId($queues, $user_id) {
		foreach($queues as $queue) {
			if($queue['user_id'] == $user_id) {
				return array('Queue' => $queue);
			}
		}
		return null; 
	}
	
}
	