<?php

class DepartmentHelper extends AppHelper {
	public function getOneRowList($outerDepartments, $departments) {
		$departments_string = "";
		$department_ids = array(); 
        if(!empty($outerDepartments)) {
            foreach($outerDepartments as $department) {
                $department_ids[] = $department['id'];
            }
			$departments_new = array();
			foreach($department_ids as $department_id) {
				$departments_new[] = $departments[$department_id];
			}
			sort($departments_new);
			$parent_department = '';
			$first = true;
			foreach ($departments_new as $department) {
				$pos = strpos($department['name'], '->') + 3;
				$tmp_parent_department = substr($department['name'], 0, $pos);
				$department = substr($department['name'], $pos);
				if($parent_department != $tmp_parent_department) {
					if(!$first) {
						$departments_string .= ", ";
					}
					$departments_string .= substr($tmp_parent_department, 0, -4). ": ";
					$first = false;
				}
				$departments_string .= $department . " ";
				$parent_department = $tmp_parent_department;								
			}
        }
		return $departments_string;
	}
}