
<?php
if(!empty($users)):
?>
	<?php
		echo $this->element('forms/create', array(
				'model' => 'User', 
	 			'options' => array(
					'class' => 'form-inline',
					'inputDefaults' => array(						
						'label' => false, 
						'div' => false
					),
					'id' => 'user-search-form'
				)
			) 
		);
		echo $this->Form->input(
	        'search', array(
	        	'label' => __('Vyhledat').':  ',
	            'placeholder' => __('Zadejte email nebo jméno'),
	            'class' => 'input-xxlarge',
	            'div' => false
	        )); 
			
        echo $this->element('forms/submit', array(
                'title' => "<i class='fa fa-search'></i>",
                'options' => array(
                    'class' => 'btn',
                    'escape' => false,
                    'default' => false,
                    'span12' => false
                )
            ) 
        );
	?> 

<table class="table">
    <thead>
    <tr>
        <th>#</th>
        <th><?php echo __('Jméno'); ?></th>
        <th><?php echo __('Email'); ?></th>
        <th><?php echo __('Oprávnění'); ?></th>
        <th><?php echo __('Vytvořeno'); ?></th>
        <th><?php echo __('Přihlášení'); ?></th>
    </tr>
    </thead>
    <tbody class="table-striped">
    <?php foreach ($users as $user): ?>
    <tr>
        <td><?php echo $user['User']['id']; ?></td>
        <td><?php echo h($user['User']['full_name']); ?></td>
        <td>
            <?php echo $this->Html->link($user['User']['email'], 'mailto:'.$user['User']['email']); ?>
        </td>
        <td>
            <?php echo $user['Group']['name']; ?> 
        </td>
        <td><?php echo h($user['User']['created']); ?></td>
        <td><?php echo $this->Html->link('<i class="fa fa-arrow-right"></i>', array('controller' => 'users', 'action' => 'login', $user['User']['email']), array(
            'escape' => false,
            'class' => 'btn btn-small'
        )); ?></td>
    </tr>
    <?php endforeach; ?>
    <?php unset($user); ?>
    
    </tbody>
</table>
<div class="pagination pagination-centered">
	<?php echo $this->Paginator->pagination(); ?>
</div>  
<?php
else:
echo $this->element('views/records-not-found');

endif;
?>