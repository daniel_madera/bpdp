<div class="row-fluid">
	<div class="span12">
		<div class="page-header">
		  <h3>
		      <?php echo h($current_user['User']['full_name']); ?>
		      <br />
		      <small>
		          <?php echo h($current_user['User']['email']); ?>
		      </small>
		  </h3> 
		</div>
	</div>
</div>	
<div class="row-fluid">
	<div class="span6 well">
	<h3><?php echo __('Nastavení'); ?></h3>
	<?php
		echo $this->element('forms/user-settings');
	?>		
	</div>
	<div class="span6">
		<div class="well">		
			<dl class="dl-horizontal">
				<?php
					echo '<dt>'.__('Vytvořeno').'</dt>'; 
		  			echo '<dd>'.h($current_user['User']['created']).'</dd>';
					echo '<dt>'.__('Poslední přihlášení').'</dt>';
		  			echo '<dd>'.h($current_user['User']['last_login']).'</dd>';
					echo '<dt>'.__('Oprávnění').'</dt>';
		  			echo '<dd>'.h($current_user['Group']['name']).'</dd>';
		  			if($current_user['Group']['name'] != 'student') {
						echo '<dt>'.__('Ústav').'</dt>';
		  				echo '<dd>'.h($current_user['Institute']['abbreviation']).'</dd>';
		  			}
					echo '<dt>'.__('Email').'</dt>';
		  			echo '<dd>'.h($current_user['User']['email']).'</dd>'; 
				?>
			</dl>
		</div>
		<?php echo $this->element('views/queue-limits'); ?>
		<?php echo $this->element('views/theses-merged'); ?>
	</div>
</div>
