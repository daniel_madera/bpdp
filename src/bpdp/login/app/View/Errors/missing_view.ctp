<?php 
$this->layout = 'error';
?>
<h3><?php printf('%s - %s',__('V aplikaci došlo k chybě'), __('stránka nebyla nalezena')); ?></h3>
<p class="error">
	<strong><?php echo 'Error 404.'; ?>: </strong>
	<?php echo __('Požadovaná stránka na serveru chybí.'); ?>
</p>