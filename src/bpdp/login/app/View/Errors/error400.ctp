<?php
 $this->layout = 'error';
?>
<h3><?php printf('%s - %s',__('Upozornění'), __('v aplikaci došlo k chybě')); ?></h3>
<p class="error">
	<?php echo '<strong>'.__('Detail chyby').':</strong> '.__($name); ?>
	<br>
	<?php printf(
		__('Neplatný dotaz: %s'),
		"<strong>'{$url}'</strong>"
	); ?>
</p>
<?php
if (Configure::read('debug') > 0):
	echo $this->element('exception_stack_trace');
	echo $this->element('sql_dump');
endif;
?>
