<?php 
$this->layout = 'error';
?>
<h3><?php printf('%s - %s',__('Nepodařilo se provést dotaz na DB'), __('v aplikaci došlo k chybě')); ?></h3>

<p class="error">
	<strong><?php echo __('Chyba databáze'); ?>: </strong>
	<?php 
	if(!empty($message)) {
		echo __($message); 
	}
	echo __($name); 
	?>
</p>
<?php echo $this->element('errors/admin-contact');?>

<?php if (!empty($error->queryString)) : ?>
	<p class="notice">
		<strong><?php echo __d('cake_dev', 'SQL Query'); ?>: </strong>
		<?php echo h($error->queryString); ?>
	</p>
<?php endif; ?>
<?php if (!empty($error->params)) : ?>
		<strong><?php echo __d('cake_dev', 'SQL Query Params'); ?>: </strong>
		<?php echo Debugger::dump($error->params); ?>
<?php endif; ?>