<?php
  $this->layout = 'error';
?>
<h3><?php printf('%s - %s',__('Upozornění'), __('v aplikaci došlo k chybě')); ?></h3>
<p class="error">
	<?php echo '<strong>'.__('Detail chyby').':</strong><br>'.__($name); ?>
</p>
<?php echo $this->element('errors/admin-contact');?>
