<?php
 $this->layout = 'error';
?>
<h3><?php printf('%s - %s',__('Internal error'), __('v aplikaci došlo k chybě')); ?></h3>
<p class="error">
	<?php 
		echo '<strong>'.__('Detail chyby').':</strong> ';
		if(!empty($message)) {
			echo __($message); 
		}
		echo __($name);  
	
	?>
</p>
<?php echo $this->element('errors/admin-contact');?>
<?php
if (Configure::read('debug') > 0):
	echo $this->element('exception_stack_trace');
	echo $this->element('sql_dump');
endif;
?>
