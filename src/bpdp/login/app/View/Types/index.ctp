<div class="row-fluid">
    <div class="span6">
        <h3>Typy studentských prací</h3>
        <div class="well">
         <table class="table">
        <thead>
        <tr>
            <th>#</th>
            <th><?php echo __('Název'); ?></th>
            <th><?php echo __('Zkratka'); ?></th>
            <th><?php echo __('Popis'); ?></th>
            <th><?php echo __('Rozsah práce'); ?></th>
            <th></th>
        </tr>
        </thead>
        <tbody class="table-striped">
        <?php foreach ($types as $type): ?>
        <tr>
            <td><?php echo $type['Type']['id']; ?></td>
            <td>
                <?php echo h($type['Type']['name']); ?>
            </td>
            <td>
                <?php echo h($type['Type']['abbreviation']); ?> 
            </td>
            <td><?php echo $this->element('modal-text', array('text' => h($type['Type']['description']), 'title' => __('Popis'), 'maxLength' => 20)) ?></td>
            <td><?php echo $this->element('modal-text', array('text' => h($type['Type']['range']), 'title' => __('Rozsah práce'), 'maxLength' => 20)) ?></td>
            <td><?php echo $this -> element('menu/actions', array(
					'action' => array(
						'edit',
						'delete'
					),
					'controller' => 'types',
					'id' => $type['Type']['id'],
					'options' => array('class' => 'btn btn-small',
					'override_action' => 'index')
				));
 			?></td>
        </tr> 
        <?php endforeach; ?>
        <?php unset($types); ?>
        
        </tbody>
    </table>
         </div>
    </div>
    <div class="span6">
    	<?php echo $this -> element('forms/type'); ?>
    </div>
    <div class="row-fluid">
        <div class="span6">
            <?php echo $this->element('cancel-button'); ?>
        </div>
        <div class="span6">
            <?php echo $this->element('forms/submit'); ?>
        </div>
    </div>
</div>
