<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc.
 * (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts.Email.html
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
	<title><?php printf('%s - %s', Configure::read('Application.name'), Configure::read('Application.slogan')); ?></title>
</head>
<body>
	<?php
		if(!empty($debug)) {
			echo '<p>';	
			printf('<strong>%s:</strong> %s', __('Debug report'), $debug);
			echo '</p>';
		}
 	?>
	<p><?php echo __('Dobrý den') . ',<br>' . __('tento email je automaticky generován z aplikace %s.', sprintf('<strong>%s - %s</strong>', Configure::read('Application.name'), Configure::read('Application.slogan'))); ?> </p>	
	<?php echo $this->fetch('content'); ?>
	<p>
		<?php echo __('Přístup do aplikace') . ' - ' . (!isset($url) ? (Configure::read('Application.url')) : $url); ?>	
	</p>
</body>
</html>