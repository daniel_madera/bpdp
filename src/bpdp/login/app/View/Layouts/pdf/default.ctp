<!DOCTYPE html>
<html lang="en">
    <head>
        <style>
            <?php echo file_get_contents(APP . 'webroot' . DS . 'css' . DS . 'pdf.css');?>
        </style>
    </head>  
    <body>
        <div class="container">
             <?php echo $this->fetch('content'); ?>
        </div>
    </body>
</html>