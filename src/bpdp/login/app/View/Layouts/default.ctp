<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $this->Html->charset(); ?> 
        <title>
        	<?php
        	if(!empty($title_for_browser)) {
				$title_valid = $title_for_browser;
			} else if(!empty($title_for_layout)) {
				$title_valid = $title_for_layout;
			} else if(!empty($subtitle_for_layout)) {
				$title_valid = $subtitle_for_layout;
			} else {
				$title_valid = Configure::read("Application.slogan");
			}
        	?>
            <?php echo __("%s: %s", Configure::read("Application.name"), $title_valid); ?>
        </title>
        <?php
		echo $this->Html->meta('favicon.ico','/favicon.ico', array('type' => 'icon'));
		echo $this->Html->meta(array('name' => 'viewport', 'content' => 'width=device-width, initial-scale=1.0'));
        echo $this->Html->css('bootstrap.min');
        echo $this->Html->css('bootstrap-responsive.min');
		echo $this->Html->css('font-awesome.min');
        echo $this->Html->css('jquery-ui.min');
        echo $this->Html->css('layout');
		echo $this->fetch('css');
		echo $this->fetch('meta');
        echo $this->Html->script('jquery');
        echo $this->Html->script('jquery-ui.min');
        echo $this->Html->script('jquery-ui-datepicker-locale-'.Configure::read('App.locale'));
		echo $this->Html->script('jquery-ui-timepicker-locale-'.Configure::read('App.locale'));        
        echo $this->Html->script('common');
        echo $this->Html->script('bootstrap.min');
        echo $this->Html->script('bootbox.min');
        ?>
    </head> 
    <body>
        <div class="container"> 
			<div class="row-fluid">
				<div class="span12">
		            <?php
		            echo $this->element('menu/main');
		            ?>
		            
		            <div id="main-content">
		                <?php
		                echo (!empty($title_for_layout) ? '<h3>'.__($title_for_layout).'</h3>': '');
						echo $this->Session->flash('auth');	
		                echo $this->Session->flash();
						?>
		                <div id="next-content">
		                	<?php 
		                	 echo $this->fetch('content'); 
		                	 echo $this->Js->writeBuffer();
		                	?>
		                	
						</div>
		            </div>
		       </div>
        	</div><!-- /.row-fluid -->
        	<div class="row-fluid">
				<div class="span12">
					 <div id="footer" class="well well-small text-center" style="margin-top: 10px;">
		            	<span>
		            		<?php printf('&copy; 2014 <strong>%s</strong> - %s, %s %s',
		            		Configure::read('Application.name'),
		            		Configure::read('Application.slogan'),  
		            		Configure::read('Application.univerzity.faculty_short'),
		            		Configure::read('Application.univerzity.name_short')); ?>
		            	</span>		            	
		            </div>
        		</div>
        	</div>
        	<?php
            if (Configure::read('debug') > 0):
				echo $this->element('sql_dump');
			endif;
            ?>
        </div>
        <div id="overlay-wrapper">
    	<div id="overlay"></div>
    		<?php
				echo $this->Html->image(
		            'indicator.gif',
		            array(
		            	'id' => 'busy-indicator',
					)
		        );
			?>
    	</div>
        <?php
        	echo $this->Js->writeBuffer();
        ?>
    </body>
</html>
