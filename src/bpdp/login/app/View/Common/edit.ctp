<?php
$model = Inflector::classify($this->params['controller']);
echo $this->element('forms/create', array(
    'model' => $model,
));
echo $this->element('forms/'.strtolower($model));
echo $this->Form->input('id', array('type' => 'hidden'));
echo $this->element('forms/submit', array('cancel' => true));

