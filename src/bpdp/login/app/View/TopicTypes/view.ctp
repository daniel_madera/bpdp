<table class="table">
    <thead>
    <tr>
        <th>#</th>
        <th><?php echo __('Název');?></th>
        <th><?php echo __('Zkratka');?></th>
        <th><?php echo __('Popis');?></th>
    </tr>
    </thead>
    <tbody class="table-striped">
    <?php foreach ($topic_types as $topic_type): ?>
    <tr>
        <td><?php echo $topic_type['TopicType']['id']; ?></td>
        <td>
            <?php echo $this->Html->link(h($topic_type['TopicType']['name']), array('controller' => 'topic_types', 'action' => 'view', $topic_type['TopicType']['id'])); ?>
        </td>
        <td>
            <?php echo h($topic_type['TopicType']['abbreviation']); ?> 
        </td>
        <td><?php echo h($user['TopicType']['description']); ?></td>
    </tr>
    <?php endforeach; ?>
    <?php unset($topic_types); ?>
    
    </tbody>
</table>
