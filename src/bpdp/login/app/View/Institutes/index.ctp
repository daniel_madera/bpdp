<div class="row-fluid">
    <div class="span6">
        <h3>Ústavy na fakultě</h3>
        <div class="well">
         <?php
            foreach($institutes as $institute) {
                $actions = $this->element('menu/actions', array(
                    'controller' => 'institutes',
                    'action' => array('edit', 'delete'),
                    'id' => $institute['Institute']['id'],
                    'options' => array('class' => 'btn btn-small',
					'override_action' => 'index')
                ));
                
                $format = '<span class="inblock" style="width: 465px;">%s (%s)</span>'.$actions;
                echo sprintf($format.'<br/>', h($institute['Institute']['name']), h($institute['Institute']['abbreviation']));                
            }
         ?>
         <?php unset($institutes); ?>
         </div>
    </div>
    <div class="span6">
    	<?php echo $this->element('forms/institute'); ?>
    </div>
    <div class="row-fluid">
        <div class="span6">
            <?php echo $this->element('cancel-button'); ?>
        </div>
        <div class="span6">
            <?php echo $this->element('forms/submit'); ?>
        </div>
    </div>
</div>
