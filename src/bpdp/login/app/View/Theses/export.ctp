<?php

echo $this->Html->script('thesis-export');

echo $this->element('forms/create', array(
	'model' => 'Thesis',
	'options' => array('id' => 'thesis-exort-form')
));
?>
<div class="row-fluid">
	<div class="span6">
		<div class="well">
		<?php
		if($current_user['Group']['name'] == 'superuser') {
			echo $this->Form->input('institute_id', array( 
				'options' => $institutes,
				'default' => $current_user['Institute']['id'],
				'label' => __('Vyberte ústav pro export'),
				'div' => false,
			));
		} else {
			echo '' . __('Export se provádí pro ústav %s', '<strong>'.$current_user['Institute']['abbreviation'].'</strong>') . '';
			echo $this->Form->input('institute_id', array( 
				'type' => 'hidden',
				'value' => $current_user['Institute']['id']
			));
		}
		?>
		</div>
		<div class="well">
		<?php
		echo $this->Form->input('final', array(
			'options' => array(
				'1' => __('Zaslané k zpracování'),
				'0' => __('Rozpracované'),
				'archived' => __('Archivované'),
			),
			'selected' => array('1'),
			'multiple' => 'checkbox',
			'div' => false,
			'label' => __('Vyberte status zadání') 
		));
		
		?>
		</div>
		<div class="well">
			<?php
			echo $this->Form->input('columns', array(
				'options' => array(
					'Thesis.id' => __('ID zadání'),
					'Thesis.name' => __('Název zadání'),
					'Thesis.name-en' => __('Anglický název zadání'),
					'Thesis.status-text' => __('Stav zadání'),
					'Type.name' => __('Typ práce'),
					'User.full-name' => __('Jméno studenta'),
					'User.email' => __('Email studenta'), 
					'Topic.Leader.full-name' => __('Jméno vedoucího'),
					'Topic.Leader.email' => __('Email vedoucího'),
					'Topic.Institute.name' => __('Název ústavu vedoucího'),
					'Topic.Institute.abbreviation' => __('Zkratka ústavu vedoucího'),
					'Thesis.modified' => __('Datum poslední změny zadání'),
					'Thesis.created' => __('Datum vytvoření zadání'),
					'Requirement.%d.text' => __('Zásady pro vypracování'),
					'Literature.%d.text' => __('Literatura'),
					'Topic.abstract' => __('Abstract tématu'),
					'Topic.requirements' => __('Požadavky na studenta tématu'),
					'Topic.consultant' => __('Konzultant')
				),
				'empty' => __('Vyberte'),
				'label' => __('Vyberte sloupce, které se mají exportovat'),
				'id' => 'thesis-export-form-columns-select',
				'div' => false
			));
			?>

			<div id="thesis-export-form-columns-container">

			</div>
		</div>
	</div>
	<div class="span6">
		<div class="well">
			<?php
			echo $this->Form->input('sort', array(
				'options' => array(
					'Thesis.id' => __('ID zadání'),
					'Thesis.name' => __('Název zadání'),
					'Thesis.name-en' => __('Anglický název zadání'),
					'Thesis.status-text' => __('Stav zadání'),
					'User.name' => __('Jméno studenta'),
					'User.email' => __('Email studenta'), 
					'Topic.Leader.name' => __('Jméno vedoucího'),
					'Topic.Leader.email' => __('Email vedoucího'),
					'Topic.Institute.name' => __('Název ústavu vedoucího'),
					'Topic.Institute.abbreviation' => __('Zkratka ústavu vedoucího'),
					'Thesis.modified' => __('Datum poslední změny zadání'),
					'Thesis.created' => __('Datum vytvoření zadání'),
				),
				'label' => __('Řazení zadání podle'),
				'div' => false,
				'default' => 'Thesis.created'
			));
			
			echo $this->Form->input('direction', array( 
				'options' => array(
					'ASC' => __('Vzestupně'),
					'DESC' => __('Sestupně'),
				),
				'label' => __('Směr řazení'),
				'div' => false,
				'default' => 'ASC'
			));
				
			?>
		</div>
		<div class="well" id="thesis-export-date-picker">
			<div class="row-fluid">
				<div class="span6">
					<script>
						$(function() {
							$(".datepicker").datepicker({
								dateFormat : 'yy-mm-dd'
							//}).attr('readonly', 'readonly');
							});
							//$("#thesis-export-date-picker").hide();
						});
					</script>
					<?php
					echo $this->Form->input('created_from', array(
						'class' => 'datepicker',
						'type' => 'text',
						'label' => __('Zadání od data vytvoření'),
						'div' => false
					));
					?>
				</div>
				<div class="span6">
					<?php
					echo $this->Form->input('created_to', array(
						'class' => 'datepicker',
						'type' => 'text',
						'label' => __('Zadání do data vytvoření'),
						'div' => false
					));
					?>
				</div>
			</div>
		</div>
		<?php
		echo $this->Form->input('file_type', array(
			'options' => array(
				'csv_CP1250' => __('CSV soubor s kódovaním CP1250 (vhodné pro Excel)'),
				'csv_UTF-8' => __('CSV s kódovaním UTF-8'),
				'txt' => __('TXT soubor'),
				'xml' => __('XML soubor'),
			),
			'label' => __('Vyberte typ souboru pro export')
		));
		?>
	</div>
</div>
<div class="row-fluid">
	<div class="span6">
		<?php echo $this->element('cancel-button'); ?>
	</div>
	<div class="span6">
		<?php
			echo $this->element('forms/submit', array(
				'options' => array('id' => 'thesis-form-submit-button', ),
				'title' => __('Exportovat')
			));
			// 
			// $this->Js->get('#ThesisFinalArchived');
			// $this->Js->event('click', '
			// $("#thesis-export-date-picker").toggle();
			// return true;
			// ');

			$this->Js->get('#thesis-export-form-columns-select');
			$this->Js->event('change', '
				if($(this).val() == "") return false;
				$("#thesis-export-form-columns-container").append(create_input($(this).val(), $(this).find("option:selected").text()));
				$(this).val("");
			');
		?>
	</div>
</div>

<?php echo $this->Js->writeBuffer(); ?>