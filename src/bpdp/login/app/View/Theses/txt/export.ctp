<?php
$this->response->type('Content-Type: text/csv');

define('cEOL', "\r\n");
$separator = cEOL."------------------%s-----------------".cEOL;
echo sprintf($separator, 'HEADER');
foreach (array_keys($export_data[0]) as $value) {
    echo $value.cEOL;
}
echo sprintf($separator, 'VALUES');
foreach($export_data as $export) {
    foreach ($export as $value) {
        
        if(is_array($value)) {
            foreach($value as $key => $val) {
               echo sprintf('%d. %s', $key + 1, $val).cEOL; 
            }    
        } else {        
            echo $value.cEOL;
        }
    }
    echo sprintf($separator, 'NEXT');
}
