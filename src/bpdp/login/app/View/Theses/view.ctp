<head>
    <style>
        body {font-style: normal; font-family: Georgia, Arial, sans-serif; font-size: 13px;}
        table td {font-weight: bold; letter-spacing: 1px;}
        table th {font-weight: normal;}
        table td, .table th {padding: 5px; text-align: left; vertical-align: top;}
        .center {text-align: center; width: 100%;} 
        .div-top-margin {margin-top: 50px;} 
        .uppercase {text-transform: uppercase;}
        .no-bold {font-weight: normal;}
        .heading-list {font-family: Courier New, Courier, monotype; font-size: 14px;letter-spacing: 3px;}
        
        #main-title {margin-top: 30px; margin-bottom: 40px;}
        #thesis-detail {margin-top: 5px; }
        
        h1 {font-family: Garamond, Times, serif;}
        h5 {font-size: 160%; font-weight: normal;}
        h6 {font-size: 140%;}
        
    </style>
</head>

<div class="container"> 
    <div class="center"> 
        <h4>
            <span class="uppercase"><?php echo __(Configure::read('Application.univerzity.name'));?></span>
            <br />
            <span class="no-bold"><?php echo __(Configure::read('Application.univerzity.faculty'));?></span>
        </h4>
    </div> 
    <div class="center" id="main-title">
        <h1 class="uppercase"><?php echo __('Zadání - %s', $thesis['Type']['name']);?></h1>
    </div>
    <table id="thesis-detail"> 
        <tbody>
            <tr><th><?php echo __('Jméno a příjmení:');?></th><td><?php echo h($thesis['User']['full_name']);?></td></tr>
            <tr><th><?php echo __('Vedoucí práce:');?></th><td><?php echo h($thesis['Topic']['Leader']['full_name']);?></td></tr>
            <tr><th><?php echo __('Název tématu:');?></th><td><?php echo h($thesis['Thesis']['name']);?></td></tr>
            <tr><th><?php echo __('Zadávající katedra:');?></th><td><?php echo h($thesis['Topic']['Institute']['name']);?></td></tr>
        </tbody>
    </table>
    <div id="requirements" class="div-top-margin">
        <?php if(!empty($thesis['Requirement'])): ?>
            <div class="center">
                <span class="heading-list"><?php echo __('Zásady pro vypracování:');?></span>
            </div>
        <?php endif; ?>
        <?php foreach($thesis['Requirement'] as $index => $requirement): ?>
            <p class="requirement">
                <?php echo __('%d. %s', $index + 1, h($requirement['text'])); ?>
            </p>
        <?php endforeach; ?>   
    </div>
    <div id="literature" class="div-top-margin">
        <?php if(!empty($thesis['Literature'])): ?>
            <div class="center">
                <span class="heading-list"><?php echo __('Doporučená literatura:');?></span>
            </div>
        <?php endif; ?>
        <?php foreach($thesis['Literature'] as $index => $literature): ?>
            <p class="requirement">
                <?php echo __('[%d] %s', $index + 1, h($literature['text'])); ?>
            </p>
        <?php endforeach; ?>   
    </div>
</div>