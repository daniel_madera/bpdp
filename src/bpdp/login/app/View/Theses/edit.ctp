<?php
echo $this->element('forms/create', array(
    'model' => 'Thesis',
    'options' => array(
        'id' => 'thesis-form'
    )
));
echo $this->element('forms/'.strtolower('Thesis'));
echo $this->Form->input('id', array('type' => 'hidden'));

?>
<div class="row-fluid">
<div class="span4">    
<?php echo $this->element('cancel-button'); ?>
</div>
<div class="span4">    
<?php echo $this->element('forms/submit', array(
	'title' => __('Pouze uložit'),
    'options' => array(
        'type' => 'submit',
        'id' => 'thesis-form-submit-button-save-only',
        'name' => 'data[Thesis][final]',
        'value' => '0',
        'class' => 'btn btn-default btn-large btn-block',
        'data-submit' => '#thesis-form'
    ) 
));
?>
</div>
<div class="span4">
<?php echo $this->element('forms/submit', array(
	'title' => __('Poslat k dalšímu zpracování'),
    'options' => array(
        'type' => 'submit',
        'name' => 'data[Thesis][final]',
        'value' => '1',
        'id' => 'thesis-form-submit-button-final',
        'class' => 'btn btn-primary btn-large btn-block',
        'data-title' => __('Potvrzení žádosti'),
		'data-text' => __('Po zvolení poslat k dalšímu zpracování již 
			nebude možné editovat zadání. Pro editaci budete nuceni 
			požádat administrátora vašeho ústavu.'),
		'data-submit' => '#thesis-form'
    ) 
));
	?>
</div>