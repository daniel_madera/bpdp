<?php
//die($encoding);
//$this->response->type("content-type:application/csv;charset=UTF-8");
$this -> CSV -> addRow(array_keys($export_data[0]));
foreach ($export_data as $export) {
    foreach($export as &$data) {
        if(is_array($data)) {
            $output = "";
            foreach($data as $key => $value) {
                $output .= sprintf('%d. %s || ', $key + 1, $value);
            }
            $data = substr($output, 0, -4);
        }
        // if(isset($encoding)) {
        //     $data = iconv('UTF-8', $encoding, $data); 
        // }
        //$data = iconv('UTF-8', 'CP1250', $data); 
    }        
    $this -> CSV -> addRow($export);
}
$filename = __('export');
echo $this -> CSV -> render(false, $encoding);
