<div class="center">
    <h4>
        <span class="uppercase"><?php echo __(Configure::read('Application.univerzity.name')); ?></span>
        <br />
        <?php echo __(Configure::read('Application.univerzity.faculty')); ?>
        <br />
        <span class="no-bold"><?php echo __('Akademický rok:'); ?></span>
        <?php 
            $year = date('Y', strtotime($thesis['Thesis']['created']));
            echo $year.'/'.($year + 1);
        ?>
    </h4>
</div> 
<div class="center" id="main-title">
    <h1 class="uppercase">
    	<?php 
    	if($thesis['Thesis']['final'] > 0) {
    		echo __('Zadání %s', $thesis['Type']['name']);
		} else {
			echo '<span class="text-red">'.__('Rozpracované zadání %s', $thesis['Type']['name']).'</span>';
		} 
    	?>
    </h1>
</div>
<table class="table" id="thesis-detail"> 
    <tbody>
        <tr><th><?php echo __('Jméno a příjmení:'); ?></th><td><?php echo h($thesis['User']['full_name']); ?></td></tr>
        <tr><th><?php echo __('Název práce:'); ?></th><td><?php echo h($thesis['Thesis']['name']); ?></td></tr>
        <tr><th><?php echo __('Zadávající katedra:'); ?></th><td><?php echo h($thesis['Topic']['Institute']['name']); ?></td></tr>
        <tr><th><?php echo __('Vedoucí práce:'); ?></th><td><?php echo h($thesis['Topic']['Leader']['full_name']); ?></td></tr>
        <tr><th><?php echo __('Rozsah práce:'); ?></th><td><?php echo ($thesis['Type']['range']); ?></td></tr>
        <?php if(!empty($thesis['Topic']['consultant'])): ?>
            <tr><th><?php echo __('Konzultant:'); ?></th><td><?php echo h($thesis['Topic']['consultant']); ?></td></tr>
        <?php endif; ?>
    </tbody>
</table>
<br>
<br>
<br>
<div id="requirements" class="">
    <?php if(!empty($thesis['Requirement'])): ?>
        <div class="center">
            <span class="heading-list"><?php echo __('Zásady pro vypracování:'); ?></span>
        </div>
    <?php endif; ?>

    <?php $counter = 0; foreach($thesis['Requirement'] as $index => $requirement): ?>
        <p class="requirement <?php echo ($counter == 0 ? "noident" : ""); ?>">
            <?php echo __('<div class="number">%d.</div><div class="text">%s</div>', $index + 1, nl2br(h($requirement['text']))); ?>
            <?php // echo nl2br(h($requirement['text'])); $counter++;?>
        </p>
    <?php endforeach; ?>   
</div>
<br>
<div class="no-pagebreak">
    <span id="literature">
            <?php if(!empty($thesis['Literature'])): ?>
                <p class="center">
                    <span class="heading-list"><?php echo __('Seznam odborné literatury:'); ?></span>
                </p>
            <?php endif; ?>
            <?php foreach($thesis['Literature'] as $index => $literature): ?>
                <p class="literature">
                    <?php echo __('<div class="number">[%d]</div><div class="text">%s</div>', $index + 1, nl2br(h($literature['text']))); ?>
                </p>
            <?php endforeach; ?>   
    </span>    
    <span>
        <div id="date-footer" style="width: 40%;  padding-top: 30px; background: white;">
            <?php echo __('V Liberci dne').' ..........................'; ?>        
        </div>
        <div style="margin-top: -16px; text-align: right; width: 100%;">
            <?php 
                $delimeter = '.....................................................';
                echo $delimeter.'<br>'; ?>
            <?php echo h($thesis['Topic']['Leader']['full_name']); ?>
            <?php
                $len = (int) ((36-strlen(h($thesis['Topic']['Leader']['full_name'])))/2);

                for($i = 0; $i < $len; $i++) {
                    echo "&nbsp;";
                }
            ?>
        </div>
    </span>
    
</div>