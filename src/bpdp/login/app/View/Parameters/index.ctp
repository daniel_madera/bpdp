<div class="row-fluid">
    <div class="span6">
        <h3>Vlastnosti vyhledáváných témat</h3>
        <div class="well">
         <table class="table">
        <thead>
        <tr>
            <th>#</th>
            <th><?php echo __('Název'); ?></th>
            <th><?php echo __('Popis'); ?></th>
            <th></th>
        </tr>
        </thead>
        <tbody class="table-striped">
        <?php foreach ($parameters as $parameter): ?>
        <tr>
            <td><?php echo $parameter['Parameter']['id']; ?></td>
            <td>
                <?php echo h($parameter['Parameter']['name']); ?>
            </td>
            <td><?php echo $this->element('modal-text', array('text' => h($parameter['Parameter']['description']), 'title' => __('Popis'), 'maxLength' => 30)) ?></td>
            <td><?php echo $this -> element('menu/actions', array(
					'action' => array(
						'edit',
						'delete'
					),
					'controller' => 'parameters',
					'id' => $parameter['Parameter']['id'],
					'options' => array('class' => 'btn btn-small',
					'override_action' => 'index')
				));
 			?></td>
        </tr> 
        <?php endforeach; ?>
        <?php unset($parameters); ?>
        
        </tbody>
    </table>
         </div>
    </div>
    <div class="span6">
    	<?php echo $this -> element('forms/parameter'); ?>
    </div>
    <div class="row-fluid">
        <div class="span6">
            <?php echo $this->element('cancel-button'); ?>
        </div>
        <div class="span6">
            <?php echo $this->element('forms/submit'); ?>
        </div>
    </div>
</div>