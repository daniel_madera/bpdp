<?php
echo '<p>' . __('
	Student (%s) požádal o Vaše téma číslo %s.', 
	$queue['User']['full_name'], $queue['Topic']['id']).'<br>'.
	__('V aplikaci můžete studentovi téma přiřadit nebo ho odmítnout smazáním.')
	.'</p>';
?>
<strong><?php echo __('Detail požádaného tématu'); ?></strong>
	<br/>
	 <?php echo _('ID') . ': '; ?>
	 <?php echo $queue['Topic']['id']; ?>
	 <br/>
	 <?php echo _('Název') . ': '; ?>
	 <?php echo h($queue['Topic']['name']); ?>
	 <br/>
	 <?php echo _('Typ práce') . ': '; ?>
	 <?php echo h($queue['Type']['name']); ?>
	 <br/>
	 <?php echo _('Student') . ': '; ?>
	 <?php echo '<strong>' . h($queue['User']['full_name']) . '</strong>' . sprintf(' (%s)', h($queue['User']['email'])); ?>
	 <br/>
	 <?php echo _('Komentář') . ': '; ?>
	 <?php echo h($queue['Queue']['comment']); ?>
	 
<?php
echo '<p>' . __('
	Téma zůstává volné pro ostatní studenty, dokud k němu nepřiřadíte některého studenta.
	Po přiřazení budou všichni ostatní studenti, kteří požádali o téma, informováni a odstraněni.
') . '</p>';
	 
