<?php
echo '<p>' . __('
	Vedoucí práce (%s) Vás přiřadil k tématu číslo %s. 
	Automaticky byly zrušeny všechny Vaše ostatní žádosti o přiřazení k jiným tématům.
	', h($topic['Leader']['full_name']), $topic['Topic']['id']) . '</p>';

echo '<p>' . __('
	Vedoucí práce nyní vypracuje oficiální zadání, které naleznete v sekci "Oficiální zadání".
	V případě jakýchkoliv změn, kontaktujte přímo vedoucího práce.
	') . '</p>';
?>
<strong><?php echo __('Detail požádaného tématu'); ?></strong>
	<br/>
	 <?php echo _('ID') . ': '; ?>
	 <?php echo $topic['Topic']['id']; ?>
	 <br/>
	 <?php echo _('Název') . ': '; ?>
	 <?php echo h($topic['Topic']['name']); ?>
	 <br/>
	 <?php echo _('Typ práce') . ': '; ?>
	 <?php echo h($topic['Thesis']['Type']['name']); ?>
	 <br/>
	 <?php echo __('Přiřazený student') . ': '; ?>
	 <?php echo h($topic['Thesis']['User']['full_name']); ?>
	 <br/>
	 <?php echo _('Vedoucí') . ': '; ?>
	 <?php echo '<strong>' . h($topic['Leader']['full_name']) . '</strong>' . sprintf(' (%s)', $topic['Leader']['email']); ?>	 

	 
