<?php
echo '<p>' . __('
	Vedoucí práce (%s) odstranil oficiální zadání k tématu číslo %s. 
	Pokud si nejste vědomi této operace, prosím kontaktujte neprodleně vedoucího práce.
	', h($thesis['Topic']['Leader']['full_name']), $thesis['Topic']['id']) . '</p>';
?>
<strong><?php echo __('Detail požádaného tématu');?></strong>
	 <br/>
	 <?php echo __('ID').': ';?>
	 <?php echo $thesis['Topic']['id'];?> 
	 <br/>
	 <?php echo __('Název').': ';?>
	 <?php echo h($thesis['Topic']['name']);?>
	 <br/>
	 <?php echo __('Typ práce').': ';?>
	 <?php echo h($thesis['Type']['name']);?>
	 <br/>
	 <?php echo __('Vedoucí').': ';?>
	 <?php echo '<strong>'.h($thesis['Topic']['Leader']['full_name']).'</strong>'.sprintf(' (%s)',  $thesis['Topic']['Leader']['email']);?>
