<?php
echo '<p>' . __('
	Vedoucí práce (%s) si váží Vašeho zájmu, ale bohužel k tématu číslo %s jste nebyl přiřazen. 
	Prosím požádejte o jiné téma.
	', h($topic['Leader']['full_name']), $topic['Topic']['id']) . '</p>';
?>
<strong><?php echo __('Detail požádaného tématu');?></strong>
	 <br/>
	 <?php echo __('ID').': ';?>
	 <?php echo h($topic['Topic']['id']);?> 
	 <br/>
	 <?php echo __('Název').': ';?>
	 <?php echo h($topic['Topic']['name']);?>
	 <br/>
	 <?php echo __('Typ práce').': ';?>
	 <?php echo h($queue['Type']['name']);?>
	 <br/>
	 <?php echo __('Vedoucí').': ';?>
	 <?php echo '<strong>'.h($topic['Leader']['full_name']).'</strong>'.sprintf(' (%s)',  $topic['Leader']['email']);?>
