<div class="row-fluid">
    <div class="span6">
        <h3>Obory na fakultě</h3>
        <div class="well">
         <?php
            foreach($departments as $department['id'] => $department['name']) {
                $actions = $this->element('menu/actions', array(
                    'controller' => 'departments',
                    'action' => array('edit', 'delete'),
                    'id' => $department['id'],
                    'options' => array('class' => 'btn btn-small',
					'override_action' => 'index')
                ));
                
                $format = '<span class="inblock" style="width: 80%%; margin-left: 20px;">%s (%s)</span>'.$actions;
                if(in_array($department['name'], $root_departments)) {
                    $format = "<strong>%s</strong>";
                }
                 
                echo sprintf($format.'<br/>', h($department['name']), $abbreviations_list[$department['id']]);                
            }
         ?>
         </div>
    </div>
    <div class="span6">
    	<?php echo $this->element('forms/department'); ?>
    </div>
    <div class="row-fluid">
        <div class="span6">
            <?php echo $this->element('cancel-button'); ?>
        </div>
        <div class="span6">
            <?php echo $this->element('forms/submit'); ?>
        </div>
    </div>
</div>
<?php unset($departments); ?>
