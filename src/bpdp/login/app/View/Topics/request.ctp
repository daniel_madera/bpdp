<?php
if (is_array($topic['Queue'])) {
	$countRequested = count($topic['Queue']);
}
?>
<div class="row-fluid ">
	<div class="span6 well">
		<h4>Vybrané téma</h4>
		<?php echo $this->element('views/topic/detail', array('options' => array('modal-text' => array('Anotace' => array('maxLength' => 65))))); ?>
		<?php 
			echo $this -> element('views/queue-limits');
			echo $this->element('views/theses-merged');
		?>
	</div>
	<div class="span6 well">
		<?php
		echo $this->element('forms/create', array(
			'model' => 'Queue',
			'options' => array(
				'class' => 'form-inline no-bottom-margin',
				'id' => 'queue-create-form'
			)
		));

		$options = array();
		foreach ($topic['Type'] as $type) {
			$options[$type['id']] = h($type['name']);
		}

		echo $this->Form->input('Queue.type_id', array(
			'multiple' => false,
			'type' => 'select',
			'options' => $options,
			'div' => 'control-group',
			'class' => ' input-block-level',
			'empty' => __('Vyberte...'),
			'label' => __('Vyberte typ práce, o kterou chcete požádat')
		));

		echo $this->Form->input('comment', array(
			'type' => 'textarea',
			'div' => 'control-group',
			'class' => ' input-block-level',
			'label' => __('Napište důvod, proč jste si vybral toto téma')
		));

		echo $this->element('forms/submit', array(
			'title' => __('Potvrdit výběr'),
			'options' => array(
				'class' => 'btn btn-primary btn-large btn-block js-confirm-op',
				'data-title' => __('Potvrzení žádosti'),
				'data-text' => __('Opravdu chcete závazně požádat o toto téma? Po potvrzení může Vaší žádost zrušit pouze vedoucí práce.'),
				'data-submit' => '#queue-create-form'
			)
		));
		?>
	</div>
</div>