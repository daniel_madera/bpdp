<?php
       $this->Paginator->options(array(
            'url'=>$this->passedArgs,
            'data'=>http_build_query($this->request->data),
            'update'=>'#sub-content',
            'evalScripts'=>true,
            'method'=>'POST',
            'before'=>$this->Js->get('#overlay-wrapper')->effect('fadeIn'),
            'complete'=>$this->Js->get('#overlay-wrapper')->effect('fadeOut').'window.scrollTo(0,121);paginate_icons_sort();'
        ));  
       echo $this->Js->buffer('
        $(".accordion-body").on("show", function () {
            $("#heading-" + $(this).attr("id")).find("i").attr("class", "fa fa-chevron-up");            
        });
        $(".accordion-body").on("hide", function () {
            $("#heading-" + $(this).attr("id")).find("i").attr("class", "fa fa-chevron-down");          
        });
    ');
    $this->Js->get('#topics-expand-button');
    $this->Js->event('click','
            var icon = $(this).find("i");
            if(icon.hasClass("fa-chevron-down")) {
                $(".accordion").find(".accordion-body").collapse("show");   
                icon.removeClass("fa-chevron-down");
                icon.addClass("fa-chevron-up");
            } else if(icon.hasClass("fa-chevron-up")) {
                $(".accordion").find(".accordion-body").collapse("hide");   
                icon.addClass("fa-chevron-down");
                icon.removeClass("fa-chevron-up");
            }
        ');
        ?>
<div class="row-fluid">
	<div class="span9">	
		
		<?php 
		echo '<h3>'.$subtitle_for_layout.'</h3>';
        
		echo '<p>'.__('Student má možnot požádat maximálně o %s témata.', '<strong>'.Configure::read('Application.limit.requests.student').'</strong>').'<br />';
		echo __('O jedno téma může zažádat neomezený počet studentů.').'<br />';
		echo __('Vedoucí práce rozhodne o přiřazení nebo odmítnutí studentů. Nepřiřazení studenti mohou zažádat o jiné téma.').'</p>';
        ?>
        <div id="sub-content">
			<?php echo $this->element('views/topic/topics-list'); ?>
		</div>
	</div>
	<div class="span3" >
			<?php echo $this->element('views/topic/info', array('userQueues' => $userQueues)); ?> 
		 	<?php echo $this->element('views/topic/filter'); ?>
	</div>
</div>
<?php
echo $this->Js->writeBuffer();
?>
