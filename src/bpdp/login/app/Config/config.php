<?php
date_default_timezone_set('Europe/Prague'); 
setlocale(LC_ALL, 'cs_CZ');
Configure::write('App.locale', 'cs_CZ');
 
Configure::write(
    'Application', array(
        'name' => 'BPDP',
        'slogan' => 'Správa studentských prací',
        'url' => 'http://www.mti.tul.cz/bpdp',
        'univerzity' => array(
            'name' => 'Technická univerzita v Liberci', 
            'name_short' => 'TUL',
            'faculty' => 'Fakulta mechatroniky, informatiky a mezioborových studií',
            'faculty_short' => 'FM'
        ),
        'admin_contact' => 'jana.vitvarova@tul.cz',
		'email_notifications' => 1, // 0 for disable 
		'default_institute' => 'MTI', // if set allows users without info about institute from shibboleth and sets default institute
        'restrictions' => array( // has to be in order (admin, employee, student)
            'superuser' => array(
                'eppn' => array(
                    'combinations' => array(
                        'jana.vitvarova@tul.cz', // prvni bude zobrazen jako kontakt na admina, kdyz dojde k chybe, ze ktere se uz nepujde vzpamatovat              
                        'daniel.madera@tul.cz'
                    )
                )
            ),
			'admin' => array(
				'eppn' => array(
					'combinations' => array(	
						'jan.koprnicky@tul.cz', 
						'tester.admin@tul.cz',
						'josef.chaloupka@tul.cz', // nekdo z ITE
						'zdenek.pliva@tul.cz' // nekdo z NTI
					)
				)
			),
			'employee' => array(
				'affiliation' => array(
					'delimiter' => ';',
					'combinations' => array( // all possible combinations
						'faculty@tul.cz;member@tul.cz;student@tul.cz', // must contain at least this values
						'faculty@tul.cz;affiliate@tul.cz',
						'employee@tul.cz',
						'stuff@tul.cz'
					)
				),
				'orgunit-dn' => array(
					'delimiter' => ',',
					'combinations' => array(
						'ou=FM',
						'ou=CXI'
					)
				)
			),
			'student' => array(
				'affiliation' => array(
					'delimiter' => ';',
					'combinations' => array(
						'student@tul.cz',
						'alum@tul.cz'
					)
				),
				// 'orgunit-dn' => array(
				// 	'delimiter' => ',',
				// 	'combinations' => array(
				// 		'ou=FM'
				// 	)
				// )
			) 
		),
		'limit' => array(
            'requests' => array(
                'student' => 3, // limit pozadani o temata na jednoho studenta a typ prace
            ),
        ),
        'init' => array( // pri vytvareni noveho tematu
            'requirement' => 5, // vychozi pocet poli zasadam pro vypracovani
            'literature' => 3, // vychozi pocet poli u literatury
        ),
        'paginator' => array(
			'default' => array(
		        'limit' => 20, // defaultni strankovani 
			),
			'topics' => array(
				'limit' => 20, // defaultrni strankovani pro temata, muze si uzivatel prenastavit
				'order' => array( // vychozi razeni
		            'Topic.created' => 'desc'
		        )
			),
            'users' => array(
                'limit' => 25, 
                'order' => array(
                    'User.surname' => 'asc'
                )
            )	
		),
    )	
);

Configure::write('debug', 0); // hide all debug reports