<?php

// ------------DEBUGGING-------------
Configure::write('debug_orgunit-dn', 'ou=FM,o=VSLIB');
Configure::write('debug_affiliation', 'member@tul.cz;student@tul.cz');
Configure::write('debug_eppn', 'daniel.madera1@tul.cz');
Configure::write('debug', 2); 
Configure::write('debug_email', 'madera.dan@gmail.com');

// debug values passed as if from shibboleth
// Configure::write('debug_orgunit-dn', 'cn=MTI,ou=FM,o=VSLIB');
// Configure::write('debug_affiliation', 'alum@tul.cz;faculty@tul.cz;member@tul.cz;employee@tul.cz');
// Configure::write('debug_eppn', 'tester.admin@tul.cz');

//teacher
// Configure::write('debug_orgunit-dn', 'cn=MTI,ou=FM');
// Configure::write('debug_affiliation', 'faculty@tul.cz;member@tul.cz;employee@tul.cz');
// Configure::write('debug_eppn', 'test.teacher@tul.cz');

// student
// Configure::write('debug_orgunit-dn', 'ou=FM');
// Configure::write('debug_affiliation', 'student@tul.cz');
// Configure::write('debug_eppn', 'tester.student@tul.cz');

//admin
Configure::write('debug_orgunit-dn', 'cn=MTI,ou=FM');
Configure::write('debug_eppn', 'jana.vitvarova@tul.cz');

// show debug reports
// Configure::write('debug', 2); 
// override email notifications
// Configure::write('debug_email', 'madera.dan@gmail.com'); 

// Configure::write('debug_affiliation', 'member@tul.cz;student@tul.cz');
// Configure::write('debug_orgunit-dn', 'ou=UZS,ou=LIANE,o=VSLIB');
// Configure::write('debug_eppn', 'david.hatle@tul.cz');
?>