INSERT INTO
  `departments` (`id`, `name`, `abbreviation`, `parent_id`, `lft`, `rght`)
VALUES
  (1, 'Bakalářské obory', NULL, NULL, 1, 8),
  (2, 'Informační technologie','IT', 1, 2, 3),
  (3, 'Elektronické řídící systémy', 'EIRS', 1, 4, 5),
  (4, 'Nanomateriály', 'NANO', 1, 6, 7),
  (5, 'Magisterské obory', NULL, NULL, 9, 14),
  (6, 'Informační technologie', 'IT', 5, 10, 11),
  (7, 'Nanomateriály', 'NANO', 5, 12, 13);

INSERT INTO `bpdp`.`types` (`id`, `name`, `abbreviation`, `department_id`, `description`, `range`) VALUES 
('1', 'Bakalářský projekt', 'PRJ', 1, 'Nejaky popis', '30 - 40 stran'),
('2', 'Bakalářská práce', 'BP', 1, 'Nejaky popis', '30 - 40 stran'),
('3', 'Magisterský projekt', 'PRO', 5, 'Nejaky popis', '30 - 40 stran'),
('4', 'Diplomová práce', 'DP', 5, 'Nejaky popis', '40 - 50 stran');

INSERT INTO `bpdp`.`institutes` (`id` , `name` , `abbreviation`) VALUES
('1', 'Ústav mechatroniky a technické informatiky', 'MTI'),
('2', 'Ústav nových technologií a aplikované informatiky', 'NTI'),
('3', 'Ústav informačních technologií a elektroniky', 'ITE');

INSERT INTO `bpdp`.`groups` (`id`, `created`, `modified`, `name`) VALUES 
('1', NOW(), NOW(), 'admin'), 
('2', NOW(), NOW(), 'employee'), 
('3', NOW(), NOW(), 'student'),
('4', NOW(), NOW(), 'superuser');
