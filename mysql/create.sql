-- MySQL Script generated by MySQL Workbench
-- Tue 04 Nov 2014 03:15:52 PM CET
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema bpdp
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema bpdp
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `bpdp` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `bpdp` ;

-- -----------------------------------------------------
-- Table `bpdp`.`groups`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bpdp`.`groups` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `created` DATETIME NULL,
  `modified` DATETIME NULL,
  `name` VARCHAR(45) NULL,
  `auto_access` DATETIME NOT NULL DEFAULT "2014-01-01 00:00:01",
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bpdp`.`institutes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bpdp`.`institutes` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  `abbreviation` VARCHAR(5) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bpdp`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bpdp`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `created` DATETIME NULL DEFAULT NULL,
  `modified` DATETIME NULL DEFAULT NULL,
  `email` VARCHAR(80) NOT NULL,
  `last_login` DATETIME NULL,
  `group_id` INT NOT NULL,
  `forename` VARCHAR(50) NOT NULL,
  `surname` VARCHAR(50) NOT NULL,
  `title_before_name` VARCHAR(25) NULL,
  `title_behind_name` VARCHAR(15) NULL,
  `institute_id` INT NULL,
  `page_limit` INT NULL,
  `collapsed` TINYINT NOT NULL DEFAULT 0,
  `topic_count` INT NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC),
  CONSTRAINT `fk_users_groups1`
    FOREIGN KEY (`group_id`)
    REFERENCES `bpdp`.`groups` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_users_institutes1`
    FOREIGN KEY (`institute_id`)
    REFERENCES `bpdp`.`institutes` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bpdp`.`departments`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bpdp`.`departments` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `lft` INT NULL DEFAULT NULL,
  `rght` INT NULL DEFAULT NULL,
  `name` VARCHAR(50) NOT NULL,
  `abbreviation` VARCHAR(10) NULL,
  `parent_id` INT NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_departments_departments1`
    FOREIGN KEY (`parent_id`)
    REFERENCES `bpdp`.`departments` (`id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'magisterske studium\n	- IT, asd\ninzenyrske studium\n	-  IT, EI' /* comment truncated */ /*RS, atd.*/;


-- -----------------------------------------------------
-- Table `bpdp`.`types`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bpdp`.`types` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `abbreviation` VARCHAR(6) NOT NULL,
  `description` VARCHAR(500) NOT NULL,
  `range` VARCHAR(30) NULL,
  `department_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_types_departments1`
    FOREIGN KEY (`department_id`)
    REFERENCES `bpdp`.`departments` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bpdp`.`topics`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bpdp`.`topics` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  `created` DATETIME NULL,
  `modified` DATETIME NULL,
  `leader_id` INT NOT NULL COMMENT 'as leader\n',
  `abstract` VARCHAR(1500) NOT NULL,
  `requirements` VARCHAR(500) NULL,
  `literature` VARCHAR(500) NULL,
  `consultant` VARCHAR(255) NULL,
  `institute_id` INT NOT NULL,
  `state` TINYINT NOT NULL DEFAULT 1 COMMENT '1 - activ\n0 - hidden\n-1 - archived\n2 - pending\n3 - confirmed',
  `archived` TINYINT NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_topics_users1`
    FOREIGN KEY (`leader_id`)
    REFERENCES `bpdp`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `fk_topics_institutes1`
    FOREIGN KEY (`institute_id`)
    REFERENCES `bpdp`.`institutes` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'topic of thesis, used as a selection list for student to add' /* comment truncated */ /* to a queue*/;


-- -----------------------------------------------------
-- Table `bpdp`.`theses`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bpdp`.`theses` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `created` DATETIME NULL,
  `modified` DATETIME NULL,
  `name` VARCHAR(200) NOT NULL,
  `name_en` VARCHAR(200) NULL,
  `user_id` INT NOT NULL,
  `type_id` INT NOT NULL,
  `topic_id` INT NOT NULL,
  `final` TINYINT NOT NULL DEFAULT 0 COMMENT '-1 - archived (after end of a year), 1 - final',
  `archived` TINYINT NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`))
ENGINE = MyISAM
COMMENT = 'status of thesis can be retrieved if user_id is assigned fro' /* comment truncated */ /*m queue or not
finalni tabulka pro zadani
*/;


-- -----------------------------------------------------
-- Table `bpdp`.`parameters`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bpdp`.`parameters` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(30) NOT NULL,
  `description` VARCHAR(200) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB
COMMENT = 'Contains dynamic properties of theses which leads to precise' /* comment truncated */ /* filtration. Such as part time, full time etc. It's similar to student requirements, language in which can be thesis written*/;


-- -----------------------------------------------------
-- Table `bpdp`.`topics_types`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bpdp`.`topics_types` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `topic_id` INT NOT NULL,
  `type_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_tasks_has_thesis_types_tasks1`
    FOREIGN KEY (`topic_id`)
    REFERENCES `bpdp`.`topics` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tasks_has_thesis_types_thesis_types1`
    FOREIGN KEY (`type_id`)
    REFERENCES `bpdp`.`types` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bpdp`.`parameters_topics`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bpdp`.`parameters_topics` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `parameter_id` INT NOT NULL,
  `topic_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_topics_has_parameters_topics1`
    FOREIGN KEY (`topic_id`)
    REFERENCES `bpdp`.`topics` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_topics_has_parameters_parameters1`
    FOREIGN KEY (`parameter_id`)
    REFERENCES `bpdp`.`parameters` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bpdp`.`queues`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bpdp`.`queues` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NOT NULL,
  `topic_id` INT NOT NULL,
  `created` DATETIME NOT NULL,
  `comment` VARCHAR(200) NULL,
  `type_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_users_has_topics_users1`
    FOREIGN KEY (`user_id`)
    REFERENCES `bpdp`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_users_has_topics_topics1`
    FOREIGN KEY (`topic_id`)
    REFERENCES `bpdp`.`topics` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_topics_users_types1`
    FOREIGN KEY (`type_id`)
    REFERENCES `bpdp`.`types` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'Represents queues';


-- -----------------------------------------------------
-- Table `bpdp`.`departments_topics`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bpdp`.`departments_topics` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `department_id` INT NOT NULL,
  `topic_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_departments_has_topics_departments1`
    FOREIGN KEY (`department_id`)
    REFERENCES `bpdp`.`departments` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_departments_has_topics_topics1`
    FOREIGN KEY (`topic_id`)
    REFERENCES `bpdp`.`topics` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bpdp`.`literatures`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bpdp`.`literatures` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `thesis_id` INT NOT NULL,
  `text` VARCHAR(200) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_literature_theses1`
    FOREIGN KEY (`thesis_id`)
    REFERENCES `bpdp`.`theses` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bpdp`.`requirements`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bpdp`.`requirements` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `thesis_id` INT NOT NULL,
  `text` VARCHAR(200) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_requirements_theses1`
    FOREIGN KEY (`thesis_id`)
    REFERENCES `bpdp`.`theses` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bpdp`.`alt_users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bpdp`.`alt_users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(80) NOT NULL,
  `group_id` INT NOT NULL,
  `institute_id` INT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC),
  CONSTRAINT `fk_alt_users_groups1`
    FOREIGN KEY (`group_id`)
    REFERENCES `bpdp`.`groups` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_alt_users_institutes1`
    FOREIGN KEY (`institute_id`)
    REFERENCES `bpdp`.`institutes` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
