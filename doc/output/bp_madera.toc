\select@language {czech}
\contentsline {section}{\numberline {1}\IeC {\'U}vod}{9}{section.1}
\contentsline {section}{\numberline {2}Anal\IeC {\'y}za st\IeC {\'a}vaj\IeC {\'\i }c\IeC {\'\i }ho stavu}{10}{section.2}
\contentsline {subsection}{\numberline {2.1}Zad\IeC {\'a}v\IeC {\'a}n\IeC {\'\i } t\IeC {\'e}mat}{10}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}V\IeC {\'y}b\IeC {\v e}r t\IeC {\'e}mat}{11}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Dokon\IeC {\v c}en\IeC {\'\i } zad\IeC {\'a}v\IeC {\'a}n\IeC {\'\i } a~export ofici\IeC {\'a}ln\IeC {\'\i }ch zad\IeC {\'a}n\IeC {\'\i }}{12}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Shrnut\IeC {\'\i }}{12}{subsection.2.4}
\contentsline {section}{\numberline {3}N\IeC {\'a}vrh na \IeC {\v r}e\IeC {\v s}en\IeC {\'\i }}{13}{section.3}
\contentsline {subsection}{\numberline {3.1}Proces automatizace}{13}{subsection.3.1}
\contentsline {subsubsection}{\numberline {3.1.1}Zad\IeC {\'a}v\IeC {\'a}n\IeC {\'\i } t\IeC {\'e}mat}{13}{subsubsection.3.1.1}
\contentsline {subsubsection}{\numberline {3.1.2}V\IeC {\'y}b\IeC {\v e}r t\IeC {\'e}mat}{14}{subsubsection.3.1.2}
\contentsline {subsubsection}{\numberline {3.1.3}Dokon\IeC {\v c}en\IeC {\'\i } zad\IeC {\'a}v\IeC {\'a}n\IeC {\'\i } a~export ofici\IeC {\'a}ln\IeC {\'\i }ch zad\IeC {\'a}n\IeC {\'\i }}{15}{subsubsection.3.1.3}
\contentsline {subsubsection}{\numberline {3.1.4}Shrnut\IeC {\'\i }}{15}{subsubsection.3.1.4}
\contentsline {subsection}{\numberline {3.2}V\IeC {\'y}b\IeC {\v e}r technologi\IeC {\'\i }}{17}{subsection.3.2}
\contentsline {subsubsection}{\numberline {3.2.1}V\IeC {\'y}b\IeC {\v e}r aplika\IeC {\v c}n\IeC {\'\i }ho frameworku}{17}{subsubsection.3.2.1}
\contentsline {subsubsection}{\numberline {3.2.2}CakePHP}{17}{subsubsection.3.2.2}
\contentsline {subsubsection}{\numberline {3.2.3}Porovn\IeC {\'a}n\IeC {\'\i } CakePHP s~Nette a~Zend}{18}{subsubsection.3.2.3}
\contentsline {subsubsection}{\numberline {3.2.4}Twitter Bootstrap}{19}{subsubsection.3.2.4}
\contentsline {subsubsection}{\numberline {3.2.5}JQuery}{19}{subsubsection.3.2.5}
\contentsline {section}{\numberline {4}Realizace aplikace}{20}{section.4}
\contentsline {subsection}{\numberline {4.1}Metodika v\IeC {\'y}voje aplikace}{20}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}U\IeC {\v z}ivatelsk\IeC {\'e} rozhran\IeC {\'\i }}{20}{subsection.4.2}
\contentsline {subsubsection}{\numberline {4.2.1}Responzivn\IeC {\'\i } design}{21}{subsubsection.4.2.1}
\contentsline {subsection}{\numberline {4.3}Datab\IeC {\'a}zov\IeC {\'y} model}{21}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}Aplika\IeC {\v c}n\IeC {\'\i } framework}{23}{subsection.4.4}
\contentsline {subsubsection}{\numberline {4.4.1}Zpracov\IeC {\'a}n\IeC {\'\i } po\IeC {\v z}adavku}{23}{subsubsection.4.4.1}
\contentsline {subsubsection}{\numberline {4.4.2}Konfigurace CakePHP}{23}{subsubsection.4.4.2}
\contentsline {subsubsection}{\numberline {4.4.3}Konvence a~n\IeC {\'a}zvoslov\IeC {\'\i }}{24}{subsubsection.4.4.3}
\contentsline {subsection}{\numberline {4.5}MVC a~adres\IeC {\'a}\IeC {\v r}ov\IeC {\'a} struktura}{24}{subsection.4.5}
\contentsline {subsection}{\numberline {4.6}Modelov\IeC {\'a} vrstva}{25}{subsection.4.6}
\contentsline {subsubsection}{\numberline {4.6.1}Rela\IeC {\v c}n\IeC {\'\i } mapov\IeC {\'a}n\IeC {\'\i }}{25}{subsubsection.4.6.1}
\contentsline {subsubsection}{\numberline {4.6.2}Validace dat}{25}{subsubsection.4.6.2}
\contentsline {subsection}{\numberline {4.7}\IeC {\v R}\IeC {\'\i }d\IeC {\'\i }c\IeC {\'\i } logika}{26}{subsection.4.7}
\contentsline {subsubsection}{\numberline {4.7.1}Akce}{26}{subsubsection.4.7.1}
\contentsline {subsubsection}{\numberline {4.7.2}Komponenty}{27}{subsubsection.4.7.2}
\contentsline {subsection}{\numberline {4.8}Pohledy}{27}{subsection.4.8}
\contentsline {subsubsection}{\numberline {4.8.1}\IeC {\v S}ablony pohled\IeC {\r u}}{27}{subsubsection.4.8.1}
\contentsline {subsubsection}{\numberline {4.8.2}Helpery}{27}{subsubsection.4.8.2}
\contentsline {subsubsection}{\numberline {4.8.3}Elementy}{28}{subsubsection.4.8.3}
\contentsline {subsubsection}{\numberline {4.8.4}Sd\IeC {\'\i }len\IeC {\'\i } pohled\IeC {\r u}}{29}{subsubsection.4.8.4}
\contentsline {subsubsection}{\numberline {4.8.5}Jazykov\IeC {\'e} variace}{29}{subsubsection.4.8.5}
\contentsline {subsection}{\numberline {4.9}Pluginy}{30}{subsection.4.9}
\contentsline {subsubsection}{\numberline {4.9.1}BoostCake}{30}{subsubsection.4.9.1}
\contentsline {subsubsection}{\numberline {4.9.2}Vlastn\IeC {\'\i } plugin}{30}{subsubsection.4.9.2}
\contentsline {subsection}{\numberline {4.10}System rol\IeC {\'\i }}{30}{subsection.4.10}
\contentsline {subsubsection}{\numberline {4.10.1}Autoriza\IeC {\v c}n\IeC {\'\i } komponenta}{31}{subsubsection.4.10.1}
\contentsline {subsubsection}{\numberline {4.10.2}Shibboleth}{32}{subsubsection.4.10.2}
\contentsline {subsubsection}{\numberline {4.10.3}Altertnativn\IeC {\'\i } p\IeC {\v r}id\IeC {\v e}len\IeC {\'\i } opr\IeC {\'a}vn\IeC {\v e}n\IeC {\'\i }}{32}{subsubsection.4.10.3}
\contentsline {subsection}{\numberline {4.11}Export dat}{32}{subsection.4.11}
\contentsline {subsection}{\numberline {4.12}N\IeC {\'a}pov\IeC {\v e}da}{33}{subsection.4.12}
\contentsline {subsection}{\numberline {4.13}Testov\IeC {\'a}n\IeC {\'\i } aplikace}{33}{subsection.4.13}
\contentsline {subsubsection}{\numberline {4.13.1}Jednotkov\IeC {\'e} testov\IeC {\'a}n\IeC {\'\i }}{34}{subsubsection.4.13.1}
\contentsline {subsubsection}{\numberline {4.13.2}Integra\IeC {\v c}n\IeC {\'\i } testov\IeC {\'a}n\IeC {\'\i }}{34}{subsubsection.4.13.2}
\contentsline {subsubsection}{\numberline {4.13.3}Akcepta\IeC {\v c}n\IeC {\'\i } testov\IeC {\'a}n\IeC {\'\i }}{34}{subsubsection.4.13.3}
\contentsline {section}{\numberline {5}Z\IeC {\'a}v\IeC {\v e}r}{35}{section.5}
\contentsline {section}{Pou\IeC {\v z}it\IeC {\'a} literatura}{36}{section*.11}
\contentsline {section}{Obsah p\IeC {\v r}ilo\IeC {\v z}en\IeC {\'e}ho DVD}{37}{section*.12}
